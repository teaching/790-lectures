\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzcards}
\usepackage{fitzcoin}
\usepackage{fitzroulette}
\renewcommand{\dukelogo}{../../dukemath.pdf}

\usepackage{caption}
\captionsetup{skip=0pt}
\usepackage{booktabs}
\usepackage{epsdice}
\newcommand\vcdice[1]{\vcenter{\hbox{\epsdice{#1}}}}

\newcommand{\myTableTitle}[4]{
  \begin{minipage}{1.0\linewidth}
    \begin{table}
      \centering
      \caption*{\mybold{#1}}%
      \begin{tabular}{c#2}
        \toprule
        \myotherbold{$\Omega$} & #3 \\
        \midrule
        \myotherbold{$\mu$} & #4 \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{minipage}%
}
\newcommand{\myTable}[3]{
  \begin{minipage}{1.0\linewidth}
    \begin{center}
      \begin{table}
        \centering
        % \caption*{\myotherbold{#1}}
        \begin{tabular}{c#1}
          \toprule
          \myotherbold{$\Omega$} & #2 \\
          \midrule
          \myotherbold{$\mu$} & #3 \\
          \bottomrule
        \end{tabular}
      \end{table}
    \end{center}
  \end{minipage}
}


\title{Expected Value}
\subtitle{Math 790-92}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Expected Value}
\subsection{Definition}
\begin{sagesilent}
  s = [50, 50, 70, 70, 70, 70, 80, 80, 80, 100]
  s_sort = sorted(list(set(s)))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myMean}{
    \oldfrac{
      \onslide<5->{\sage{s[0]}+}
      \onslide<6->{\sage{s[1]}+}
      \onslide<7->{\sage{s[2]}+}
      \onslide<8->{\sage{s[3]}+}
      \onslide<9->{\sage{s[4]}+}
      \onslide<10->{\sage{s[5]}+}
      \onslide<11->{\sage{s[6]}+}
      \onslide<12->{\sage{s[7]}+}
      \onslide<13->{\sage{s[8]}+}
      \onslide<14->{\sage{s[9]}}
    }{
      \sage{len(s)}
    }
  }
  \begin{example}
    Consider the data of a list of ten exam scores
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\sage{s}} && \onslide<3->{\operatorname{Mean}} &\onslide<3->{=} \onslide<4->{\myMean} \onslide<15->{= \sage{mean(s)}}
      \end{align*}
    \end{gather*}
    \onslide<16->Alternatively, we may look at the ``distribution'' of scores
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=1
        ]

        \begin{scope}[yscale=3]
          \onslide<17->{
            \draw[beamgreen] (1, 0) -- (1, 2/10);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{2}{10}$}}] at ( 1, 2/10) {};
          }

          \onslide<18->{
            \draw[beamgreen] (3, 0) -- (3, 4/10);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{4}{10}$}}] at ( 3, 4/10) {};
          }

          \onslide<19->{
            \draw[beamgreen] (4, 0) -- (4, 3/10);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{3}{10}$}}] at ( 4, 3/10) {};
          }

          \onslide<20->{
            \draw[beamgreen] (6, 0) -- (6, 1/10);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{1}{10}$}}] at ( 6, 1/10) {};
          }
        \end{scope}

        \draw[<->] (0, 0) -- (7, 0);

        \foreach \i/\itext in {1/50, 3/70, 4/80, 6/100}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\itext$};
        }

      \end{tikzpicture}
    \]
    \onslide<21->{To compute the mean, we have}
    \[
      \onslide<22->{\operatorname{Mean} =}
      \onslide<23->{50\cdot\pair*{\sfrac{2}{10}} +}
      \onslide<24->{70\cdot\pair*{\sfrac{4}{10}} +}
      \onslide<25->{80\cdot\pair*{\sfrac{3}{10}} +}
      \onslide<26->{100\cdot\pair*{\sfrac{1}{10}} =}
      \onslide<27->{\sage{mean(s)}}
    \]
    \onslide<28->{Expected value is a generalization of this idea.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{expected value} of a random variable $X$ is
    \[
      \E{X} = \sum_{x\in\Range(X)}x\cdot\Pr{X=x}
    \]
    \pause The scalar $\E{X}$ is also called the \emph{mean} of $X$.
  \end{definition}

\end{frame}



\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number of heads after flipping a fair coin three
    times. \onslide<2->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=2
        ]

        \newcommand{\myBinom}[1]{\binom{3}{####1}\cdot(\sfrac{1}{2})^{####1}\cdot(1-\sfrac{1}{2})^{3-####1}}
        \begin{scope}[yscale=3]

          \onslide<3->{
            \draw[beamgreen] (0, 0) -- (0, 1/8);
            \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{0}=\frac{1}{8}$}}] at ( 0, 1/8) {};
          }

          \onslide<4->{
            \draw[beamgreen] (1, 0) -- (1, 3/8);
            \node[mydot, label={[label distance=-5pt]120:{$\scriptstyle\myBinom{1}=\frac{3}{8}$}}] at ( 1, 3/8) {};
          }

          \onslide<5->{
            \draw[beamgreen] (2, 0) -- (2, 3/8);
            \node[mydot, label={[label distance=-5pt]60:{$\scriptstyle\myBinom{2}=\frac{3}{8}$}}] at ( 2, 3/8) {};
          }

          \onslide<6->{
            \draw[beamgreen] (3, 0) -- (3, 1/8);
            \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{3}=\frac{1}{8}$}}] at ( 3, 1/8) {};
          }
        \end{scope}

        \draw[<->] (-1, 0) -- (4, 0);

        \foreach \i in {0,...,3}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \node[beamblue, above = 0mm of current bounding box.north] {$X\sim\Binomial(3, \sfrac{1}{2})$};
      \end{tikzpicture}
    \]
    \onslide<7->{The expected value of $X$ is}
    \[
      \onslide<7->{\E{X} =}
      \onslide<8->{0\cdot(\sfrac{1}{8})+}\onslide<9->{1\cdot(\sfrac{3}{8})+}\onslide<10->{2\cdot(\sfrac{3}{8})+}\onslide<11->{3\cdot(\sfrac{1}{8}) =}
      \onslide<12->{\sfrac{3}{2}}
    \]
    \onslide<13->{Note that $X=\E{X}$ is impossible here.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose now that the coin is biased in favor of tails.\onslide<2->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=2
        ]

        \newcommand{\myBinom}[1]{\pgfmathtruncatemacro{\myexp}{3-####1}\binom{3}{####1}\cdot(\sfrac{2}{5})^{####1}\cdot(\sfrac{3}{5})^{\myexp}}
        \begin{scope}[yscale=3]
          \onslide<3->{
            \draw[beamgreen] (0, 0) -- (0, 27/125);
            \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{0}=\frac{27}{125}$}}] at ( 0, 27/125) {};
            }

            \onslide<4->{
          \draw[beamgreen] (1, 0) -- (1, 54/125);
          \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{1}=\frac{54}{125}$}}] at ( 1, 52/125) {};
          }

          \onslide<5->{
          \draw[beamgreen] (2, 0) -- (2, 36/125);
          \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{2}=\frac{36}{125}$}}] at ( 2, 36/125) {};
          }

          \onslide<6->{
          \draw[beamgreen] (3, 0) -- (3, 8/125);
          \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\myBinom{3}=\frac{8}{125}$}}] at ( 3, 8/125) {};
          }
        \end{scope}

        \draw[<->] (-1, 0) -- (4, 0);

        \foreach \i in {0,...,3}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \node[beamblue, above= 0mm of current bounding box.north] {$X\sim\Binomial(3, \sfrac{2}{5})$};
        % \matrix [draw,below left] at (current bounding box.north east) {
        % \node {}; \\
        % };
      \end{tikzpicture}
    \]
    \onslide<7->{The expected value of $X$ is}
    \[
      \onslide<7->{\E{X} =}
      \onslide<8->{0\cdot(\sfrac{27}{125})+}\onslide<9->{1\cdot(\sfrac{54}{125})+}\onslide<10->{2\cdot(\sfrac{36}{125})+}\onslide<11->{3\cdot(\sfrac{8}{125}) =}
      \onslide<12->{\sfrac{6}{5}}
    \]
    \onslide<13->{Note that $X=\E{X}$ is impossible here.}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPayout}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {``red/black''};
      \onslide<2->{
        \node[overlay, above right= 1mm and -0.25cm of a] (text) {payout is \usd{1} to \usd{1}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myEQ}{
    \begin{aligned}
      \onslide<3->{\E{X}} &\onslide<3->{=} \onslide<4->{\usd{1000}\cdot\Pr{X=\usd{1000}}-\usd{1000}\cdot\Pr{X=-\usd{1000}}} \\
      &\onslide<4->{=} \onslide<5->{\usd{1000}\cdot\Pr{\texttt{win}}-\usd{1000}\cdot\Pr{\texttt{lose}}} \\
      &\onslide<6->{=} \onslide<7->{\usd{1000}\cdot\pair*{\frac{18}{37}}-\usd{1000}\cdot\pair*{\frac{19}{37}}} \\
      &\onslide<7->{=} \onslide<8->{-\usd{1000/37}} \\
      &\onslide<9->{\simeq -\usd{27.0270}}
    \end{aligned}
  }
  \begin{example}
    Let $X$ be the winnings on a $\usd{1000}$\myPayout bet in roulette.
    \begin{gather*}
      \begin{align*}
        \roulette[2/3] &&  \myEQ
      \end{align*}
    \end{gather*}
    \onslide<10->{On average, we expect to \emph{lose} approximately
      $\usd{27.027}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myPayout}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {``green''};
      \onslide<2->{
        \node[overlay, above right= 1mm and -0.25cm of a] (text) {payout is \usd{35} to \usd{1}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \newcommand{\myEQ}{
    \begin{aligned}
      \onslide<3->{\E{X}} &\onslide<3->{=} \onslide<4->{\usd{35000}\cdot\Pr{X=\usd{35000}}-\usd{1000}\cdot\Pr{X=-\usd{1000}}} \\
      &\onslide<4->{=} \onslide<5->{\usd{35000}\cdot\Pr{\texttt{win}}-\usd{1000}\cdot\Pr{\texttt{lose}}} \\
      &\onslide<5->{=} \onslide<6->{\usd{35000}\cdot\pair*{\frac{1}{37}}-\usd{1000}\cdot\pair*{\frac{36}{37}}} \\
      &\onslide<7->{=} \onslide<8->{-\usd{1000/37}} \\
      &\onslide<9->{\simeq -\usd{27.0270}}
    \end{aligned}
  }
  \begin{example}
    Let $X$ be the winnings on a $\usd{1000}$\myPayout bet in roulette.
    \begin{gather*}
      \begin{align*}
        \roulette[2/3] &&  \myEQ
      \end{align*}
    \end{gather*}
    \onslide<10->{On average, we expect to \emph{lose} approximately $\usd{27.027}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number rolled on a fair six-sided die.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=1
        ]

        \draw[<->] (0, 0) -- (7, 0);

        \foreach \i in {1,...,6}{
          \begin{scope}[yscale=6]
            \pgfmathtruncatemacro{\myslide}{\i+1}
            \onslide<\myslide->{
              \draw[beamgreen] (\i, 0) -- (\i, 1/6);
              \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{1}{6}$}}] at (\i, 1/6) {};
            }
          \end{scope}
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }


        \node[beamblue, above= 0mm of current bounding box.north] {$X\sim\Unif\Set{1, 2, 3, 4, 5, 6}$};
      \end{tikzpicture}
    \]
    \onslide<8->{The expected value of $X$ is}
    \begin{gather*}
      \onslide<8->{\E{X} = (\onslide<9->{0+}\onslide<10->{1+}\onslide<11->{2+}\onslide<12->{3+}\onslide<13->{4+}\onslide<14->{5+}\onslide<15->{6})\cdot(\sfrac{1}{6})} \onslide<16->{= \sfrac{7}{2}}
    \end{gather*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the sum of a roll of two fair six-sided dice.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=.85
        ]

        \begin{scope}[yscale=12]
          \onslide<2->{
            \draw[beamgreen] (2, 0) -- (2, 1/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{1}{36}$}}] at ( 2, 1/36) {};
          }

          \onslide<4->{
            \draw[beamgreen] (3, 0) -- (3, 2/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{2}{36}$}}] at ( 3, 2/36) {};
          }

          \onslide<6->{
            \draw[beamgreen] (4, 0) -- (4, 3/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{3}{36}$}}] at ( 4, 3/36) {};
          }

          \onslide<8->{
            \draw[beamgreen] (5, 0) -- (5, 4/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{4}{36}$}}] at ( 5, 4/36) {};
          }

          \onslide<10->{
            \draw[beamgreen] (6, 0) -- (6, 5/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{5}{36}$}}] at ( 6, 5/36) {};
          }

          \onslide<12->{
            \draw[beamgreen] (7, 0) -- (7, 6/36);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{6}{36}$}}] at ( 7, 6/36) {};
          }

          \onslide<11->{
            \draw[beamgreen] (8, 0) -- (8, 5/36);
            \node[mydot, label={[label distance=-5pt]0:{$\scriptstyle\frac{5}{36}$}}] at ( 8, 5/36) {};
          }

          \onslide<9->{
            \draw[beamgreen] (9, 0) -- (9, 4/36);
            \node[mydot, label={[label distance=-5pt]0:{$\scriptstyle\frac{4}{36}$}}] at ( 9, 4/36) {};
          }

          \onslide<7->{
            \draw[beamgreen] (10, 0) -- (10, 3/36);
            \node[mydot, label={[label distance=-5pt]0:{$\scriptstyle\frac{3}{36}$}}] at (10, 3/36) {};
          }

          \onslide<5->{
            \draw[beamgreen] (11, 0) -- (11, 2/36);
            \node[mydot, label={[label distance=-5pt]0:{$\scriptstyle\frac{2}{36}$}}] at (11, 2/36) {};
          }

          \onslide<3->{
            \draw[beamgreen] (12, 0) -- (12, 1/36);
            \node[mydot, label={[label distance=-5pt]0:{$\scriptstyle\frac{1}{36}$}}] at (12, 1/36) {};
          }
        \end{scope}

        \draw[<->] (1, 0) -- (13, 0);

        \foreach \i in {2,...,12}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        % \node[beamblue, above right= 0mm and 0mm of current bounding box.north west] {$\Dist(X)$};
      \end{tikzpicture}
    \]
    \onslide<13->{The expected value of $X$ is}
    \begin{gather*}
      \onslide<14->{\E{X} =}
      \onslide<15->{(2+12)\cdot(\sfrac{1}{36}) +}
      \onslide<16->{(3+11)\cdot(\sfrac{2}{36}) +}
      \onslide<17->{(4+10)\cdot(\sfrac{3}{36}) +}
      \onslide<18->{(5+ 9)\cdot(\sfrac{4}{36}) +}
      \onslide<19->{(6+ 8)\cdot(\sfrac{5}{36}) +}
      \onslide<20->{7\cdot(\sfrac{6}{36}) =}
      \onslide<21->{7}
    \end{gather*}
  \end{example}
\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Linearity of Expected Value]
    $\E{\lambda_1\cdot X_1+\lambda_2\cdot X_2}=\lambda_1\cdot\E{X_1}+\lambda_2\cdot\E{X_2}$
  \end{theorem}


  \newcommand{\myXa}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$X_1$};
      \onslide<5->{
        \node[overlay, above left= 0mm and 0.25cm of a] (text) {first roll $\E{X_1}=\sfrac{7}{2}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.east);
      }
    }
  }
  \newcommand{\myXb}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$X_1$};
      \onslide<6->{
        \node[overlay, above right= 0mm and 0.25cm of a] (text) {second roll $\E{X_2}=\sfrac{7}{2}$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (text.west);
      }
    }
  }
  \onslide<2->
  \begin{example}
    Let $X$ be the sum of a roll of two fair six-sided dice. \onslide<3->{Then}
    \[
      \onslide<4->{X = \myXa+\myXb}
    \]
    \onslide<7->{The expected value of $X$ is then}
    \[
      \onslide<7->{\E{X} =}
      \onslide<8->{\E{X_1+X_2} =}
      \onslide<9->{\E{X_1}+\E{X_2} =}
      \onslide<10->{\sfrac{7}{2}+\sfrac{7}{2} =}
      \onslide<11->{7}
    \]
    \onslide<12->{This is easier than computing $\E{X}$ directly!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myX}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , mydot/.style={circle, fill, beamblue, inner sep=2pt}
      , xscale=0.85
      ]

      \draw[<->] (-1, 0) -- (3, 0);


      \foreach \i/\inumerator/\idenominator in {0/1/4, 1/2/4, 2/1/4}{
        \pgfmathtruncatemacro{\myslide}{\i+3}
        \begin{scope}[yscale=2]
          \onslide<\myslide->{
            \draw[beamgreen] (\i, 0) -- (\i, \inumerator/\idenominator);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{\inumerator}{\idenominator}$}}] at (\i, \inumerator/\idenominator) {};
          }
        \end{scope}
        \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
      }


      \node[beamblue, above= 0mm of current bounding box.north] {$\Dist(X)$};

    \end{tikzpicture}
  }
  \newcommand{\myY}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , mydot/.style={circle, fill, beamblue, inner sep=2pt}
      , xscale=0.85
      ]

      \draw[<->] (-1, 0) -- (3, 0);


      \foreach \i/\inumerator/\idenominator in {0/1/4, 1/2/4, 2/1/4}{
        \pgfmathtruncatemacro{\myslide}{\i+7}
        \begin{scope}[yscale=2]
          \onslide<\myslide->{
            \draw[beamgreen] (\i, 0) -- (\i, \inumerator/\idenominator);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{\inumerator}{\idenominator}$}}] at (\i, \inumerator/\idenominator) {};
          }
        \end{scope}
        \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
      }

      \node[beamblue, above= 0mm of current bounding box.north] {$\Dist(Y)$};

    \end{tikzpicture}
  }
  \newcommand{\myXY}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , mydot/.style={circle, fill, beamblue, inner sep=2pt}
      , xscale=0.85
      ]

      \draw[<->] (-1, 0) -- (2, 0);

      \foreach \i/\inumerator/\idenominator in {0/2/4, 1/2/4}{
        \pgfmathtruncatemacro{\myslide}{\i+11}
        \begin{scope}[yscale=2]
          \onslide<\myslide->{
            \draw[beamgreen] (\i, 0) -- (\i, \inumerator/\idenominator);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\frac{\inumerator}{\idenominator}$}}] at (\i, \inumerator/\idenominator) {};
          }
        \end{scope}
        \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
      }

      \node[beamblue, above= 0mm of current bounding box.north] {$\Dist(X\cdot Y)$};

    \end{tikzpicture}
  }
  \begin{example}
    Flip a coin twice. Count the number of heads ($X$) and tails ($Y$).
    \begin{align*}
      \onslide<2->{\myX} && \onslide<6->{\myY} && \onslide<10->{\myXY}
    \end{align*}
    \onslide<13->{The expected values of $X$, $Y$, and $X\cdot Y$ are}
    \begin{gather*}
      \begin{align*}
        \onslide<14->{\E{X}} &\onslide<14->{=} \onslide<15->{1\cdot(\sfrac{2}{4})+2\cdot(\sfrac{1}{4}) = 1} & \onslide<16->{\E{Y}} &\onslide<16->{=} \onslide<17->{1\cdot(\sfrac{2}{4})+2\cdot(\sfrac{1}{4}) = 1} & \onslide<18->{\E{X\cdot Y}} &\onslide<18->{=} \onslide<19->{1\cdot(\sfrac{2}{4}) = \sfrac{1}{2}}
      \end{align*}
    \end{gather*}
    \onslide<20->{Here, we have $\E{X\cdot Y}\neq\E{X}\cdot\E{Y}$.}
  \end{example}

\end{frame}


\subsection{The Method of Indicators}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{indicator} of an event $E$ is the random variable $\chi_E$ given by
    \[
      \chi_E(\omega)
      =
      \begin{cases}
        1 & \omega\in E \\
        0 & \omega\notin E
      \end{cases}
    \]
    \onslide<2->The distribution of $\chi_E$ is
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        ]

        \draw[<->] (-1, 0) -- (2, 0);

        \begin{scope}[yscale=2]
          \onslide<3->{
            \draw[beamgreen] (0, 0) -- (0, 1/3);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\Pr{E^c}$}}] at (0, 1/3) {};
          }

          \onslide<4->{
            \draw[beamgreen] (1, 0) -- (1, 2/3);
            \node[mydot, label={[label distance=-5pt]180:{$\scriptstyle\Pr{E}$}}] at (1, 2/3) {};
          }
        \end{scope}

        \draw (0, 3pt) -- (0, -3pt) node[below] {$0$};
        \draw (1, 3pt) -- (1, -3pt) node[below] {$1$};
      \end{tikzpicture}
    \]
    \onslide<5->{The expected value of $\chi_E$ is $\E{\chi_E}=\Pr{E}$.}
  \end{definition}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $X$ is a sum of indicators
    \[
      X = \chi_{E_1}+\chi_{E_2}+\dotsb+\chi_{E_n}
    \]
    \onslide<2->{Then $X$ counts the number of $\Set{E_1, E_2,\dotsc, E_n}$ that
      occur and}
    \begin{align*}
      \onslide<3->{\E{X}}
      &\onslide<3->{=} \onslide<4->{\E{\chi_{E_1}}+\E{\chi_{E_2}}+\dotsb+\E{\chi_{E_n}}} \\
      &\onslide<4->{=} \onslide<5->{\Pr{E_1}+\Pr{E_2}+\dotsb+\Pr{E_n}}
    \end{align*}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{Problem}
    How many of $N$ students collect their own cap after graduation?
  \end{exampleblock}
  \onslide<2->
  \begin{block}{Solution}
    The number of matches is counted by
    \[
      X = \chi_{E_1}+\chi_{E_2}+\dotsb+\chi_{E_N}
    \]
    where $E_i$ is the event the $i$th student collects their own cap. \onslide<3->Then
    \[
      \onslide<3->{\E{X} =}
      \onslide<4->{\Pr{E_1}+\Pr{E_2}+\dotsb+\Pr{E_N} =}
      \onslide<5->{\frac{1}{N}+\frac{1}{N}+\dotsb+\frac{1}{N} =}
      \onslide<6->{1}
    \]
    \onslide<7->{On average, one person selects their own cap.}
  \end{block}
\end{frame}


\subsection{Functions of Random Variables}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\displaystyle\E{f(X)}=\sum_{x\in\Range(X)}f(x)\cdot\Pr{X=x}$
  \end{theorem}

  \pause
  \begin{alertblock}{Warning}
    $\E{f(x)}\neq f(\E{X}))$ in general
  \end{alertblock}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number of heads after flipping a fair coin three times.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=1
        ]

        \newcommand{\myBinom}[1]{\binom{3}{####1}\cdot(\sfrac{1}{2})^{####1}\cdot(1-\sfrac{1}{2})^{3-####1}}
        \begin{scope}[yscale=3]
          \onslide<2->{
            \draw[beamgreen] (0, 0) -- (0, 1/8);
            \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\frac{1}{8}$}}] at ( 0, 1/8) {};
          }

          \onslide<3->{
            \draw[beamgreen] (1, 0) -- (1, 3/8);
            \node[mydot, label={[label distance=-5pt]120:{$\scriptstyle\frac{3}{8}$}}] at ( 1, 3/8) {};
          }

          \onslide<4->{
            \draw[beamgreen] (2, 0) -- (2, 3/8);
            \node[mydot, label={[label distance=-5pt]60:{$\scriptstyle\frac{3}{8}$}}] at ( 2, 3/8) {};
          }

          \onslide<5->{
            \draw[beamgreen] (3, 0) -- (3, 1/8);
            \node[mydot, label={[label distance=-5pt]90:{$\scriptstyle\frac{1}{8}$}}] at ( 3, 1/8) {};
          }
        \end{scope}

        \draw[<->] (-1, 0) -- (4, 0);

        \foreach \i in {0,...,3}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \node[beamblue, above= 0mm of current bounding box.north] {$X\sim\Binomial(3, \sfrac{1}{2})$};
      \end{tikzpicture}
    \]
    \onslide<6->{Here, we have}
    \begin{align*}
      \onslide<6->{\E{X}}   &\onslide<6->{=} \onslide<7->{1\cdot(\sfrac{3}{8})+2\cdot(\sfrac{3}{8})+3\cdot(\sfrac{1}{8}) = \sfrac{3}{2}} \\
      \onslide<8->{\E{X^2}} &\onslide<8->{=} \onslide<9->{1^2\cdot(\sfrac{3}{8})+2^2\cdot(\sfrac{3}{8})+3^2\cdot(\sfrac{1}{8}) = 3}
    \end{align*}
    \onslide<10->{Note that $\E{X^2}\neq\E{X}^2$.}
  \end{example}
\end{frame}


\section{Named Distributions}
\subsection{$X\sim\Unif\Set{a,\dotsc,b}$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Unif\Set{a,\dotsc,b}$. Then $\E{X}=\frac{(a+b)}{2}$.
  \end{theorem}

  \pause
  \begin{example}
    Let $X$ be the roll of a fair die, so
    \[
      X\sim\Unif\Set{1, 2, 3, 4, 5, 6}
    \]
    \pause The expected value of $X$ is $\E{X}=\pause\frac{7}{2}$.
  \end{example}
\end{frame}


\subsection{$X\sim\Bernoulli(p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Bernoulli(p)$. Then $\E{X}=p$.
  \end{theorem}

  \onslide<2->
  \begin{proof}
    This follows immediately from the distribution
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        ]

        \draw[<->] (-1, 0) -- (2, 0);

        \begin{scope}[yscale=2]
          \onslide<3->{
            \draw[beamgreen] (0, 0) -- (0, 1/3);
            \node[mydot, label={[label distance=-5pt]180:{$(1-p)$}}] at (0, 1/3) {};
          }

          \onslide<4->{
            \draw[beamgreen] (1, 0) -- (1, 2/3);
            \node[mydot, label={[label distance=-5pt]180:{$p$}}] at (1, 2/3) {};
          }
        \end{scope}

        \draw (0, 3pt) -- (0, -3pt) node[below] {$0$};
        \draw (1, 3pt) -- (1, -3pt) node[below] {$1$};
      \end{tikzpicture}
    \]
    \onslide<5->{We have $\E{X}=0\cdot(1-p)+1\cdot p=p$.\qedhere}
  \end{proof}
\end{frame}


\subsection{$X\sim\Binomial(n, p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Binomial(n, p)$. Then $\E{X}=n\cdot p$.
  \end{theorem}

  \onslide<2->
  \begin{proof}
    Note that $X$ is a sum of indicators
    \[
      X = \chi_{E_1}+\chi_{E_2}+\dotsb+\chi_{E_n}
    \]
    where $E_i$ is the event of success on the $i$th Bernoulli
    trial. \onslide<3->Hence
    \[
      \onslide<4->{\E{X} =}
      \onslide<5->{\Pr{E_1}+\Pr{E_2}+\dotsb+\Pr{E_n} =}
      \onslide<6->{p+p+\dotsb+p =}
      \onslide<7->{n\cdot p\qedhere}
    \]
  \end{proof}
\end{frame}


\subsection{$X\sim\Geometric(p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Geometric(p)$. Then $\E{X}=\frac{1}{p}$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{Problem}
    Suppose there are $N$ coupons and we collect one per day. What is the
    expected number of days until we collect all the coupons?
  \end{exampleblock}

  \onslide<2->
  \begin{block}{Solution}
    Let $T_k$ be the number of days between collecting the $(k-1)$st coupon and
    the $k$th coupon.
    \begin{align*}
      \onslide<3->{T_k} &\onslide<3->{\sim}\onslide<4->{\Geometric\pair*{\frac{N-(k-1)}{N}}} & \onslide<5->{\E{T_k}} &\onslide<5->{=} \onslide<6->{\frac{N}{N-(k-1)}}
    \end{align*}
    \onslide<7->{This gives}
    \begin{align*}
      \onslide<8->{\E{\mathtt{days}}}
      &\onslide<9->{=} \onslide<10->{\E{T_1}+\E{T_2}+\dotsb+\E{T_N}} \\
      &\onslide<10->{=} \onslide<11->{\sfrac{N}{N}+\sfrac{N}{(N-1)}+\sfrac{N}{(N-2)}+\dotsb+\sfrac{N}{2}+\sfrac{N}{1}} \\
      &\onslide<11->{=} \onslide<12->{N\cdot \pair{\sfrac{1}{N}+\sfrac{1}{(N-1)}+\dotsb+\sfrac{1}{2}+1}}
    \end{align*}
  \end{block}
\end{frame}


\subsection{$X\sim\NB(r, p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\NB(r, p)$. Then $\E{X}=\dfrac{r}{p}$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number of rolls of a fair die until $\vcdice{2}$ is rolled
    four times. \onslide<2->{Then $X\sim\NB(4, \sfrac{1}{6})$ and}
    \[
      \onslide<3->{\E{X} =}
      \onslide<4->{\frac{4}{\sfrac{1}{6}} =}
      \onslide<5->{24}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    $X\sim\NB(10, \sfrac{18}{37})$ counts the number of spins to achieve ten reds.
    \begin{gather*}
      \begin{align*}
        \roulette[2/3] && \onslide<2->{\E{X} =} \onslide<3->{\frac{10}{\sfrac{18}{37}} =} \onslide<4->{\frac{185}{9}}
      \end{align*}
    \end{gather*}
  \end{example}
\end{frame}


\subsection{$X\sim\Poisson(\mu)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Poisson(\mu)$. Then $\E{X}=\mu$.
  \end{theorem}

\end{frame}


\end{document}
