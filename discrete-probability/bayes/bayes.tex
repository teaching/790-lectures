\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzcards}
\usepackage{fitzcoin}
\renewcommand{\dukelogo}{../../dukemath.pdf}

\usepackage{caption}
\captionsetup{skip=0pt}
\usepackage{booktabs}
\usepackage{epsdice}
\newcommand\vcdice[1]{\vcenter{\hbox{\epsdice{#1}}}}

\newcommand{\myTableTitle}[4]{
  \begin{minipage}{1.0\linewidth}
    \begin{table}
      \centering
      \caption*{\mybold{#1}}%
      \begin{tabular}{c#2}
        \toprule
        \myotherbold{$\Omega$} & #3 \\
        \midrule
        \myotherbold{$\mu$} & #4 \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{minipage}%
}
\newcommand{\myTable}[3]{
  \begin{minipage}{1.0\linewidth}
    \begin{table}
      \centering
      % \caption*{\myotherbold{#1}}
      \begin{tabular}{c#1}
        \toprule
        \myotherbold{$\Omega$} & #2 \\
        \midrule
        \myotherbold{$\mu$} & #3 \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{minipage}
}


\title{Bayes' Rule and Indepencence}
\subtitle{Math 790-92}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Conditional Probability}
\subsection{Motivation}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Knowledge of an event occuring can change the distribution.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose we roll a fair die.
    \myTableTitle{no information given}{cccc}{
      $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$
    }{
      $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$
    }
    \onslide<2->{The distribution changes when new information is given.}
    \onslide<2->{
      \myTableTitle{given roll $\neq 4$}{lll}{
        $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$
      }{
        $\frac{1}{3}\onslide<3->{=\dfrac{\frac{1}{4}}{1-\frac{1}{4}}}$ & $\frac{1}{3}\onslide<4->{=\dfrac{\frac{1}{4}}{1-\frac{1}{4}}}$ & $\frac{1}{3}\onslide<5->{=\dfrac{\frac{1}{4}}{1-\frac{1}{4}}}$
      }
    }
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How does ``new information'' influence our models?
  \end{block}

  \pause
  \begin{block}{Answer}
    \emph{Conditional probability} incorporates this new information.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(7132)
  n = 7
  v = vector(Partitions(n, length=4).random_element()) / n
  p1, p2, p3, p4 = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myDie}{
    \begin{table}
      \centering
      \caption*{\mybold{no information given}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ \\
        \midrule
        \myotherbold{$\mu$} & $\sage{p1}$ & $\sage{p2}$ & $\sage{p3}$ & $\sage{p4}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \newcommand{\myGiven}{
    \begin{table}
      \centering
      \caption*{\mybold{given roll is $\neq4$}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$                     & $\vcdice{2}$                     & $\vcdice{3}$ \\
        \midrule
        \myotherbold{$\mu$}    & $\dfrac{\sage{p1}}{1-\sage{p4}}$ & $\dfrac{\sage{p2}}{1-\sage{p4}}$ & $\dfrac{\sage{p3}}{1-\sage{p4}}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    Consider the experiment of rolling a biased die.
    \noindent
    \begin{minipage}{.5\textwidth}
      \myDie
    \end{minipage}% This must go next to Y
    \begin{minipage}{.5\textwidth}
      \onslide<6->{\myGiven}
    \end{minipage}
    \onslide<2->{The probability the roll is not $\vcdice{1}$ is}
    \[
      \onslide<2->{\Pr{\Set{\vcdice{1}}^c}=}
      \onslide<3->{\sage{p2}+\sage{p3}+\sage{p4}=}
      \onslide<4->{\sage{p2+p3+p4}}
    \]
    \onslide<5->{The model changes if it is known the roll is not $\vcdice{4}$.}
    \[
      \onslide<7->{\Pr{\Set{\vcdice{1}}^c\given\Set{\vcdice{4}}^c} =}
      \onslide<8->{\dfrac{\sage{p2}}{1-\sage{p4}}+\dfrac{\sage{p3}}{1-\sage{p4}} =}
      \onslide<9->{\dfrac{\Pr{\Set{\vcdice{1}}^c\cap\Set{\vcdice{4}}^c}}{\Pr{\Set{\vcdice{4}}^c}}=}
      \onslide<10->{\sage{p2/(1-p4)+p3/(1-p4)}}
    \]
  \end{example}

\end{frame}


\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{conditional probability} of $A$ given $B$ is
    \[
      \Pr{A\given B}=\frac{\Pr{A\cap B}}{\Pr{B}}
    \]
    \pause The notation $A\mid B$ means ``$A$ given $B$.''
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myh}{\texttt{heads}}
  \begin{exampleblock}{Problem}
    Flip a coin twice. What is $\Pr{\Set{\heads\heads}\given\heads\geq 1}$?
  \end{exampleblock}

  \onslide<2->
  \begin{block}{Solution}
    Our probability space is
    \begin{table}
      \centering
      % \caption*{\myotherbold{no information given}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\heads\heads$ & $\heads\tails$ & $\tails\heads$ & $\tails\tails$ \\
        \midrule
        \myotherbold{$\mu$} & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
    \onslide<3->{By definition, our conditional probability is}
    \begin{gather*}
      \onslide<4->{\Pr{\Set{\heads\heads}\given\heads\geq 1}
        =} \onslide<5->{\frac{\Pr{\Set{\heads\heads}\cap\heads\geq 1}}{\Pr{\heads\geq1}}
        =} \onslide<6->{\frac{\Pr{\Set{\heads\heads}\cap\Set{\heads\heads, \heads\tails, \tails\heads}}}{\Pr{\Set{\heads\heads, \heads\tails, \tails\heads}}}
        =} \onslide<7->{\frac{\frac{1}{4}}{\frac{3}{4}}
        =} \onslide<8->{\frac{1}{3}}
    \end{gather*}
  \end{block}

\end{frame}


\subsection{Multiplication Rule}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Multiplication Rule]
    $\Pr{A\cap B}=\Pr{A\given B}\cdot\Pr{B}$ and $\Pr{A\cap B}=\Pr{B\given A}\cdot\Pr{A}$
  \end{theorem}
  \pause
  \begin{proof}
    By definition, we have
    \begin{align*}
      \Pr{A\given B} &= \frac{\Pr{A\cap B}}{\Pr{B}} & \Pr{B\given A} &= \frac{\Pr{B\cap A}}{\Pr{A}}
    \end{align*}
    \pause Solving these equations for $\Pr{A\cap B}$ gives the result.
  \end{proof}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball] (B1) {};
      \node[myball, right= 0.5cm of B1] (B2) {};
      \node[myball, right= 0.5cm of B2] (B3) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B1] (R1) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B2] (R2) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $A$};

    \end{tikzpicture}
  }
  \newcommand{\myB}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball, fill=red] (R1) {};
      \node[myball, fill=red, right= 0.5cm of R1] (R2) {};
      \node[myball, fill=red, right= 0.5cm of R2] (R3) {};
      \node[myball, fill=red, right= 0.5cm of R3] (R4) {};
      \node[myball, fill=red, above right= 0.25cm and 0.25cm of R4] (R5) {};
      \node[myball, above right= 0.25cm and 0.25cm of R1] (B1) {};
      \node[myball, above right= 0.25cm and 0.25cm of R2] (B2) {};
      \node[myball, above right= 0.25cm and 0.25cm of R3] (B3) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2) (R3) (R4) (R5)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $B$};
    \end{tikzpicture}
  }
  \newcommand{\myred}{\texttt{red}}
  \newcommand{\urnA}{\texttt{Urn A}}
  \newcommand{\urnB}{\texttt{Urn B}}
  \begin{example}
    A ball is chosen at random from one of these two urns.
    \begin{align*}
      \myA && \myB
    \end{align*}
    \onslide<2->{The probability of drawing a red ball from Urn $A$ is}
    \begin{gather*}
      \onslide<2->{\Pr{\myred\cap\urnA}
        =}
      \onslide<3->{\overset{\onslide<4->{\sage{2/5}}}{\Pr{\myred\given\urnA}}
        \cdot
        \overset{\onslide<5->{\sage{1/2}}}{\Pr{\urnA}}
        =}
      \onslide<6->{\sage{2/5/2}}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Average Conditional Probabilities}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Rule of Average Conditional Probabilities]
    Every partition $\Omega=E_1\sqcup\dotsb\sqcup E_n$ gives
    \begin{gather*}
      \Pr{A}
      = \Pr{A\given E_1}\cdot\Pr{E_1}
      + \dotsb
      + \Pr{A\given E_n}\cdot\Pr{E_n}
    \end{gather*}
    In particular, we have
    $\Pr{A}=\Pr{A\given E}\cdot\Pr{E}+\Pr{A\given E^c}\cdot\Pr{E^c}$.
  \end{theorem}

  \onslide<2->
  \begin{proof}
    The partition $A=(A\cap E_1)\sqcup \dotsb\sqcup(A\cap E_n)$ gives
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\Pr{A}}
        &\onslide<3->{=} \onslide<4->{\Pr{A\cap E_1}+\dotsb+\Pr{A\cap E_n}} \\
        &\onslide<4->{=} \onslide<5->{\Pr{A\given E_1}\cdot\Pr{E_1}+\dotsb+\Pr{A\given E_n}\cdot\Pr{E_n}}
      \end{align*}
    \end{gather*}
    \onslide<5->{The second equation follows from the partition
      $\Omega=E\sqcup E^c$.}
  \end{proof}


\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball] (B1) {};
      \node[myball, right= 0.5cm of B1] (B2) {};
      \node[myball, right= 0.5cm of B2] (B3) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B1] (R1) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B2] (R2) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $A$};

    \end{tikzpicture}
  }
  \newcommand{\myB}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball, fill=red] (R1) {};
      \node[myball, fill=red, right= 0.5cm of R1] (R2) {};
      \node[myball, fill=red, right= 0.5cm of R2] (R3) {};
      \node[myball, fill=red, right= 0.5cm of R3] (R4) {};
      \node[myball, fill=red, above right= 0.25cm and 0.25cm of R4] (R5) {};
      \node[myball, above right= 0.25cm and 0.25cm of R1] (B1) {};
      \node[myball, above right= 0.25cm and 0.25cm of R2] (B2) {};
      \node[myball, above right= 0.25cm and 0.25cm of R3] (B3) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2) (R3) (R4) (R5)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $B$};
    \end{tikzpicture}
  }
  \newcommand{\myred}{\texttt{red}}
  \newcommand{\urnA}{\texttt{Urn A}}
  \newcommand{\urnB}{\texttt{Urn B}}
  \begin{example}
    A ball is chosen at random from one of these two urns.
    \begin{align*}
      \myA && \myB
    \end{align*}
    \onslide<2->{The partition $\Omega=\urnA\sqcup\urnB$ gives}
    \begin{gather*}
      \onslide<3->{\Pr{\myred}=}
      \onslide<4->{\overset{\onslide<5->{\sage{2/5}}}{\Pr{\myred\given\urnA}}\cdot\overset{\onslide<6->{\sage{1/2}}}{\Pr{\urnA}}+\overset{\onslide<7->{\sage{5/8}}}{\Pr{\myred\given\urnB}}\cdot\overset{\onslide<8->{\sage{1/2}}}{\Pr{\urnB}}}
      \onslide<9->{= \sage{2/5/2+5/8/2}}
    \end{gather*}

  \end{example}

\end{frame}



\begin{sagesilent}
  test_accuracy = 99/100
  pos_given_sick = test_accuracy
  pos_given_healthy = 1-test_accuracy
  sick = 1/10000
  healthy = 1-sick
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\sick}{\texttt{sick}}
  \newcommand{\healthy}{\texttt{healthy}}
  \begin{exampleblock}{Problem}
    A disease infects $\sage{sick}$ people and testing methods have an accuracy
    rate of $\sage{test_accuracy}$. What is $\Pr{\sick\given+}$?
  \end{exampleblock}

  \onslide<2->
  \begin{block}{Solution}
    Applying the rule of average conditional probabilities gives
    \begin{gather*}
      \begin{align*}
        \Pr{\sick\given+}
        &= \onslide<3->{\frac{\Pr{\sick\cap+}}{\Pr{+}}} \\
        &\onslide<3->{=} \onslide<4->{\frac{\Pr{+\given\sick}\cdot\Pr{\sick}}{\Pr{+\given\sick}\cdot\Pr{\sick}+\Pr{+\given\healthy}\cdot\Pr{\healthy}}} \\
        &\onslide<4->{=} \onslide<5->{\oldfrac{(\sage{pos_given_sick})\cdot(\sage{sick})}{(\sage{pos_given_sick})\cdot(\sage{sick})+(\sage{pos_given_healthy})\cdot(\sage{healthy})}} \\
        &\onslide<5->{=} \onslide<6->{\sage{(pos_given_sick*sick)/(pos_given_sick*sick+pos_given_healthy*healthy)}}
      \end{align*}
    \end{gather*}
  \end{block}

\end{frame}


\section{Bayes' Rule}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Bayes' Rule]
    $\Pr{A\given B}=\dfrac{\Pr{B\given A}\cdot\Pr{A}}{\Pr{B}}$
  \end{theorem}

  \pause
  \begin{proof}
    $\Pr{A\given B}=\pause\dfrac{\Pr{A\cap B}}{\Pr{B}}=\pause\dfrac{\Pr{B\given A}\cdot\Pr{A}}{\Pr{B}}$
  \end{proof}

\end{frame}



\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myA}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball] (B1) {};
      \node[myball, right= 0.5cm of B1] (B2) {};
      \node[myball, right= 0.5cm of B2] (B3) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B1] (R1) {};
      \node[myball, fill=red, below right= 0.25cm and 0.25cm of B2] (R2) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $A$};

    \end{tikzpicture}
  }
  \newcommand{\myB}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , rounded corners
      , ultra thick
      , myball/.style={circle, thick, draw=black, fill=beamblue},
      ]

      \node[myball, fill=red] (R1) {};
      \node[myball, fill=red, right= 0.5cm of R1] (R2) {};
      \node[myball, fill=red, right= 0.5cm of R2] (R3) {};
      \node[myball, fill=red, right= 0.5cm of R3] (R4) {};
      \node[myball, fill=red, above right= 0.25cm and 0.25cm of R4] (R5) {};
      \node[myball, above right= 0.25cm and 0.25cm of R1] (B1) {};
      \node[myball, above right= 0.25cm and 0.25cm of R2] (B2) {};
      \node[myball, above right= 0.25cm and 0.25cm of R3] (B3) {};

      \node[draw, fit=(B1) (B2) (B3) (R1) (R2) (R3) (R4) (R5)] {};

      \node[above] at (current bounding box.north) {\color{beamblue}Urn $B$};
    \end{tikzpicture}
  }
  \newcommand{\myred}{\texttt{red}}
  \newcommand{\urnA}{\texttt{Urn A}}
  \newcommand{\urnB}{\texttt{Urn B}}
  \begin{example}
    A ball is chosen at random from one of these two urns.
    \begin{align*}
      \myA && \myB
    \end{align*}
    \onslide<2->{We previously found $\Pr{\myred}=\frac{41}{80}$.} \onslide<3->{Bayes' Rule then implies}
    \begin{gather*}
      \onslide<4->{\Pr{\urnA\given\myred}=}
      \onslide<5->{\frac{\Pr{\myred\given\urnA}\cdot\Pr{\urnA}}{\Pr{\myred}}=}
      \onslide<6->{\frac{(\frac{2}{5})\cdot(\frac{1}{2})}{\frac{41}{80}}=}
      \onslide<7->{\sage{(2/5)*(1/2)/(41/80)}}
    \end{gather*}
  \end{example}

\end{frame}



\begin{sagesilent}
  clear, rain = 60, 30
  snow = 100-clear-rain
  attend_clear, attend_rain, attend_snow = 80, 40, 20
  P = lambda N: N/100
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\clear}{\texttt{clear}}
  \newcommand{\rain}{\texttt{rain}}
  \newcommand{\snow}{\texttt{snow}}
  \newcommand{\attend}{\texttt{attend}}
  \begin{example}
    The probability you attend class tomorrow depends on the weather.
    \noindent
    \begin{minipage}{.5\textwidth}
      \begin{table}
        \centering
        \caption*{\mybold{Weather Forecast}}%
        \begin{tabular}{cccc}
          \toprule
          Clear & Rain & Snow \\
          \midrule
          $\sage{clear}\%$ & $\sage{rain}\%$ & $\sage{snow}\%$ \\
          \bottomrule
        \end{tabular}
      \end{table}
    \end{minipage}% This must go next to Y
    \begin{minipage}{.5\textwidth}
      \begin{align*}
        \onslide<2->{\Pr{\attend\given\clear}} &\onslide<2->{= \sage{attend_clear}\%} \\
        \onslide<3->{\Pr{\attend\given\rain}} &\onslide<3->{= \sage{attend_rain}\%} \\
        \onslide<4->{\Pr{\attend\given\snow}} &\onslide<4->{= \sage{attend_snow}\%}
      \end{align*}
    \end{minipage}
    \onslide<5->{Suppose you attend class tomorrow.} \onslide<6->{Bayes' Rule then gives}
    \begin{gather*}
      \begin{align*}
        \onslide<6->{\Pr{\rain\given\attend}}
        &\onslide<6->{=} \onslide<7->{\frac{\Pr{\attend\given\rain}\cdot\Pr{\rain}}{\Pr{\attend}}} \\
        &\onslide<7->{=} \onslide<8->{\frac{\Pr{\attend\given\rain}\cdot\Pr{\rain}}{\Pr{\attend\given\clear}\cdot\Pr{\clear}+\Pr{\attend\given\rain}\cdot\Pr{\rain}+\Pr{\attend\given\snow}\cdot\Pr{\snow}}} \\
        &\onslide<8->{=} \onslide<9->{\oldfrac{(\sage{P(attend_rain)})\cdot(\sage{P(rain)})}{(\sage{P(attend_clear)})\cdot(\sage{P(clear)})+(\sage{P(attend_rain)})\cdot(\sage{P(rain)})+(\sage{P(attend_snow)})\cdot(\sage{P(snow)})}} \\
        &\onslide<9->{=} \onslide<10->{\sage{(P(attend_rain))*(P(rain))/((P(attend_clear))*(P(clear))+(P(attend_rain))*(P(rain))+(P(attend_snow))*(P(snow)))}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\section{Independence}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Two events $A$ and $B$ are \emph{independent} if $\Pr{A} = \Pr{A\given B}$.
  \end{definition}

  \pause
  \begin{block}{Intuition}
    Independent events do not influence one another.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(4801)
  n = 17
  p = vector(Partitions(n, length=6).random_element()) / n
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myprime}{\texttt{prime}}
  \newcommand{\myodd}{\texttt{odd}}
  \begin{example}
    Flip a biased coin.
    \begin{center}
      \myTable{ccccccc}{
        $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ & $\vcdice{5}$ & $\vcdice{6}$
      }{
        $\sage{p[0]}$ & $\sage{p[1]}$ & $\sage{p[2]}$ & $\sage{p[3]}$ & $\sage{p[4]}$ & $\sage{p[5]}$
      }
    \end{center}
    \onslide<2->{Rolling a prime depends on the event that the roll is odd.}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\Pr{\myprime}} &\onslide<2->{=} \onslide<3->{\Pr{\Set{\vcdice{2}, \vcdice{3}, \vcdice{5}}}}                                                                        & \onslide<6->{\Pr{\myprime\given\myodd}} &\onslide<6->{=} \onslide<7->{\onslide<7->{\frac{\Pr{\myprime\cap\myodd}}{\Pr{\myodd}}}} \\
                                    &\onslide<3->{=} \onslide<4->{\overset{\sage{p[1]}}{\mu(\vcdice{2})}+\overset{\sage{p[2]}}{\mu(\vcdice{3})}+\overset{\sage{p[4]}}{\mu(\vcdice{5})}} &                                         &\onslide<7->{=} \onslide<8->{\frac{\Pr{\Set{\vcdice{3}, \vcdice{5}}}}{\Pr{\Set{\vcdice{1}, \vcdice{3}, \vcdice{5}}}}} \\
                                    &\onslide<4->{=} \onslide<5->{\textstyle{\sage{p[1]+p[2]+p[4]}}}                                                                                    &                                         &\onslide<8->{=} \onslide<9->{\oldfrac{\sage{p[2]}+\sage{p[4]}}{\sage{p[0]}+\sage{p[2]}+\sage{p[4]}}} \\
                                    &                                                                                                                                                   &                                         &\onslide<9->{=} \onslide<10->{\textstyle{\sage{(p[2]+p[4])/(p[0]+p[2]+p[4])}}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $A$ and $B$ are independent if and only if $\Pr{A\cap B}=\Pr{A}\cdot\Pr{B}$
  \end{theorem}

  \pause
  \begin{proof}
    $\Pr{A\cap B}=\pause\Pr{A\given B}\cdot\Pr{B}=\pause\Pr{A}$
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{Problem}
    Suppose that two events $A$ and $B$ satisfy
    \begin{align*}
      \Pr{A} &= \frac{1}{3} & \Pr{B} &= \frac{1}{2} & \Pr{A\cup B} &= \frac{2}{3}
    \end{align*}
    Determine if $A$ and $B$ are independent.
  \end{exampleblock}
  \pause
  \begin{block}{Solution}
    $
    \Pr{A\cap B}
    = \pause\overset{\frac{1}{3}}{\Pr{A}}+\overset{\frac{1}{2}}{\Pr{B}}-\overset{\frac{2}{3}}{\Pr{A\cup B}}
    = \pause\sage{1/3+1/2-2/3}
    = \pause\overset{\frac{1}{3}}{\Pr{A}}\cdot\overset{\frac{1}{2}}{\Pr{A}}
    $
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Draw two cards from a deck \emph{with replacement}.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\Pr{\myheart?}} &\onslide<2->{=} \onslide<3->{\frac{13\cdot 52}{52\cdot 52} =} \onslide<4->{\frac{1}{4}} & \onslide<5->{\Pr{?\myheart}} &\onslide<5->{=} \onslide<6->{\frac{52\cdot 13}{52\cdot 52} =} \onslide<7->{\frac{1}{4}} & \onslide<8->{\Pr{\myheart?\cap?\myheart}} &\onslide<9->{=} \onslide<10->{\Pr{\myheart\myheart} =} \onslide<11->{\frac{13\cdot 13}{52\cdot 52} =} \onslide<12->{\frac{1}{16}}
      \end{align*}
    \end{gather*}
    \onslide<13->{$\Pr{\myheart?}\cdot\Pr{?\myheart}=\Pr{\myheart?\cap?\myheart}$,
      so $\myheart?$ and $?\myheart$ are independent.}
  \end{example}

  \onslide<14->
  \begin{example}
    Draw two cards from a deck \emph{without replacement}.
    \begin{gather*}
      \begin{align*}
        \onslide<15->{\Pr{\myheart?}} &\onslide<15->{=} \onslide<16->{\frac{13\cdot 51}{52\cdot 51} =} \onslide<17->{\frac{1}{4}} & \onslide<18->{\Pr{?\myheart}} &\onslide<19->{=} \onslide<20->{\frac{1}{4}} & \onslide<21->{\Pr{\myheart?\cap?\myheart}} &\onslide<21->{=} \onslide<22->{\Pr{\myheart\myheart} =} \onslide<23->{\frac{13\cdot 12}{52\cdot 51} =} \onslide<24->{\sage{(13*12)/(52*51)}}
      \end{align*}
    \end{gather*}
    \onslide<25->{$\Pr{\myheart?}\cdot\Pr{?\myheart}\neq\Pr{\myheart?\cap?\myheart}$,
      so $\myheart?$ and $?\myheart$ are dependent.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    $\Set{E_1,\dotsc,E_n}$ is \emph{independent} if
    $\Pr{E_1\cap\dotsb \cap E_n}=\Pr{E_1}\dotsb\Pr{E_n}$
  \end{definition}

  \pause
  \begin{definition}
    $\Set{E_1,\dotsc,E_n}$ is \emph{pairwise independent} if
    $\Pr{E_i\cap E_j}=\Pr{E_i}\cdot\Pr{E_j}$
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\same}{\texttt{same}}
  \begin{example}
    Flip a fair coin twice.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\Pr{\same}} &\onslide<2->{=} \onslide<3->{\Pr{\Set{\heads\heads, \tails\tails}} =} \onslide<4->{\sage{1/2}} & \onslide<5->{\Pr{\heads?}} &\onslide<5->{=} \onslide<6->{\Pr{\Set{\heads\heads, \heads\tails}} =} \onslide<7->{\sage{1/2}} & \onslide<8->{\Pr{?\heads}} &\onslide<8->{=} \onslide<9->{\Pr{\Set{\heads\heads, \tails\heads}} =} \onslide<10->{\sage{1/2}}
      \end{align*}
    \end{gather*}
    \onslide<11->{Consider the computations}
    \begin{align*}
      \onslide<12->{\Pr{\same\cap\heads?}}             &\onslide<12->{=} \onslide<13->{\Pr{\Set{\heads\heads}} =} \onslide<14->{\sage{1/4} =} \onslide<15->{\overset{\sage{1/2}}{\Pr{\same}}\cdot\overset{\sage{1/2}}{\Pr{\heads?}}} \\
      \onslide<16->{\Pr{\same\cap?\heads}}             &\onslide<16->{=} \onslide<17->{\Pr{\Set{\heads\heads}} =} \onslide<18->{\sage{1/4} =} \onslide<19->{\overset{\sage{1/2}}{\Pr{\same}}\cdot\overset{\sage{1/2}}{\Pr{?\heads}}} \\
      \onslide<20->{\Pr{\heads?\cap?\heads}}           &\onslide<20->{=} \onslide<21->{\Pr{\Set{\heads\heads}} =} \onslide<22->{\sage{1/4} =} \onslide<23->{\overset{\sage{1/2}}{\Pr{\heads?}}\cdot\overset{\sage{1/2}}{\Pr{?\heads}}} \\
      \onslide<24->{\Pr{\heads\cap\heads?\cap?\heads}} &\onslide<24->{=} \onslide<25->{\Pr{\Set{\heads\heads}} =} \onslide<26->{\sage{1/4} \neq \overset{\sage{1/2}}{\Pr{\same}}\cdot\overset{\sage{1/2}}{\Pr{\heads?}}\cdot\overset{\sage{1/2}}{\Pr{?\heads}}}
    \end{align*}
    \onslide<27->{$\Set{\same, \heads?, ?\heads}$ is pairwise independent but
      not independent.}
  \end{example}

\end{frame}


\end{document}
