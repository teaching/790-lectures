\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzcards}
\usepackage{fitzcoin}
\renewcommand{\dukelogo}{../../dukemath.pdf}

\usepackage{caption}
\captionsetup{skip=0pt}
\usepackage{booktabs}
\usepackage{epsdice}
\newcommand\vcdice[1]{\vcenter{\hbox{\epsdice{#1}}}}

\newcommand{\myTableTitle}[4]{
  \begin{minipage}{1.0\linewidth}
    \begin{table}
      \centering
      \caption*{\mybold{#1}}%
      \begin{tabular}{c#2}
        \toprule
        \myotherbold{$\Omega$} & #3 \\
        \midrule
        \myotherbold{$\mu$} & #4 \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{minipage}%
}
\newcommand{\myTable}[3]{
  \begin{minipage}{1.0\linewidth}
    \begin{table}
      \centering
      % \caption*{\myotherbold{#1}}
      \begin{tabular}{c#1}
        \toprule
        \myotherbold{$\Omega$} & #2 \\
        \midrule
        \myotherbold{$\mu$} & #3 \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{minipage}
}


\title{Random Variables}
\subtitle{Math 790-92}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Random Variables}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{random variable} is a function $X:\Omega\to\mathbb{R}$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Flip a coin three times. Let $X$ be the number of heads.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , rounded corners
        , ultra thick
        ]

        \newcommand{\myPad}{0.125mm}
        \node (hhh) {$\heads\heads\heads$};
        \node[below=\myPad of hhh] (hht) {$\heads\heads\tails$};
        \node[below=\myPad of hht] (hth) {$\heads\tails\heads$};
        \node[below=\myPad of hth] (thh) {$\tails\heads\heads$};
        \node[below=\myPad of thh] (tth) {$\tails\tails\heads$};
        \node[below=\myPad of tth] (tht) {$\tails\heads\tails$};
        \node[below=\myPad of tht] (htt) {$\heads\tails\tails$};
        \node[below=\myPad of htt] (ttt) {$\tails\tails\tails$};

        \node[above=6*\myPad of hhh] {$\Omega$};

        \node[draw, fit=(hhh) (ttt)] {};

        \begin{scope}[
          , shift={(6, -1)}
          ]

          \renewcommand{\myPad}{0.25cm}
          \onslide<2->{\node (0) {$0$};}
          \onslide<3->{\node[below=\myPad of 0] (1) {$1$};}
          \onslide<4->{\node[below=\myPad of 1] (2) {$2$};}
          \onslide<5->{\node[below=\myPad of 2] (3) {$3$};}

          \node[above=0.5*\myPad of 0] {$\Range(X)$};

          \node[draw, fit=(0) (3)] {};

        \end{scope}

        \onslide<6->{\draw[->] (hhh.east) -- (3.west) {};}
        \onslide<7->{\draw[->] (hht.east) -- (2.west) {};}
        \onslide<8->{\draw[->] (hth.east) -- (2.west) {};}
        \onslide<9->{\draw[->] (thh.east) -- (2.west) {};}
        \onslide<10->{\draw[->] (tth.east) -- (1.west) {};}
        \onslide<11->{\draw[->] (tht.east) -- (1.west) {};}
        \onslide<12->{\draw[->] (htt.east) -- (1.west) {};}
        \onslide<13->{\draw[->] (ttt.east) -- (0.west) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Many probabilities can be conveniently expressed in terms of $X$.
  \end{block}

  \onslide<2->
  \begin{example}
    Flip a coin three times. Let $X$ be the number of heads.
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\Pr{\texttt{two heads}}} &\onslide<3->{=} \onslide<4->{\Pr{X=2}} & \onslide<5->{\Pr{\texttt{two or three heads}}} &\onslide<5->{=} \onslide<6->{\Pr{X\geq 2}}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}


\subsection{Distributions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{distribution} of a random variable $X$ is
    \[
      \Dist(X)
      = \Set{\Pr{X=x}\given x\in\Range(X)}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Select three balls with replacement from this urn.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , rounded corners
        , ultra thick
        , myball/.style={circle, thick, draw=black, fill=beamblue}
        ]

        \node[myball, fill=red] (R1) {};
        \node[myball, fill=red, right= 0.5cm of R1] (R2) {};
        \node[myball, fill=red, right= 0.5cm of R2] (R3) {};
        \node[myball, fill=red, right= 0.5cm of R3] (R4) {};
        \node[myball, fill=red, above right= 0.25cm and 0.25cm of R4] (R5) {};
        \node[myball, fill=red, right= 0.5cm of R4] (R6) {};
        \node[myball, above right= 0.25cm and 0.25cm of R1] (B1) {};
        \node[myball, above right= 0.25cm and 0.25cm of R2] (B2) {};
        \node[myball, above right= 0.25cm and 0.25cm of R3] (B3) {};

        \node[draw, fit=(B1) (B2) (B3) (R1) (R2) (R3) (R4) (R5) (R6)] {};
      \end{tikzpicture}
    \]
    \onslide<2->Let $X$ be the number of red balls drawn. \onslide<3->The distribution of
    $X$ is
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt},
        , xscale=2
        ]

        \draw[<->] (-1/2, 0) -- (7/2, 0);

        \onslide<4->{\draw (0, 3pt) -- (0, -3pt) node[below] {$0$};}
        \onslide<5->{\draw (1, 3pt) -- (1, -3pt) node[below] {$1$};}
        \onslide<6->{\draw (2, 3pt) -- (2, -3pt) node[below] {$2$};}
        \onslide<7->{\draw (3, 3pt) -- (3, -3pt) node[below] {$3$};}

        \begin{scope}[
          , yscale=5
          ]

          \onslide<8->{
            \node[
            , mydot
            , label={90:{$\scriptstyle\Pr{X=0}=\pair*{\frac{3}{9}}^3=\frac{1}{27}$}}
            ]
            at (0, 1/27) {};
          }

          \onslide<9->{
            \node[
            , mydot
            , label={90:{$\scriptstyle\Pr{X=1}=3\cdot\pair*{\frac{6}{9}}\cdot\pair*{\frac{3}{9}}^2=\frac{2}{9}$}}
            ]
            at (1, 2/9) {};
          }

          \onslide<10->{
            \node[
            , mydot
            , label={90:{$\scriptstyle\Pr{X=2}=3\cdot\pair*{\frac{6}{9}}^2\cdot\pair*{\frac{3}{9}}=\frac{4}{9}$}}
            ]
            at (2, 4/9) {};
          }

          \onslide<11->{
            \node[
            , mydot
            , label={90:{$\scriptstyle\Pr{X=3}=\pair*{\frac{6}{9}}^3=\frac{8}{27}$}}
            ]
            at (3, 8/27) {};
          }
        \end{scope}


      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Roll a die until you roll $\vcdice{6}$. Let $X$ be the number of rolls, so
    \[
      \Range(X) =
      \onslide<2->{\Set{1, 2, 3, 4,\dotsc} =}
      \onslide<3->{\mathbb{N}}
    \]
    \onslide<4->{The distribution of $X$ is}
    \begin{align*}
      \onslide<4->{\Dist(X)}
      &\onslide<4->{=} \onslide<5->{\Set{\Pr{X=x}\given x\in\Range(X)}} \\
      &\onslide<5->{=} \onslide<6->{\Set{\Pr{X=x}\given x\in\mathbb{N}}} \\
      &\onslide<6->{=} \onslide<7->{\Set{\pair*{\sfrac{5}{6}}^{x-1}\cdot\pair{\sfrac{1}{6}}\given x\in\mathbb{N}}}
    \end{align*}
    \onslide<8->{We may then compute}
    \begin{align*}
      \onslide<8->{\Pr{X\leq k}}
      &\onslide<8->{=} \onslide<9->{\Pr{X=1}+\Pr{X=2}+\dotsb+\Pr{X=k}} \\
      &\onslide<9->{=} \onslide<10->{\pair*{\sfrac{1}{6}}+\pair*{\sfrac{5}{6}}\cdot\pair*{\sfrac{1}{6}}+\dotsb+\pair*{\sfrac{5}{6}}^{k-1}\cdot\pair*{\sfrac{1}{6}}} \\
      &\onslide<10->{=} \onslide<11->{1-\pair*{\sfrac{5}{6}}^k}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Functions of Random Variables}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider a diagram of functions
    \[
      \begin{tikzcd}
        \Omega\arrow[r, "X"]
        \pgfmatrixnextcell
        \mathbb{R}\arrow[r, "f"]
        \pgfmatrixnextcell\mathbb{R}
      \end{tikzcd}
    \]
    \onslide<2->{The function $Y=f\circ X$ is a random variable with
      distribution}
    \begin{align*}
      \onslide<2->{\Dist(Y)}
      &\onslide<2->{=} \onslide<3->{\Set{\Pr{Y=y}\given y\in\Range(Y)}} \\
      &\onslide<3->{=} \onslide<4->{\Set{\Pr{f(X)=y}\given y\in\Range(Y)}}
    \end{align*}
    \onslide<5->{The random variable $Y$ is a \emph{function} of $X$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose the distribution of $X$ is
    \begin{center}
      \begin{table}
        \centering
        \begin{tabular}{ccccccc}
          \toprule
          \myotherbold{$x$}        & $-3$          & $-2$          & $-1$          & $0$           & $1$           & $2$ \\
          \midrule
          \myotherbold{$\Pr{X=x}$} & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ \\
          \bottomrule
        \end{tabular}
      \end{table}
    \end{center}
    \onslide<2->The distribution of $Y=2\cdot X$ is
    \begin{center}
      \begin{table}
        \centering
        \begin{tabular}{ccccccc}
          \toprule
          \myotherbold{$y$}        & \onslide<3->{$-6$}          & \onslide<4->{$-4$}          & \onslide<5->{$-2$}          & \onslide<6->{$0$}           & \onslide<7->{$2$}           & \onslide<8->{$4$} \\
          \midrule
          \myotherbold{$\Pr{Y=y}$} & \onslide<9->{$\frac{1}{6}$} & \onslide<10->{$\frac{1}{6}$} & \onslide<11->{$\frac{1}{6}$} & \onslide<12->{$\frac{1}{6}$} & \onslide<13->{$\frac{1}{6}$} & \onslide<14->{$\frac{1}{6}$} \\
          \bottomrule
        \end{tabular}
      \end{table}
    \end{center}
    \onslide<15->The distribution of $Y=X^2$ is
    \begin{center}
      \begin{table}
        \centering
        \begin{tabular}{ccccc}
          \toprule
          \myotherbold{$y$}        & \onslide<16->{$0$}          & \onslide<17->{$1$}          & \onslide<18->{$4$}          & \onslide<19->{$9$}              \\
          \midrule
          \myotherbold{$\Pr{Y=y}$} & \onslide<20->{$\frac{1}{6}$} & \onslide<21->{$\frac{1}{3}$} & \onslide<22->{$\frac{1}{3}$} & \onslide<23->{$\frac{1}{6}$} \\
          \bottomrule
        \end{tabular}
      \end{table}
    \end{center}
  \end{example}

\end{frame}


\section{Important Distributions}
\subsection{The Uniform Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{uniform distribution} if
    \begin{align*}
      \Range(X) &= \Set{a, a+1, \dotsc, a+N} & \Pr{X=x} &= \frac{1}{N+1}
    \end{align*}
    \onslide<2->{Here, we write $X\sim\Unif\Set{a, a+1,\dotsc, a+N}$.}
  \end{definition}

  \onslide<3->
  \begin{example}
    Let $X$ be the number rolled on a fair die. \onslide<4->Then
    $X\sim\Unif\Set{1,\dotsc, 6}$. \onslide<5->
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt},
        ]

        \draw[<->] (1/2, 0) -- (13/2, 0);
        \onslide<6->{\draw (1, 3pt) -- (1, -3pt) node[below] {$1$};}
        \onslide<7->{\draw (2, 3pt) -- (2, -3pt) node[below] {$2$};}
        \onslide<8->{\draw (3, 3pt) -- (3, -3pt) node[below] {$3$};}
        \onslide<9->{\draw (4, 3pt) -- (4, -3pt) node[below] {$4$};}
        \onslide<10->{\draw (5, 3pt) -- (5, -3pt) node[below] {$5$};}
        \onslide<11->{\draw (6, 3pt) -- (6, -3pt) node[below] {$6$};}

        \onslide<12->{\node[mydot] at (1, 1) {};}
        \onslide<13->{\node[mydot] at (2, 1) {};}
        \onslide<14->{\node[mydot] at (3, 1) {};}
        \onslide<15->{\node[mydot] at (4, 1) {};}
        \onslide<16->{\node[mydot] at (5, 1) {};}
        \onslide<17->{\node[mydot] at (6, 1) {};}

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{The $\Bernoulli(p)$ Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{Bernoulli distribution} if
    \begin{align*}
      \Range(X) &= \Set{0, 1} & \Pr{X=1} &= p & \Pr{X=0} &= 1-p
    \end{align*}
    \onslide<2->{Here, we write $X\sim\Bernoulli(p)$.}
  \end{definition}

  \onslide<3->
  \begin{example}
    Flip a biased coin where $\Pr{\texttt{heads}}=\frac{3}{4}$. Then
    \begin{align*}
      \onslide<4->{\texttt{num heads}\sim\Bernoulli(\sfrac{3}{4}}) && \onslide<5->{\texttt{num tails}\sim\Bernoulli(\sfrac{1}{4})}
    \end{align*}
  \end{example}

\end{frame}


\subsection{The $\Binomial(n, p)$ Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{binomial distribution} if
    \begin{align*}
      \Range(X) &= \Set{0, 1, \dotsc, n} & \Pr{X=k} &= \binom{n}{k}\cdot p^k\cdot(1-p)^{n-k}
    \end{align*}
    \onslide<2->{Here, we write $X\sim\Binomial(n, p)$.}
  \end{definition}

  \onslide<3->
  \begin{block}{Interpretation}
    $X$ is the number of successes in $n$ \emph{independent} $\Bernoulli(p)$
    trials.
  \end{block}

  \onslide<4->
  \begin{theorem}
    $\Binomial(1, p)=\onslide<5->\Bernoulli(p)$
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\pw}{\sfrac{1}{2}}
  \newcommand{\px}{\sfrac{1}{3}}
  \newcommand{\py}{\sfrac{1}{4}}
  \newcommand{\pz}{\sfrac{1}{5}}
  \begin{example}
    Flip a biased coin $n$ times and let $X$ be the number of heads.
    \begin{table}
      \[
        \begin{array}{cccc}
          \toprule
          & \multicolumn{3}{c}{\myotherbold{\Pr{\texttt{heads}}}}\\
          \cmidrule(l){2-4}
          \myotherbold{n} & \pw & \px & \py \\
          \midrule
          2               & \Binomial(2, \pw) & \Binomial(2, \px) & \Binomial(2, \py) \\
          3               & \Binomial(3, \pw) & \Binomial(3, \px) & \Binomial(3, \py) \\
          4               & \Binomial(4, \pw) & \Binomial(4, \px) & \Binomial(4, \py) \\
          \bottomrule
        \end{array}
      \]
    \end{table}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Binomial(10, \sfrac{1}{3})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=.85
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=14]
          \node[mydot] at ( 0, 0.0173415299158326   ) {};
          \node[mydot] at ( 1, 0.0867076495791631   ) {};
          \node[mydot] at ( 2, 0.195092211553117    ) {};
          \node[mydot] at ( 3, 0.260122948737489    ) {};
          \node[mydot] at ( 4, 0.227607580145303    ) {};
          \node[mydot] at ( 5, 0.136564548087182    ) {};
          \node[mydot] at ( 6, 0.0569018950363258   ) {};
          \node[mydot] at ( 7, 0.0162576842960931   ) {};
          \node[mydot] at ( 8, 0.00304831580551745  ) {};
          \node[mydot] at ( 9, 0.000338701756168606 ) {};
          \node[mydot] at (10, 0.0000169350878084303) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Binomial(10, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=.85
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=14]
          \node[mydot] at ( 0, 0.000976562500000000) {};
          \node[mydot] at ( 1, 0.00976562500000000 ) {};
          \node[mydot] at ( 2, 0.0439453125000000  ) {};
          \node[mydot] at ( 3, 0.117187500000000   ) {};
          \node[mydot] at ( 4, 0.205078125000000   ) {};
          \node[mydot] at ( 5, 0.246093750000000   ) {};
          \node[mydot] at ( 6, 0.205078125000000   ) {};
          \node[mydot] at ( 7, 0.117187500000000   ) {};
          \node[mydot] at ( 8, 0.0439453125000000  ) {};
          \node[mydot] at ( 9, 0.00976562500000000 ) {};
          \node[mydot] at (10, 0.000976562500000000) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Binomial(10, \sfrac{4}{5})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , xscale=.85
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=14]
          \node[mydot] at ( 0, 0           ) {};
          \node[mydot] at ( 1, 0           ) {};
          \node[mydot] at ( 2, 0.000073728 ) {};
          \node[mydot] at ( 3, 0.000786432 ) {};
          \node[mydot] at ( 4, 0.005505024 ) {};
          \node[mydot] at ( 5, 0.0264241152) {};
          \node[mydot] at ( 6, 0.088080384 ) {};
          \node[mydot] at ( 7, 0.201326592 ) {};
          \node[mydot] at ( 8, 0.301989888 ) {};
          \node[mydot] at ( 9, 0.268435456 ) {};
          \node[mydot] at (10, 0.1073741824) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\subsection{The $\Geometric(p)$ Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{geometric distribution} if
    \begin{align*}
      \Range(X) &= \Set{1, 2, 3, \dotsc}=\mathbb{N} & \Pr{X=k} &= (1-p)^{k-1}\cdot p
    \end{align*}
    \onslide<2->{Here, we write $X\sim\Geometric(p)$.}
  \end{definition}

  \onslide<3->
  \begin{block}{Interpretation}
    $X$ is the num of independent $\Bernoulli(p)$ trials until $1$st success.
  \end{block}

\end{frame}

\begin{sagesilent}
  set_random_seed(841)
  n = 50
  v = vector(Partitions(n, length=6).random_element()) / n
  p1, p2, p3, p4, p5, p6 = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFair}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ & $\vcdice{5}$ & $\vcdice{6}$ \\
        \midrule
        \myotherbold{$\mu$} & $\sage{p1}$ & $\sage{p2}$ & $\sage{p3}$ & $\sage{p4}$ & $\sage{p5}$ & $\sage{p6}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    Consider the experiment of rolling a biased die.
    \begin{center}
      \myFair
    \end{center}
    Let $X_i$ be the number of rolls until you roll $i$.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{X_1} &\onslide<2->{\sim}\onslide<3->{\Geometric(\textstyle{\sage{p1}})} & \onslide<4->{X_2} &\onslide<4->{\sim}\onslide<5->{\Geometric(\textstyle{\sage{p2}})} & \onslide<6->{X_3} &\onslide<6->{\sim}\onslide<7->{\Geometric(\textstyle{\sage{p3}})}\\
        \onslide<8->{X_4} &\onslide<8->{\sim}\onslide<9->{\Geometric(\textstyle{\sage{p4}})} & \onslide<10->{X_5} &\onslide<10->{\sim}\onslide<11->{\Geometric(\textstyle{\sage{p5}})} & \onslide<12->{X_6} &\onslide<12->{\sim}\onslide<13->{\Geometric(\textstyle{\sage{p6}})}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Geometric(\sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        % \matrix [draw,below left] at (current bounding box.north east) {
        % \node [mydot,label=right:Foo] {}; \\
        % \node [mydot,label=right:Bar] {}; \\
        % \node [mydot,label=right:Baz] {}; \\
        % };

        \draw[<->] (1/2, 0) -- (10.5, 0);

        \pgfmathsetmacro{\p}{1/2}
        \foreach \i in {1,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
          \begin{scope}[yscale=10]
            \node[mydot] at (\i, \p) {};
          \end{scope}
          \pgfmathparse{\p*(1-\p)}
          \global\let\p=\pgfmathresult
        }

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Geometric(\sfrac{1}{3})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        % \matrix [draw,below left] at (current bounding box.north east) {
        % \node [mydot,label=right:Foo] {}; \\
        % \node [mydot,label=right:Bar] {}; \\
        % \node [mydot,label=right:Baz] {}; \\
        % };

        \draw[<->] (1/2, 0) -- (10.5, 0);

        \pgfmathsetmacro{\p}{1/3}
        \foreach \i in {1,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
          \begin{scope}[yscale=10]
            \node[mydot] at (\i, \p) {};
          \end{scope}
          \pgfmathparse{\p*(1-\p)}
          \global\let\p=\pgfmathresult
        }

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Geometric(\sfrac{4}{5})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        % \matrix [draw,below left] at (current bounding box.north east) {
        % \node [mydot,label=right:Foo] {}; \\
        % \node [mydot,label=right:Bar] {}; \\
        % \node [mydot,label=right:Baz] {}; \\
        % };

        \draw[<->] (1/2, 0) -- (10.5, 0);

        \pgfmathsetmacro{\p}{4/5}
        \foreach \i in {1,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
          \begin{scope}[yscale=5]
            \node[mydot] at (\i, \p) {};
          \end{scope}
          \pgfmathparse{\p*(1-\p)}
          \global\let\p=\pgfmathresult
        }

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\subsection{The $\NB(r, p)$ Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{negative binomial distribution} if
    \begin{gather*}
      \begin{align*}
        \Range(X) &= \Set{r, r+1, r+2, \dotsc} & \Pr{X=k} &= \binom{k-1}{r-1}\cdot p^r\cdot(1-p)^{k-r}
      \end{align*}
    \end{gather*}
    \pause Here, we write $X\sim\NB(r, p)$.
  \end{definition}

  \pause
  \begin{block}{Interpretation}
    $X$ is the number of trials in a sequence of independent $\Bernoulli(p)$
    trials before $r$ successes occur.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number of rolls of a fair die until $\vcdice{2}$ is rolled
    four times. \pause Then $X\sim\pause\NB(4, \sfrac{1}{6})$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\NB(1, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 1/2) {};
          \node[mydot] at (1, 1/4) {};
          \node[mydot] at (2, 1/8) {};
          \node[mydot] at (3, 1/16) {};
          \node[mydot] at (4, 1/32) {};
          \node[mydot] at (5, 1/64) {};
          \node[mydot] at (6, 1/128) {};
          \node[mydot] at (7, 1/256) {};
          \node[mydot] at (8, 1/512) {};
          \node[mydot] at (9, 1/1024) {};
          \node[mydot] at (10, 1/2048) {};
        \end{scope}


      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\NB(2, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 1/4) {};
          \node[mydot] at (1, 1/4) {};
          \node[mydot] at (2, 3/16) {};
          \node[mydot] at (3, 1/8) {};
          \node[mydot] at (4, 5/64) {};
          \node[mydot] at (5, 3/64) {};
          \node[mydot] at (6, 7/256) {};
          \node[mydot] at (7, 1/64) {};
          \node[mydot] at (8, 9/1024) {};
          \node[mydot] at (9, 5/1024) {};
          \node[mydot] at (10, 11/4096) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\NB(3, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 1/8) {};
          \node[mydot] at (1, 3/16) {};
          \node[mydot] at (2, 3/16) {};
          \node[mydot] at (3, 5/32) {};
          \node[mydot] at (4, 15/128) {};
          \node[mydot] at (5, 21/256) {};
          \node[mydot] at (6, 7/128) {};
          \node[mydot] at (7, 9/256) {};
          \node[mydot] at (8, 45/2048) {};
          \node[mydot] at (9, 55/4096) {};
          \node[mydot] at (10, 33/4096) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\NB(4, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 1/16) {};
          \node[mydot] at (1, 1/8) {};
          \node[mydot] at (2, 5/32) {};
          \node[mydot] at (3, 5/32) {};
          \node[mydot] at (4, 35/256) {};
          \node[mydot] at (5, 7/64) {};
          \node[mydot] at (6, 21/256) {};
          \node[mydot] at (7, 15/256) {};
          \node[mydot] at (8, 165/4096) {};
          \node[mydot] at (9, 55/2048) {};
          \node[mydot] at (10, 143/8192) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\NB(5, \sfrac{1}{2})$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 1/32) {};
          \node[mydot] at (1, 5/64) {};
          \node[mydot] at (2, 15/128) {};
          \node[mydot] at (3, 35/256) {};
          \node[mydot] at (4, 35/256) {};
          \node[mydot] at (5, 63/512) {};
          \node[mydot] at (6, 105/1024) {};
          \node[mydot] at (7, 165/2048) {};
          \node[mydot] at (8, 495/8192) {};
          \node[mydot] at (9, 0.0436) {};
          \node[mydot] at (10, 0.0305) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\subsection{The $\Poisson(\mu)$ Distribution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A random variable $X$ has the \emph{Poisson distribution} if
    \begin{align*}
      \Range(X) &= \Set{0, 1, 2, \dotsc} & \Pr{X=k} &= \frac{\mu^k\cdot e^k}{k!}
    \end{align*}
    \pause Here, we write $X\sim\Poisson(\mu)$.
  \end{definition}

  \pause
  \begin{block}{Interpretation}
    $X$ is the number of successes occurring over a fixed interval of time or
    space if these events occur independently and the expected number of
    successes is $\mu$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    On average, two customers approach a cashier in any $10$-minute interval of
    time. Let $X$ be the number of customers that approach the cashier during a
    random $10$-minute interval. \pause Then $X\sim\pause\Poisson(2)$.
  \end{example}

  \pause
  \begin{example}
    Rain is falling at an average rate of $30$ drops per square inch per
    minute. Let $X$ be the number of drops that hit a given square inch per
    minute. \pause Then $X\sim\pause\Poisson(30)$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Poisson(1)$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 0.367879441171442) {};
          \node[mydot] at (1, 0.367879441171442) {};
          \node[mydot] at (2, 0.183939720585721) {};
          \node[mydot] at (3, 0.0613132401952404) {};
          \node[mydot] at (4, 0.0153283100488101) {};
          \node[mydot] at (5, 0.00306566200976202) {};
          \node[mydot] at (6, 0.000510943668293670) {};
          \node[mydot] at (7, 0.0000729919526133814) {};
          \node[mydot] at (8, 0) {};
          \node[mydot] at (9, 0) {};
          \node[mydot] at (10, 0) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Poisson(2)$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 0.135335283236613) {};
          \node[mydot] at (1, 0.270670566473225) {};
          \node[mydot] at (2, 0.270670566473225) {};
          \node[mydot] at (3, 0.180447044315484) {};
          \node[mydot] at (4, 0.0902235221577418) {};
          \node[mydot] at (5, 0.0360894088630967) {};
          \node[mydot] at (6, 0.0120298029543656) {};
          \node[mydot] at (7, 0.00343708655839016) {};
          \node[mydot] at (8, 0.000859271639597541) {};
          \node[mydot] at (9, 0.000190949253243898) {};
          \node[mydot] at (10, 0.0000381898506487796) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Poisson(3)$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 0.0497870683678639) {};
          \node[mydot] at (1, 0.149361205103592) {};
          \node[mydot] at (2, 0.224041807655388) {};
          \node[mydot] at (3, 0.224041807655388) {};
          \node[mydot] at (4, 0.168031355741541) {};
          \node[mydot] at (5, 0.100818813444924) {};
          \node[mydot] at (6, 0.0504094067224622) {};
          \node[mydot] at (7, 0.0216040314524838) {};
          \node[mydot] at (8, 0.00810151179468143) {};
          \node[mydot] at (9, 0.00270050393156048) {};
          \node[mydot] at (10, 0.000810151179468143) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Poisson(4)$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 0.0183156388887342) {};
          \node[mydot] at (1, 0.0732625555549367) {};
          \node[mydot] at (2, 0.146525111109873) {};
          \node[mydot] at (3, 0.195366814813165) {};
          \node[mydot] at (4, 0.195366814813165) {};
          \node[mydot] at (5, 0.156293451850532) {};
          \node[mydot] at (6, 0.104195634567021) {};
          \node[mydot] at (7, 0.0595403626097263) {};
          \node[mydot] at (8, 0.0297701813048632) {};
          \node[mydot] at (9, 0.0132311916910503) {};
          \node[mydot] at (10, 0.00529247667642012) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Visualization}
    $\Poisson(5)$
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , mydot/.style={circle, fill, beamblue, inner sep=2pt}
        , ultra thick
        ]

        \draw[<->] (-1/2, 0) -- (10.5, 0);

        \foreach \i in {0,...,10}{
          \draw (\i, 3pt) -- (\i, -3pt) node[below] {$\i$};
        }

        \begin{scope}[yscale=10]
          \node[mydot] at (0, 0.00673794699908547) {};
          \node[mydot] at (1, 0.0336897349954273) {};
          \node[mydot] at (2, 0.0842243374885683) {};
          \node[mydot] at (3, 0.140373895814281) {};
          \node[mydot] at (4, 0.175467369767851) {};
          \node[mydot] at (5, 0.175467369767851) {};
          \node[mydot] at (6, 0.146222808139876) {};
          \node[mydot] at (7, 0.104444862957054) {};
          \node[mydot] at (8, 0.0652780393481587) {};
          \node[mydot] at (9, 0.0362655774156437) {};
          \node[mydot] at (10, 0.0181327887078219) {};
        \end{scope}
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}



\end{document}
