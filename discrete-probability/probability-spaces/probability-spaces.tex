\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzcards}
\usepackage{fitzcoin}
\renewcommand{\dukelogo}{../../dukemath.pdf}

\usepackage{caption}
\captionsetup{skip=0pt}
\usepackage{booktabs}
\usepackage{epsdice}
\newcommand\vcdice[1]{\vcenter{\hbox{\epsdice{#1}}}}


\title{Probability Spaces}
\subtitle{Math 790-92}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Basic Definitions}
\subsection{Probability Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{probability space} is the data of a nonnegative
    $\mu:\Omega\to\mathbb{R}$ with
    \[
      \sum_{\omega\in\Omega}\mu(\omega)=1
    \]
    \pause We call $\Omega$ the \emph{sample space} and $\mu$ the
    \emph{distribution}.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Application}
    Probability spaces can be used to model experiments.\pause
    \begin{description}[$\quad\Omega$]
    \item[$\Omega$] enumerates all possible outcomes of the experiment\pause
    \item[$\mu$] measures the likelihood of each possible outcome in $\Omega$
    \end{description}
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFair}{
    \begin{table}
      \centering
      \caption*{\mybold{Fair Coin}}
      \begin{tabular}{ccc}
        \toprule
        \myotherbold{$\Omega$} & $\heads$ & $\tails$ \\  %\multicolumn{1}{c}{\color{beamblue}Root} & \multicolumn{1}{c}{\color{beamblue}Multiplicity} \\
        \midrule
        \myotherbold{$\mu$} & \onslide<3->{$\frac{1}{2}$} & \onslide<4->{$\frac{1}{2}$} \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \newcommand{\myBiased}{
    \begin{table}
      \centering
      \caption*{\mybold{Biased Coin}}
      \begin{tabular}{ccc}
        \toprule
        \myotherbold{$\Omega$} & $\heads$ & $\tails$ \\  %\multicolumn{1}{c}{\color{beamblue}Root} & \multicolumn{1}{c}{\color{beamblue}Multiplicity} \\
        \midrule
        \myotherbold{$\mu$} & \onslide<6->{$p$} & \onslide<7->{$1-p$} \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    We can model the experiment of flipping a coin.
    \noindent
    \begin{minipage}{.5\textwidth}
      \onslide<2->{\myFair}
    \end{minipage}% This must go next to Y
    \begin{minipage}{.5\textwidth}
      \onslide<5->{\myBiased}
    \end{minipage}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFair}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ & $\vcdice{5}$ & $\vcdice{6}$ \\
        \midrule
        \myotherbold{$\mu$} & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ & $\frac{1}{6}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \newcommand{\myBiased}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ & $\vcdice{5}$ & $\vcdice{6}$ \\
        \midrule
        \myotherbold{$\mu$} & $\frac{3}{12}$ & $\frac{4}{12}$ & $\frac{2}{12}$ & $\frac{1}{12}$ & $\frac{1}{12}$ & $\frac{1}{12}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    We can model the experiment of rolling a fair die.
    \begin{center}
      \myFair
    \end{center}
    \pause We can adjust the model to account for bias in the die.
    \begin{center}
      \myBiased
    \end{center}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFair}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\crdJs$ & $\crdJh$ & $\crdJc$ & $\crdJd$\\  %\multicolumn{1}{c}{\color{beamblue}Root} & \multicolumn{1}{c}{\color{beamblue}Multiplicity} \\
        \midrule
        \myotherbold{$\mu$} & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \newcommand{\myBiased}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\crdJs$ & $\crdJh$ & $\crdJc$ & $\crdJd$\\  %\multicolumn{1}{c}{\color{beamblue}Root} & \multicolumn{1}{c}{\color{beamblue}Multiplicity} \\
        \midrule
        \myotherbold{$\mu$} & \onslide<4->{$\frac{3}{8}$} & \onslide<5->{$\frac{2}{8}$} & \onslide<6->{$\frac{2}{8}$} & \onslide<7->{$\frac{1}{8}$} \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    Drawing one card from $\crdJs\crdJh\crdJc\crdJd$ is modeled by
    \begin{center}
      \onslide<2->{\myFair}
    \end{center}
    \onslide<3->Drawing one card from
    $\crdJs\crdJs\crdJs\crdJh\crdJh\crdJc\crdJc\crdJd$ is modeled by
    \begin{center}
      \myBiased
    \end{center}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say that $\mu$ is \emph{uniform} if every $\omega\in\Omega$ satisfies
    $\mu(\omega)=\frac{1}{\abs{\Omega}}$.
  \end{definition}

  \pause
  \begin{example}
    Flipping a fair coin, rolling a fair die, drawing from a fair deck.
  \end{example}
\end{frame}


\subsection{Events}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{event} in a probability space is a subset of the sample space.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Flipping a coin twice has sample space
    \[
      \Omega=\Set*{\heads\heads, \heads\tails, \tails\heads, \tails\tails}
    \]
    \pause The event that at least one heads is flipped is
    \[
      E = \Set*{\heads\heads, \heads\tails, \tails\heads}
    \]
    \pause The event that the flips are different is
    \[
      E = \Set*{\heads\tails, \tails\heads}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The sample space modeling the experiment of rolling a die is
    \[
      \Omega=\Set{\vcdice{1}, \vcdice{2}, \vcdice{3}, \vcdice{4}, \vcdice{5}, \vcdice{6}}
    \]
    \pause The event that the roll is greater than two is
    \[
      E = \Set{\vcdice{3}, \vcdice{4}, \vcdice{5}, \vcdice{6}}
    \]
    \pause The event that the roll is even is
    \[
      E = \Set{\vcdice{2}, \vcdice{4}, \vcdice{6}}
    \]
    \pause The event that the roll is prime is
    \[
      E = \Set{\vcdice{2}, \vcdice{3}, \vcdice{5}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider experiment of drawing one card from
    \[
      \crdtwoh\crdtwoh\crdtwoc\crdtwod\crdtres\crdtred\crdfours\crdfours\crdfourc\crdfivec
    \]
    \pause The sample space is
    \[
      \Omega=\Set{\crdtwoh, \crdtwoc, \crdtwod, \crdtres, \crdtred, \crdfours, \crdfourc, \crdfivec}
    \]
    \pause The event a club is drawn is
    \[
      E=\Set{\crdtwoc, \crdfourc, \crdfivec}
    \]
    \pause The event the value of the drawn card is prime is
    \[
      E=\Set{\crdtwoh, \crdtwoc, \crdtwod, \crdtres, \crdtred, \crdfivec}
    \]
    \pause The event that the drawn card is a queen is $E=\pause\varnothing$.
  \end{example}

\end{frame}


\section{Probability}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{probability} of an event $E$ is
    \[
      \mathbb{P}(E)=\sum_{\omega\in E}\mu(\omega)
    \]
    \pause This is the likelihood that an outcome in $E$ will occur.
  \end{definition}

  \pause
  \begin{block}{Observation}
    By definition, $0\leq \mathbb{P}(E)\leq 1$ and $\mathbb{P}(\Omega)=1$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The experiment of flipping a coin twice is modeled by
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccc}
        \toprule
        \myotherbold{$\Omega$} & $\heads\heads$ & $\heads\tails$ & $\tails\heads$ & $\tails\tails$ \\
        \midrule
        \myotherbold{$\mu$} & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ & $\frac{1}{4}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
    \onslide<2->The probability of flipping at least one heads is
    \[
      \mathbb{P}(\Set{\heads\heads, \heads\tails, \tails\heads})
      =
      \onslide<3->
      \overset{\onslide<4->{\frac{1}{4}}}{\mu(\heads\heads)}
      + \overset{\onslide<4->{\frac{1}{4}}}{\mu(\heads\tails)}
      + \overset{\onslide<4->{\frac{1}{4}}}{\mu(\tails\heads)}
      \onslide<5->= \frac{3}{4}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  set_random_seed(841)
  n = 50
  v = vector(Partitions(n, length=6).random_element()) / n
  p1, p2, p3, p4, p5, p6 = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myFair}{
    \begin{table}
      \centering
      % \caption*{\myotherbold{Fair Die}}
      \begin{tabular}{ccccccc}
        \toprule
        \myotherbold{$\Omega$} & $\vcdice{1}$ & $\vcdice{2}$ & $\vcdice{3}$ & $\vcdice{4}$ & $\vcdice{5}$ & $\vcdice{6}$ \\
        \midrule
        \myotherbold{$\mu$} & $\sage{p1}$ & $\sage{p2}$ & $\sage{p3}$ & $\sage{p4}$ & $\sage{p5}$ & $\sage{p6}$ \\
        \bottomrule
      \end{tabular}
    \end{table}
  }
  \begin{example}
    Consider the experiment of rolling a biased die.
    \begin{center}
      \myFair
    \end{center}
    \onslide<2->{The probability the roll is greater than two is}
    \begin{gather*}
      \onslide<2->{\mathbb{P}(\Set{\vcdice{3}, \vcdice{4}, \vcdice{5}, \vcdice{6}}) =}
      \onslide<3->{
      \overset{\onslide<4->{\sage{p3}}}{\mu(\vcdice{3})}
      + \overset{\onslide<4->{\sage{p4}}}{\mu(\vcdice{4})}
      + \overset{\onslide<4->{\sage{p5}}}{\mu(\vcdice{5})}
      + \overset{\onslide<4->{\sage{p6}}}{\mu(\vcdice{6})}
      =
      }
      \onslide<5->{\sage{p3+p4+p5+p6}}
    \end{gather*}
    \onslide<6->{The probability the roll is even is}
    \[
      \onslide<6->{\mathbb{P}(\Set{\vcdice{2}, \vcdice{4}, \vcdice{6}}) =}
      \onslide<7->{
        \overset{\onslide<8->{\sage{p2}}}{\mu(\vcdice{2})}
      + \overset{\onslide<8->{\sage{p4}}}{\mu(\vcdice{4})}
      + \overset{\onslide<8->{\sage{p6}}}{\mu(\vcdice{6})}
      =
      }
      \onslide<9->{\sage{p2+p4+p6}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the experiment
    \begin{table}
      \centering
      \caption*{\myotherbold{draw one card from $\crdtwoh\crdtwoh\crdtwoc\crdtwod\crdtres\crdtred\crdfours\crdfours\crdfourc\crdfivec$}}
      \begin{tabular}{ccccccccc}
        \toprule
        \myotherbold{$\Omega$} & $\crdtwoh$ & $\crdtwoc$ & $\crdtwod$ & $\crdtres$ & $\crdtred$ & $\crdfours$ & $\crdfourc$ & $\crdfivec$ \\
        \midrule
        \myotherbold{$\mu$} & \onslide<2->{$\frac{2}{10}$} & \onslide<3->{$\frac{1}{10}$} & \onslide<4->{$\frac{1}{10}$} & \onslide<5->{$\frac{1}{10}$} & \onslide<6->{$\frac{1}{10}$} & \onslide<7->{$\frac{2}{10}$} & \onslide<8->{$\frac{1}{10}$} & \onslide<9->{$\frac{1}{10}$} \\
        \bottomrule
      \end{tabular}
    \end{table}
    \onslide<10->{The probability of drawing an even value is}
    \begin{gather*}
      \onslide<10->{\mathbb{P}(\Set{\crdtwoh, \crdtwoc, \crdtwod, \crdfours, \crdfourc}) =}
      \onslide<11->{
        \overset{\onslide<12->{\frac{2}{10}}}{\mu(\crdtwoh)}
      + \overset{\onslide<12->{\frac{1}{10}}}{\mu(\crdtwoc)}
      + \overset{\onslide<12->{\frac{1}{10}}}{\mu(\crdtwod)}
      + \overset{\onslide<12->{\frac{2}{10}}}{\mu(\crdfours)}
      + \overset{\onslide<12->{\frac{1}{10}}}{\mu(\crdfourc)}
      =
      }
      \onslide<13->{\frac{7}{10}}
    \end{gather*}
    \onslide<14->{The probability of drawing a queen is
      $\mathbb{P}(\varnothing)=\onslide<15->{0$.}}
  \end{example}

\end{frame}


\subsection{Probability and Uniform Measure}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $\mu$ is uniform. Then $\mathbb{P}(E)=\frac{\abs{E}}{\abs{\Omega}}$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \newcommand{\numJ}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\binom{4}{3}$};
      \onslide<2->{
        \node[overlay, above left= 1mm and 3mm of a] (numJ)
        {combos of \textsf{JJJ}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (numJ.east);
        }
    }
  }
  \newcommand{\numA}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\binom{4}{2}$};
      \onslide<3->{
        \node[overlay, above right= 1mm and 3mm of a] (numA)
        {combos of \textsf{AA}};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (numA.west);
      }
    }
  }
  \newcommand{\numH}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$\binom{52}{5}$};
      \onslide<4->{
        \node[overlay, below left= 1mm and 3mm of a] (numH)
        {all five-card combos};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (numH.east);
      }
    }
  }
  \begin{example}
    The probability of being dealt $\mathsf{JJJAA}$ from a fair deck is
    \vspace{0.25cm}
    \[
      \mathbb{P}(\mathsf{JJJAA})
      = \frac{\numJ\cdot\numA}{\numH}
      \onslide<5->{= \oldfrac{\sage{binomial(4, 3)}\cdot\sage{binomial(4, 2)}}{\sage{binomial(52, 5)}} =}
      \onslide<6->{\sage{binomial(4, 3)*binomial(4, 2)/binomial(52, 5)}}
    \]
  \end{example}

\end{frame}


\subsection{The Partition Rule}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Partition Rule]
    Probability ``distributes'' across partitions
    \[
      \mathbb{P}(E_1\sqcup E_2\sqcup\dotsb\sqcup E_n)
      = \mathbb{P}(E_1)+\mathbb{P}(E_2)+\dotsb+\mathbb{P}(E_n)
    \]
  \end{theorem}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $N$ people are arranged into a line.
    \[
      \begin{tikzpicture}[
        , mydot/.style={circle, fill=black, inner sep=0pt, minimum size=0.20cm}
        ]
        \node[mydot, label=below:{end}] (p1) at (0,0) {};
        \node[mydot, right=of p1] (p2) {};
        \node[mydot, right=of p2] (p3) {};
        \node[mydot, right=of p3] (p4) {};
        \node[mydot, right=of p4] (p5) {};
        \node[mydot, right=of p5] (p6) {};
        \node[mydot, right=of p6] (p7) {};
        \node[mydot, right=of p7, label=below:{end}] (p8) {};
      \end{tikzpicture}
    \]
    \onslide<2->{Let $E$ be the event that person $A$ is next to person $B$.}
    \onslide<3->{Then}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\mathbb{P}(E\cap A_{\operatorname{middle}})} &\onslide<3->{=} \onslide<4->{\frac{2\cdot (N-2)\cdot (N-2)!}{N!}} & \onslide<5->{\mathbb{P}(E\cap A_{\operatorname{end}})} &\onslide<5->{=} \onslide<6->{\frac{2\cdot(N-2)!}{N!}}
      \end{align*}
    \end{gather*}
    \onslide<7->{Since $E = \pair{E\cap A_{\operatorname{middle}}} \sqcup \pair{E\cap A_{\operatorname{end}}}$, we have}
    \[
      \onslide<7->{\mathbb{P}(E)
        =} \onslide<8->{\mathbb{P}(E\cap A_{\operatorname{end}})+\mathbb{P}(E\cap A_{\operatorname{middle}})
        =} \onslide<9->{\frac{2}{N}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myTwo}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$2$};
      \onslide<4->{
        \node[overlay, above left= 1mm and 3mm of a] (B)
        {positions for $B$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (B.east);
      }
    }
  }
  \newcommand{\myN}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$N$};
      \onslide<3->{
        \node[overlay, above left= 6mm and 3mm of a] (N)
        {positions for $A$};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (N.east);
      }
    }
  }
  \newcommand{\myFact}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$(N-2)!$};
      \onslide<5->{
        \node[overlay, above right= 1mm and 1mm of a] (N)
        {others};
        \draw[overlay, <-, thick, shorten <=2pt] (a.north) |- (N.west);
      }
    }
  }
  \newcommand{\myOmega}{
    \tikz[
    , line join=round
    , line cap=round
    , baseline=(a.base)
    , red
    ]{
      \node[inner sep=0pt, black] (a) {$N!$};
      \onslide<6->{
        \node[overlay, below left= 1mm and 3mm of a] (N)
        {all arrangements};
        \draw[overlay, <-, thick, shorten <=2pt] (a.south) |- (N.east);
      }
    }
  }
  \newcommand{\myCirc}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , mydot/.style={circle, fill=black, inner sep=0pt, minimum size=0.20cm}
        ]
        \foreach\i in {0,...,11}{
          \node[mydot] at (\i*30:1.5) {};
        }
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Suppose $N$ people are arranged into a circle.
    \begin{gather*}
      \begin{align*}
        \myCirc && \onslide<2->{\mathbb{P}(A\texttt{ next to }B) = \frac{\myTwo\cdot \myN\cdot\myFact}{\myOmega}} \onslide<7->{= \frac{2}{N-1}}
      \end{align*}
    \end{gather*}
  \end{example}

\end{frame}


\subsection{The Complement Rule}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Complement Rule]
    $\mathbb{P}(E)=1-\mathbb{P}(E^c)$\pause
    \[
      \begin{venndiagram2sets}[
        , tikzoptions={ultra thick, rounded corners}
        , labelA=$E$
        , labelB=
        , shade=beamblue!45
        , overlap=2.4cm
        ]

        \setpostvennhook{
          \node[below right] at (0, 17/5) (Omega) {$\Omega$};
        }
      \end{venndiagram2sets}
    \]
  \end{theorem}

\end{frame}


\begin{sagesilent}
  bday = lambda N: round((1-binomial(365, N)*factorial(N)/365**N)*100, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{exampleblock}{The Birthday Problem}
    What is the probability that two of $N$ people share a birthday?
  \end{exampleblock}

  \onslide<2->
  \begin{block}{Solution}
    Using the complement rule, we have
    \begin{gather*}
      \onslide<2->{\mathbb{P}(\texttt{two share}) =}
      \onslide<3->{1-\mathbb{P}(N\texttt{ distinct}) =}
      \onslide<4->{1-\frac{P(365, N)}{365^N} =}
      \onslide<5->{1-\frac{365!}{365^N\cdot(365-N)!}}
    \end{gather*}
    \onslide<6-> This probability grows surprisingly fast! \onslide<7->
    \begin{table}
      \begin{tabular}{ccccccccc}
        \toprule
        \myotherbold{$N$}          & $5$                & $10$                & $15$                & $20$                & $25$                & $30$                & $35$\\
        \midrule
        \myotherbold{$\mathbb{P}$} & \onslide<8->{$\sage{bday(5)}\%$} & \onslide<9->{$\sage{bday(10)}\%$} & \onslide<10->{$\sage{bday(15)}\%$} & \onslide<11->{$\sage{bday(20)}\%$} & \onslide<12->{$\sage{bday(25)}\%$} & \onslide<13->{$\sage{bday(30)}\%$} & \onslide<14->{$\sage{bday(35)}\%$} \\
        \bottomrule
      \end{tabular}
    \end{table}
  \end{block}

\end{frame}


\subsection{The Inclusion-Exclusion Principle}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Inclusion-Exclusion Principle]
    $\mathbb{P}(E_1\cup E_2)=\mathbb{P}(E_1)+\mathbb{P}(E_2)-\mathbb{P}(E_1\cap E_2)$ \pause
    \[
      \begin{venndiagram2sets}[
        , tikzoptions={ultra thick, rounded corners}
        , labelA=$E_1$
        , labelB=$E_2$
        , labelAB=$\scriptscriptstyle E_1\cap E_2$
        , shade=beamblue!45
        ]

        \fillA
        \fillB

        \setpostvennhook{
          \node[above left= 0mm and 8mm of labelA] (O) {$\Omega$};
        }
      \end{venndiagram2sets}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Roll an $N$-sided die twice. \onslide<2->{Note that}
    \[
      \onslide<2->{\Set{\max=k}=(k, \leq k)\cup(\leq k, k)}
    \]
    \onslide<3->{The inclusion-exclusion principle implies that}
    \[
      \arraycolsep=1.4pt\def\arraystretch{1.85}
      \begin{array}{rclclcl}
        \onslide<3->{\mathbb{P}(\Set{\max=k})} &\onslide<3->{=}& \onslide<4->{\mathbb{P}(k, \leq k)} &\onslide<4->{+}& \onslide<4->{\mathbb{P}(\leq k, k)} &\onslide<4->{-}& \onslide<4->{\mathbb{P}((k, \leq k)\cap(\leq k, k))} \\
                                               &\onslide<5->{=}& \onslide<5->{\mathbb{P}(k, \leq k)} &\onslide<5->{+}& \onslide<5->{\mathbb{P}(\leq k, k)} &\onslide<5->{-}& \onslide<5->{\mathbb{P}(k, k)}                       \\
                                               &\onslide<6->{=}& \onslide<6->{\dfrac{k}{N}}          &\onslide<6->{+}& \onslide<6->{\dfrac{k}{N}}          &\onslide<6->{-}& \onslide<6->{\dfrac{1}{N}}                           \\
                                               &\onslide<7->{=}& \onslide<7->{\dfrac{2\,k-1}{N}}
      \end{array}
    \]

  \end{example}

\end{frame}



\begin{sagesilent}
  set_random_seed(841888)
  n = 40
  v = vector(Partitions(n, length=7).random_element())/n
  OnlyA, OnlyB, OnlyC, ABC, OnlyAB, OnlyAC, OnlyBC = v
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myVenn}{
    \begin{venndiagram3sets}[
      , tikzoptions={ultra thick, rounded corners}
      , labelA=$E_1$
      , labelB=$E_2$
      , labelC=$E_3$
      , shade=beamblue!45
      , labelOnlyA= $\sage{OnlyA}$
      , labelOnlyB= $\sage{OnlyB}$
      , labelOnlyC= $\sage{OnlyC}$
      , labelABC=   $\scriptstyle\sage{ABC}$
      % , \onslide<5->{labelOnlyAB=$\scriptstyle\sage{OnlyAB}$}
      , labelOnlyAC=$\scriptstyle\sage{OnlyAC}$
      , labelOnlyBC=$\scriptstyle\sage{OnlyBC}$
      ]

      \fillACapBNotC

      \setpostvennhook{
        \node[above left= 0mm and 8mm of labelA] (O) {$\Omega$};
      }
    \end{venndiagram3sets}
  }
  \newcommand{\myVennRed}{
    \begin{venndiagram3sets}[
      , tikzoptions={ultra thick, rounded corners}
      , labelA=$E_1$
      , labelB=$E_2$
      , labelC=$E_3$
      , shade=red!45
      , labelOnlyA= $\sage{OnlyA}$
      , labelOnlyB= $\sage{OnlyB}$
      , labelOnlyC= $\sage{OnlyC}$
      , labelABC=   $\scriptstyle\sage{ABC}$
      , labelOnlyAB=$\scriptstyle\sage{OnlyAB}$
      , labelOnlyAC=$\scriptstyle\sage{OnlyAC}$
      , labelOnlyBC=$\scriptstyle\sage{OnlyBC}$
      ]

      \fillACapB
      \fillACapC

      \setpostvennhook{
        \node[above left= 0mm and 8mm of labelA] (O) {$\Omega$};
      }
    \end{venndiagram3sets}
  }
  \begin{example}
    Suppose $\Omega=E_1\cup E_2\cup E_3$. Compute $\mathbb{P}(\texttt{shaded})$
    and $\mathbb{P}(E_1)$.
    \noindent
    \begin{minipage}{.5\textwidth}
      \[
        \alt<1-7>{\myVenn}{\myVennRed}
      \]
    \end{minipage}% This must go next to Y
    \begin{minipage}{.5\textwidth}
      \begin{gather*}
        \begin{align*}
          \onslide<2->{\mathbb{P}(\texttt{shaded})}
          &\onslide<3->{=} \onslide<4->{1-\mathbb{P}(\texttt{shaded}^c)} \\
          &\onslide<4->{=} \onslide<5->{1-\pair*{\sage{OnlyA}-\sage{OnlyB}-\sage{OnlyC}-\sage{ABC}-\sage{OnlyAC}-\sage{OnlyBC}}} \\
          &\onslide<5->{=} \onslide<6->{\sage{OnlyAB}} \\
          \onslide<7->{\mathbb{P}(E_1)}
          &\onslide<7->{=} \onslide<9->{\mathbb{P}(E_1\setminus \texttt{red})+\mathbb{P}(\texttt{red})} \\
          &\onslide<9->{=} \onslide<10->{\sage{OnlyA}+\sage{OnlyAB}+\sage{OnlyAC}+\sage{ABC}} \\
          &\onslide<10->{=} \onslide<11->{\sage{OnlyA+OnlyAB+OnlyAC+ABC}}
        \end{align*}
      \end{gather*}
    \end{minipage}
  \end{example}


\end{frame}







\end{document}
