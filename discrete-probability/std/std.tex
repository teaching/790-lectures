\documentclass[usenames, dvipsnames]{beamer}

\usepackage{fitzmath}
\usepackage{fitzslides}
\usepackage{fitzcards}
\usepackage{fitzcoin}
\usepackage{fitzroulette}
\renewcommand{\dukelogo}{../../dukemath.pdf}

\usepackage{caption}
\captionsetup{skip=0pt}
\usepackage{booktabs}
\usepackage{epsdice}
\newcommand\vcdice[1]{\vcenter{\hbox{\epsdice{#1}}}}
\DeclareSIUnit\Fahrenheit{\degree F}


\title{Variance and Standard Deviation}
\subtitle{Math 790-92}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Motivation}
\subsection{Measuring Spread}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Bet \usd{1000} on green and on red.\onslide<2->
    \begin{gather*}
      \begin{align*}
        \roulette[5/10] &&  \onslide<3->{\E{\texttt{green}} = \E{\texttt{red}} \simeq-\usd{27.0270}}
      \end{align*}
    \end{gather*}
    \onslide<4->{Still, betting on green is ``riskier'' than betting on red.}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How can we quantify the amount of ``variability'' in a distribution?
  \end{block}

  \begin{block}{Answer}<2->
    Measure how $X$ ``deviates'' from $\E{X}$ \emph{on average}.
  \end{block}

\end{frame}


\section{Variance and Standard Deviation}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Variance]
    $\Var(X)=\E{(X-\E{X})^2}$
  \end{definition}

  \begin{definition}[Standard Deviation]<2->
    $\SD(X)=\sqrt{\Var(X)}$
  \end{definition}

  \begin{block}{Interpretation}<3->
    $\Var(X)$ and $\SD(X)$ measure how ``spread out'' $\Dist(X)$ is.
  \end{block}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Var(X)\geq 0$ and $\SD(X)\geq 0$
  \end{theorem}

  \begin{theorem}<2->
    $\Var(X)=\E{X^2}-\E{X}^2$
  \end{theorem}
  \begin{proof}<3->\renewcommand{\qedsymbol}{}
    $
    \begin{aligned}
      \onslide<3->{\Var(X)}
      &\onslide<3->{=} \onslide<4->{\E{(X-\E{X})^2}} \\
      &\onslide<4->{=} \onslide<5->{\E{X^2-2\,\E{X}\cdot X+\E{X}^2}} \\
      &\onslide<5->{=} \onslide<6->{\E{X^2}-2\,\E{X}\cdot\E{X}+\E{\E{X}^2}} \\
      &\onslide<6->{=} \onslide<7->{\E{X^2}-2\,\E{X}^2+\E{X}^2} \\
      &\onslide<7->{=} \onslide<8->{\E{X^2}-\E{X}^2\hfill{\color{beamblue}{\Box}}}
    \end{aligned}
    $
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEQ}{
    \begin{aligned}
      \onslide<3->{\Var(X)} &\onslide<3->{=} \onslide<4->{\E{X^2}-\E{X}^2} \\
      &\onslide<4->{=} \onslide<5->{(\usd{35000})^2\cdot\Pr{\texttt{win}}+(-\usd{1000})^2\cdot\Pr{\texttt{lose}}-(-\usd{1000/37})^2} \\
      &\onslide<5->{=} \onslide<6->{(\usd{35000})^2\cdot\pair*{\frac{1}{37}}+(-\usd{1000})^2\cdot\pair*{\frac{36}{37}}-(-\usd{1000/37})^2} \\
      &\onslide<7->{\approx 34080350.620} \\
      \onslide<8->{\SD(X)} &\onslide<8->{=} \onslide<9->{\sqrt{\Var(X)}} \\
      &\onslide<10->{\approx 5837.837}
    \end{aligned}
  }
  \begin{example}
    Bet \usd{1000} on green and let $X$ denote your winnings.\onslide<2->
    \begin{gather*}
      \begin{align*}
        \roulette[5/10] && \myEQ
      \end{align*}
    \end{gather*}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myEQ}{
    \begin{aligned}
      \onslide<3->{\Var(X)} &\onslide<3->{=} \onslide<4->{\E{X^2}-\E{X}^2} \\
      &\onslide<4->{=} \onslide<5->{(\usd{1000})^2\cdot\Pr{\texttt{win}}+(-\usd{1000})^2\cdot\Pr{\texttt{lose}}-(-\usd{1000/37})^2} \\
      &\onslide<5->{=} \onslide<6->{(\usd{1000})^2\cdot\pair*{\frac{18}{37}}+(-\usd{1000})^2\cdot\pair*{\frac{19}{37}}-(-\usd{1000/37})^2} \\
      &\onslide<7->{\approx 999269.539} \\
      \onslide<8->{\SD(X)} &\onslide<8->{=} \onslide<9->{\sqrt{\Var(X)}} \\
      &\onslide<10->{\approx 999.634}
    \end{aligned}
  }
  \begin{example}
    Bet \usd{1000} on red and let $X$ denote your winnings.\onslide<2->
    \begin{gather*}
      \begin{align*}
        \roulette[5/10] && \myEQ
      \end{align*}
    \end{gather*}
    \onslide<11->{Betting on red is quantifiably ``safer'' than betting on
      green.}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $X$ be the number of heads after flipping one coin.
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\Var(X)}
        &\onslide<2->{=} \onslide<3->{\E{X^2}-\E{X}^2} \\
        &\onslide<3->{=} \onslide<4->{0^2\cdot\Pr{X=0}+1^2\cdot\Pr{X=1}-\pair*{0\cdot\Pr{X=0}+1\cdot\Pr{X=1}}^2} \\
        &\onslide<4->{=} \onslide<5->{0\cdot\pair{\sfrac{1}{2}}+1\cdot\pair{\sfrac{1}{2}}-\pair*{0\cdot\pair{\sfrac{1}{2}}+1\cdot\pair{\sfrac{1}{2}}}^2} \\
        &\onslide<5->{=} \onslide<6->{\frac{1}{2}-\frac{1}{4}} \\
        &\onslide<6->{=} \onslide<7->{\frac{1}{4}}
      \end{align*}
    \end{gather*}
    \onslide<8->{It follows that
      $\SD(X)=\sqrt{\Var(X)}=\sqrt{\frac{1}{4}}=\frac{1}{2}$.}
  \end{example}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The variance of a linear combination of random variables is
    \begin{gather*}
      \Var(c_1\cdot X_1+c_2\cdot X_2)
      = c_1^2\cdot\Var(X_1)+c_2^2\cdot\Var(X_2)+2ab\cdot\pair{\E{XY}-\E{X}\cdot\E{Y}}
    \end{gather*}
    \pause In particular, we have
    \[
      \Var(c\cdot X+d) = c^2\cdot\Var(X)
    \]
    \pause which gives $\SD(c\cdot X+d)=\pause\abs{c}\cdot\SD(X)$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myC}{\SI{}{\degree C}}
  \newcommand{\myF}{\SI{}{\Fahrenheit}}
  \begin{example}
    The relationship between Fahrenheit and Celsius is
    \[
      \myF=\frac{9}{5}\cdot\myC+32
    \]
    \onslide<2->{Expectation is then}
    \[
      \onslide<2->{\E{\myF} = \frac{9}{5}\cdot\E{\myC}+32}
    \]
    \onslide<3->{Variance and standard deviation are then}
    \begin{align*}
      \onslide<3->{\Var(\myF)} &\onslide<3->{=} \onslide<4->{\pair*{\frac{9}{5}}^2\cdot\Var(\myC)} & \onslide<4->{\SD(\myF)} &\onslide<4->{=} \onslide<5->{\frac{9}{5}\cdot\SD(\myC)}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Flip a coin $n$ times. \onslide<2->{Then}
    \[
      \onslide<2->{\texttt{heads}=n-\texttt{tails}}
    \]
    \onslide<3->{Expectation is given by}
    \[
      \onslide<3->{\E{\texttt{heads}} = n-\E{\texttt{tails}}}
    \]
    \onslide<4->{Variance and standard devation are then}
    \begin{gather*}
      \begin{align*}
        \onslide<4->{\Var(\texttt{heads})} &\onslide<4->{=} \onslide<5->{\Var(n-\texttt{tails}) =} \onslide<6->{\Var(\texttt{tails})} & \onslide<7->{\SD(\texttt{heads})} &\onslide<8->{=} \onslide<9->{\SD(\texttt{tails})}
      \end{align*}
    \end{gather*}

  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Var(\chi_E)=\Pr{E}\cdot\Pr{E^c}$
  \end{theorem}
  \begin{proof}<2->\renewcommand{\qedsymbol}{}
    $
    \begin{aligned}
      \onslide<2->{\Var(\chi_E)}
      &\onslide<2->{=} \onslide<3->{\E{\chi_E^2}-\E{\chi_E}^2} \\
      &\onslide<3->{=} \onslide<4->{0^2\cdot\Pr{E^c}+1^2\cdot\Pr{E}-\Pr{E}^2} \\
      &\onslide<4->{=} \onslide<5->{\Pr{E}\cdot(1-\Pr{E})} \\
      &\onslide<5->{=} \onslide<6->{\Pr{E}\cdot\Pr{E^c}\hfill{\color{beamblue}{\Box}}}
    \end{aligned}
    $
  \end{proof}
\end{frame}


\section{Named Distributions}
\subsection{$X\sim\Unif\Set{a,\dotsc,b}$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Unif\Set{a,\dotsc,b}$. Then $\Var(X)=\dfrac{(b-a+1)^2-1}{12}$.
  \end{theorem}

  \begin{example}<2->
    Let $X$ be the roll of a fair die, so $X\sim\Unif\Set{1,\dotsc, 6}$. \onslide<3->Then
    \[
      \Var(X) = \onslide<4->\frac{(6-1+1)^2-1}{12} = \onslide<5->\frac{35}{12}
    \]
    \onslide<6->It follows that $\SD(X)=\sqrt{\frac{35}{12}}$.
  \end{example}
\end{frame}


\subsection{$X\sim\Bernoulli(p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Bernoulli(p)$. Then $\Var(X)=p\cdot(1-p)$.
  \end{theorem}

  \begin{example}<2->
    Flip a coin with $\Pr{\texttt{heads}}=\frac{1}{3}$, so
    $\texttt{heads}\sim\Bernoulli(\frac{1}{3})$. \onslide<3->{Then}
    \[
      \onslide<3->{\Var(\texttt{heads}) =}
      \onslide<4->{\pair*{\frac{1}{3}}\cdot\pair*{1-\frac{1}{3}} =}
      \onslide<5->{\frac{2}{9}}
    \]
    \onslide<6->{It follows that $\SD(\texttt{heads})=\sqrt{\frac{2}{9}}$.}
  \end{example}

\end{frame}


\subsection{$X\sim\Binomial(n, p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Binomial(n, p)$. Then $\Var(X)=n\cdot p\cdot(1-p)$.
  \end{theorem}

  \begin{example}<2->
    Flip a fair coin nine times. \onslide<3->{Then
    $\texttt{heads}\sim\Binomial(9,\frac{1}{2})$ so}
    \[
      \onslide<4->{\Var(\texttt{heads}) =}
      \onslide<5->{9\cdot(\frac{1}{2})\cdot(1-\frac{1}{2}) =}
      \onslide<6->{\frac{9}{4}}
    \]
    \onslide<7->{It follows that
      $\SD(\texttt{heads})=\sqrt{\frac{9}{4}}=\frac{3}{2}$.}
  \end{example}

\end{frame}


\subsection{$X\sim\Geometric(p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Geometric(p)$. Then $\Var(X)=\dfrac{1-p}{p^2}$.
  \end{theorem}

  \begin{example}<2->
    Flip a coin until heads, so $X\sim\Geometric(\frac{1}{2})$. \onslide<3->{Then}
    \[
      \onslide<3->{\Var(X) =} \onslide<4->{\frac{1-\frac{1}{2}}{(\frac{1}{2})^2} =} \onslide<5->{2}
    \]
    \onslide<6->{It follows that $\SD(X)=\sqrt{2}$.}
  \end{example}

\end{frame}


\subsection{$X\sim\NB(r, p)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\NB(r, p)$. Then $\Var(X)=\dfrac{r\cdot(1-p)}{p^2}$.
  \end{theorem}

  \begin{example}<2->
    Let $X$ be the number of rolls of a die until nine $\vcdice{5}$.
    \[
      \onslide<3->{\Var(X) =} \onslide<4->{\frac{9\cdot(1-\frac{1}{6})}{(\frac{1}{6})^2} =} \onslide<5->{270}
    \]
    \onslide<6->{It follows that $\SD(X)=\sqrt{270}$.}
  \end{example}

\end{frame}


\subsection{$X\sim\Poisson(\mu)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $X\sim\Poisson(\mu)$. Then $\Var(X)=\mu$.
  \end{theorem}
\end{frame}



\end{document}
