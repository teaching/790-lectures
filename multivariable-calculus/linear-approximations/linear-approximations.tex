\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning, shapes.geometric}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}
\usepackage{cool}
% \usepackage{datetime}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}

\newcommand{\mysage}[2]{{#1\sage{#2}}}




\title{Linear Approximations}
\subtitle{Math 790-92.03}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}


\section{The Jacobian Derivative}
\subsection{Definition}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}^{m}$. The \emph{Jacobian derivative} of
    $f$ is the $m\times n$ matrix
    \[
    \arraycolsep=.33cm\def\arraystretch{2.5}
    Df=
    \left[
      \begin{array}{*4{>{\displaystyle}c}p{5cm}}
        \pderiv{f_1}{x_1} & \pderiv{f_1}{x_2} & \dotsb & \pderiv{f_1}{x_n} \\
        \pderiv{f_2}{x_1} & \pderiv{f_2}{x_2} & \dotsb & \pderiv{f_2}{x_n} \\
        \vdots            & \vdots            & \ddots & \vdots            \\
        \pderiv{f_m}{x_1} & \pderiv{f_m}{x_2} & \dotsb & \pderiv{f_m}{x_n} 
      \end{array}\right]
    \]
    where $f_1,f_2,\dotsc,f_m$ are the component functions of $f$.
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Notation}
    The notations \pause
    \[
    Df
    \pause= Jf
    \pause= J_f
    \pause= \frac{\partial(f_1,f_2,\dotsc,f_m)}{\partial(x_1,x_2,\dotsc,x_n)}
    \pause= f^\prime
    \]\pause%
    are common.
  \end{block}

  \pause
  \begin{block}{Note}
    The rows of $Df$ are the gradients of the component functions
    \[
    Df
    =
    \begin{bmatrix}
      \nabla f_1 \\ \nabla f_2 \\ \vdots \\ \nabla f_m
    \end{bmatrix}
    \]
  \end{block}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y')
  f1 = x ** 2 * y
  f2 = x * y - sin(y)
  f = matrix.column([f1, f2])
  latex.matrix_column_alignment('c')
\end{sagesilent}
\newcommand{\myDf}{
  \left[
    \begin{array}{*2{>{\displaystyle}c}p{5cm}}
      \pderiv{f_1}{x} & \pderiv{f_1}{y} \\ \\
      \pderiv{f_2}{x} & \pderiv{f_2}{y} 
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f:\mathbb{R}^{2}\to \mathbb{R}^{2}$  be defined by
    \[    
    f(x,y)=\sage{f}
    \]%
    \onslide<2->{The component functions of $f$ are}
    \begin{align*}
      \onslide<3->{f_1(x,y) &= \sage{f1}} &
      \onslide<4->{f_2(x,y) &= \sage{f2}}
    \end{align*}
    \onslide<5->{The Jacobian derivative of $f$ is thus}
    \[
    \onslide<6->{Df =} 
    \onslide<7->{\myDf =}
    \onslide<8->{\sage{jacobian(f, (x, y))}}
    \]
  \end{example}
\end{frame}


\begin{sagesilent}
  var('r varphi theta')
  g1 = r * sin(theta) * cos(varphi)
  g2 = r * sin(theta) * sin(varphi)
  g3 = r * cos(theta)
  g = matrix.column([g1, g2, g3])
\end{sagesilent}
\newcommand{\mygs}{
  \left[
    \begin{array}{c}
      g_1(r, \theta, \varphi)\\ g_2(r, \theta, \varphi)\\ g_3(r, \theta, \varphi)
    \end{array}\right]}
\newcommand{\myDg}{
  \left[
    \begin{array}{*3{>{\displaystyle}c}p{5cm}}
      \pderiv{g_1}{r} & \pderiv{g_1}{\theta} & \pderiv{g_1}{\varphi}\\ \\
      \pderiv{g_2}{r} & \pderiv{g_2}{\theta} & \pderiv{g_2}{\varphi}\\ \\
      \pderiv{g_3}{r} & \pderiv{g_3}{\theta} & \pderiv{g_3}{\varphi}
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $g:\mathbb{R}^{3}\to \mathbb{R}^{3}$ be defined by
    \[
    g(r, \theta, \varphi) = \sage{g} \pause= \mygs
    \]\pause%
    The Jacobian derivative of $g$ is
    \begingroup
    \tiny
    \[
    Dg(r, \theta, \varphi) =\pause \myDg =\pause \sage{jacobian(g, (r, theta, varphi))}
    \]
    \endgroup
  \end{example}
\end{frame}


\begin{sagesilent}
  var('x1 x2 x3')
  h1 = x1
  h2 = 3*x1**2*x3
  h3 = 4 * x2 **2 - 22 * x3
  h4 = x3 * sin(x1)
  h = matrix.column([h1, h2, h3, h4])
\end{sagesilent}
\newcommand{\myhs}{
  \left[
    \begin{array}{c}
      h_1(x_1, x_2, x_3) \\ h_2(x_1, x_2, x_3) \\ h_3(x_1, x_2, x_3) \\ h_4(x_1, x_2, x_3) 
    \end{array}\right]}
\newcommand{\myDh}{
  \left[
    \begin{array}{ccc}
      \pderiv{h_1}{x_1} & \pderiv{h_1}{x_2} & \pderiv{h_1}{x_3} \\ \\
      \pderiv{h_2}{x_1} & \pderiv{h_2}{x_2} & \pderiv{h_2}{x_3} \\ \\
      \pderiv{h_3}{x_1} & \pderiv{h_3}{x_2} & \pderiv{h_3}{x_3} \\ \\
      \pderiv{h_4}{x_1} & \pderiv{h_4}{x_2} & \pderiv{h_4}{x_3} 
    \end{array}\right]}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $h:\mathbb{R}^{3}\to \mathbb{R}^{4}$ be defined by
    \[
    h(x_1,x_2,x_3) = \sage{h} \pause= \myhs
    \]\pause%
    The Jacobian derivative of $h$ is
    \[
    Dh =\pause \myDh =\pause \sage{jacobian(h, (x1, x2, x3))}
    \]
  \end{example}
\end{frame}

\subsection{Curves in $\mathbb{R}^{n}$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{parameterized curve in $\mathbb{R}^{n}$} is a function
    $\vv{r}:\mathbb{R}\to \mathbb{R}^{n}$.
  \end{definition}

  \pause
  \begin{block}{Idea}
    A curve is represented geometrically by its image.
  \end{block}
\end{frame}

\begin{sagesilent}
  var('t')
  r = matrix.column([cos(t), sin(t)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The image of the parameterized curve $\vv{r}(t)=\sage{r}$ is\pause
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=2]
      \draw[ultra thick, <->] (-3/2, 0) -- (3/2, 0);
      \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);

      \draw[very thick, blue] (0, 0) circle (1);
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\begin{sagesilent}
  var('t')
  r = matrix.column([cos(3*t), sin(2*t)])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The image of the parameterized curve $\vv{r}=\sage{r}$ is\pause
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=2]
      \draw[ultra thick, <->] (-3/2, 0) -- (3/2, 0);
      \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);

      \draw[very thick, blue, domain=0:2*pi, samples=200, variable=\t] 
      plot({cos(3*\t r)},{sin(2*\t r)});
    \end{tikzpicture}
    \]
  \end{example}
\end{frame}


\newcommand{\myDr}{
  \begin{bmatrix}
    x_1^\prime(t)\\ x_2^\prime(t)\\ \vdots\\ x_n^\prime(t)
  \end{bmatrix}}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Suppose that $\vv{r}:\mathbb{R}\to \mathbb{R}^{n}$ is a parameterized curve
    of the form
    \[
    \vv{r}(t) = \langle x_1(t), x_2(t), \dotsc, x_n(t)\rangle
    \]\pause%
    The Jacobian derivative of $\vv{r}$ is 
    \[
    \vv{r}^\prime(t)=\myDr
    \]
  \end{block}

  \pause
  \begin{definition}
    The vector $\vv{r}^\prime(t_0)$ is the \emph{velocity vector of $\vv{r}(t)$
      at time $t=t_0$}.\pause%
    The length $\norm{\vv{r}^\prime(t_0)}$ is the \emph{speed of $\vv{r}(t)$ at
      time $t=t_0$}.
  \end{definition}
\end{frame}


\begin{sagesilent}
  var('t')
  r = matrix.column([cos(3*t), sin(2*t)])
  v = jacobian(r, t)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that for $\vv{r}(t)=\mysage{\tiny}{r}$ we have
    $\vv{r}^\prime(t)=\onslide<2->{\mysage{\tiny}{v}$. }%
    \onslide<3->{Thus}
    \begin{align*}
      \onslide<4->{\vv{r}(\nicefrac{\pi}{12})        &=} \onslide<5->{\sage{r(t=pi/12)}} &
      \onslide<6->{\vv{r}^\prime(\nicefrac{\pi}{12}) &=} \onslide<7->{\sage{v(t=pi/12)}}
    \end{align*}
    \onslide<8->{Geometrically, this velocity vector is depicted by}
    \begin{columns}[onlytextwidth]
      \column{.5\textwidth}
      \[
      \begin{tikzpicture}[line join=round, line cap=round, scale=1]
        \onslide<9->{
          \draw[ultra thick, <->] (-3/2, 0) -- (3/2, 0);%
          \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);%

          \draw[very thick, blue, domain=0:2*pi, samples=200, variable=\t]
          plot({cos(3*\t r)},{sin(2*\t r)});
        }

        \pgfmathsetmacro{\px}{sqrt(2)/2} \pgfmathsetmacro{\py}{1/2}
        \pgfmathsetmacro{\vx}{-sqrt(2)*3/2} \pgfmathsetmacro{\vy}{sqrt(3)}
        \pgfmathsetmacro{\qx}{sqrt(2)/2-sqrt(2)*3/2}
        \pgfmathsetmacro{\qy}{1/2+sqrt(3)}

        \coordinate (P) at (\px, \py);%
        \coordinate (Q) at (\qx, \qy);%

        \onslide<11->{
          \draw[very thick, violet, ->] (P) -- (Q) node [right]
          {\scriptsize$\vv{r}^\prime(\nicefrac{\pi}{12})$};%
        }

        \onslide<10->{
          \node at (P) {\textbullet};
        }
      \end{tikzpicture}
      \]
      \column{.5\textwidth}%
      \onslide<12->{Note that the speed of $\vv{r}(t)$ at time
        $t=\nicefrac{\pi}{12}$ is
        \[
        \norm{\vv{r}^\prime(\nicefrac{\pi}{12})}=\sqrt{\nicefrac{15}{2}}
        \]}
    \end{columns}
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $\vv{r}(t)$ be a curve. The function
    \[
    \vv{L}(t)=\vv{r}(t_0)+\vv{r}^\prime(t_0)(t-t_0)
    \]
    is a parameterization of the \emph{line tangent to $\vv{r}(t)$ at time
      $t=t_0$}.
  \end{definition}

  \pause
  \begin{block}{Note}
    Note that $\vv{L}(t_0)=\vv{r}(t_0)$ and $\vv{L}^\prime(t_0)=\vv{r}^\prime(t_0)$.
  \end{block}

  \pause
  \begin{block}{Idea}
    The tangent line $\vv{L}(t)$ can be used to estimate the values of
    $\vv{r}(t)$ for times near $t=t_0$.
  \end{block}
\end{frame}


\begin{sagesilent}
  var('t')
  r = matrix.column([2*cos(t), sin(2*t)])
  v = jacobian(r, t)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The line tangent to the curve $\vv{r}(t)=\mysage{\scriptsize}{r}$ at
    $t=\nicefrac{2\,\pi}{3}$ is
    \[
    \onslide<2->{\vv{L}(t)
      =\sage{r(t=2*pi/3)}+\sage{v(t=2*pi/3)}(t-\nicefrac{2\,\pi}{3})}
    \]
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1]
      \onslide<3->{
        \draw[ultra thick, <->] (-3, 0) -- (3, 0);%
        \draw[ultra thick, <->] (0, -2) -- (0, 2);%

        \draw[very thick, blue, domain=0:2*pi, samples=200, variable=\t]
        plot({2*cos(\t r)},{sin(2*\t r)});%
      }

      \pgfmathsetmacro{\px}{-1}%
      \pgfmathsetmacro{\py}{-sqrt(3)/2}%
      \pgfmathsetmacro{\vx}{-sqrt(3)}%
      \pgfmathsetmacro{\vy}{-1}%
      \pgfmathsetmacro{\qx}{-1-sqrt(3)}%
      \pgfmathsetmacro{\qy}{-sqrt(3)/2-1}%

      \coordinate (P) at (\px, \py);%
      \coordinate (Q) at (\qx, \qy);%

      \onslide<6->{
        \draw[thick, red, <->] 
        ($ (P)!1.25!(Q) $) -- ($ (Q)!3.5!(P)  $) node [below=4pt] {$\vv{L}(t)$};
      }

      \onslide<5->{
        \draw[ultra thick, violet, ->] (P) -- (Q) node [right=5pt]
        {\scriptsize$\vv{r}^\prime(\nicefrac{2\,\pi}{3})$};%
      }

      \onslide<4->{
        \node at (P) {\textbullet};
      }
    \end{tikzpicture}
    \]    
  \end{example}
\end{frame}


\section{Linear Approximations}
\subsection{Local Linearization Formula}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $f:\mathbb{R}^{n}\to \mathbb{R}^{m}$. The \emph{local linearization of
      $f$ at $P\in \mathbb{R}^{n}$} is the function $L:\mathbb{R}^{n}\to
    \mathbb{R}^{m}$ defined by
    \[
    L(X)=f(P)+Df(P)\cdot(\vv{X}-\vv{P})
    \]
  \end{definition}

  \pause

  \begin{block}{Note}
    Note that $L(P)=f(P)$. 
  \end{block}

  \pause

  \begin{block}{Idea}
    The local linearization $L$ can be used to estimate the values of $f(X)$ for
    points $X$ near $P$. \pause%
    Using $L$ to approximate $f$ is called \emph{approximation by
      differentials}.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Using $L$ to Approximate $f$}
    To use $L$ to approximate $f$ near $P$, note that
    \begin{align*}
      \onslide<2->{f(\vv{P}+\Delta \vv{P})} 
      \onslide<3->{&\approx L(\vv{P}+\Delta \vv{P})} \\
      \onslide<4->{&= f(P)+Df(P)\cdot(\vv{P}+\Delta\vv{P}-\vv{P})} \\
      \onslide<5->{&= f(P)+Df(P)\cdot\Delta\vv{P}}
    \end{align*}
  \end{block}

  \begin{block}{\onslide<6->{Idea}}
    \onslide<6->{The difference $f(\vv{P}+\Delta\vv{P})-f(P)$ is approximated by
      the linear transformation associated to the matrix $Df(P)$ since
      \[
      f(\vv{P}+\Delta\vv{P})-f(P)\approx} \onslide<7->{Df(P)\cdot\Delta\vv{P}}
    \]
  \end{block}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y')
  f = matrix.column([x+y**2, x**3+5*y])
  Df = jacobian(f, (x, y))
  DfP = Df(x=1, y=1)
  DP = matrix.column([1/10, 1/5])
\end{sagesilent}
\newcommand{\myDP}{
  \begin{bmatrix}
    \nicefrac{1}{10}\\ \nicefrac{1}{5}
  \end{bmatrix}}
\newcommand{\myapprox}{
  \begin{bmatrix}
    \nicefrac{5}{2}\\ \nicefrac{73}{10}
  \end{bmatrix}}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $f:\mathbb{R}^{2}\to \mathbb{R}^{2}$ be $f(x,
    y)=\mysage{\scriptsize}{f}$. Use the local linearization of $f$ at $(1,1)$
    to approximate $f(1+\nicefrac{1}{10}, 1+\nicefrac{1}{5})$.
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Note that}
    \begin{align*}
      \onslide<3->{f(1, 1)  &=} \onslide<4->{\sage{f(x=1, y=1)}} &
      \onslide<5->{Df       &=} \onslide<6->{\sage{Df}}          &
      \onslide<7->{Df(1, 1) &=} \onslide<8->{\sage{DfP}}
    \end{align*}
    \onslide<9->{Thus}
    \begin{align*}
      \onslide<10->{f(1+\nicefrac{1}{10}, 1+\nicefrac{1}{5})}
      \onslide<11->{&\approx L(1+\nicefrac{1}{10}, 1+\nicefrac{1}{5})} \\
      \onslide<12->{&= f(1, 1) + Df(1, 1)\cdot\myDP} \\
      \onslide<13->{&= \sage{f(x=1, y=1)} + \sage{DfP}\myDP} \\
      \onslide<14->{&= \myapprox}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  var('p1 p2 y')
  q1 = 6 * p1 ** (-2) * p2 ** (3/2) * y
  q2 = 4 * p1 * p2 ** (-1) * y ** 2
  Q = matrix.column([q1, q2])
  DQ = jacobian(Q, (p1, p2, y))
  DEcon = matrix.column([1/10, 1/10, -1/10])
\end{sagesilent}
\newcommand{\myQ}{
  \begin{bmatrix}
    6\,p_1^{-2}p_2^{\nicefrac{3}{2}}\,y\\ \\ 4\,p_1p_2^{-1}y^2
  \end{bmatrix}}
\newcommand{\myDEcon}{
  \begin{bmatrix}
    \nicefrac{1}{10}\\ \nicefrac{1}{10}\\ -\nicefrac{1}{10}
  \end{bmatrix}}
\begingroup
\tiny
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the demand functions $q_1=6\,p_1^{-2}p_2^{\nicefrac{3}{2}}y$ and
    $q_2=4\,p_1p_2^{-1}y^2$ for two products with prices $p_1$ and $p_2$ where
    $y$ represents income. %
    \onslide<2->{The current prices are $p_1=6$, $p_2=9$, and income is
      currently $2$. }%
    \onslide<3->{Estimate the demand for the two products if the prices are
      increased by $\nicefrac{1}{10}$ and income is decreased by
      $\nicefrac{1}{10}$.}
  \end{example}
  \begin{block}{\onslide<4->{Solution}}
    \onslide<4->{We wish to estimate $Q(6+\nicefrac{1}{10}, 9+\nicefrac{1}{10},
      2-\nicefrac{1}{10})$ where $Q(p_1, p_2, y)=}\onslide<5->{\myQ$. }%
    \onslide<6->{To do so, note that
      \begin{align*}
        Q(6, 9, 2) &= \sage{Q(p1=6, p2=9, y=2)} &
        DQ &= \sage{DQ} &
        DQ(6, 9, 2) &= \sage{DQ(p1=6, p2=9, y=2)}
      \end{align*}}
    \onslide<7->{Our approximation is thus}
    \begin{align*}
      \onslide<8->{Q(6+\nicefrac{1}{10}, 9+\nicefrac{1}{10}, 2-\nicefrac{1}{10})}
      \onslide<9->{&\approx L(6+\nicefrac{1}{10}, 9+\nicefrac{1}{10}, 2-\nicefrac{1}{10})} \\
      \onslide<10->{&= Q(6, 9, 2) + DQ(6, 9, 2)\myDEcon} \\
      \onslide<11->{&= \sage{Q(p1=6, p2=9, y=2)} + \sage{DQ(p1=6, p2=9, y=2)}\myDEcon=\sage{Q(p1=6, p2=9, y=2) + DQ(p1=6, p2=9, y=2)*DEcon}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


% \subsection{The Tangent Plane}

% \newcommand{\myTPEX}{
%   \begin{bmatrix}
%     \pderiv{f}{x}(P) & \pderiv{f}{y}(P)
%   \end{bmatrix}}
% \newcommand{\myTPEXX}{
%   \begin{bmatrix}
%     x-a\\ y-b
%   \end{bmatrix}}
% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{block}{Note}
%     The local linearization of a function $f:\mathbb{R}^{2}\to \mathbb{R}$ at
%     $P=(a, b)$ is
%     \begin{align*}
%       \onslide<2->{L(X)}
%       \onslide<3->{&= f(P) + Df(P)\cdot(\vv{X}-\vv{P})} \\
%       \onslide<4->{&= f(P) + \myTPEX\myTPEXX} \\
%       \onslide<5->{&= f(P) + \pderiv{f}{x}(P)\cdot(x-a) + \pderiv{f}{y}(P)\cdot(y-b)} \\
%       \onslide<6->{&= f(a,b) + f_x(a,b)\cdot(x-a) + f_y(a,b)\cdot(y-b)}
%     \end{align*}
%     \onslide<7->{The graph of $L$ consists of all points of the form $(x, y, z)$ where
%       $z=L$. }%
%     \onslide<8->{This gives the relation
%       \[
%       -f_x(a,b)\cdot(x-a)-f_y(a,b)\cdot(y-b)+(z-f(a,b))=0
%       \]}
%   \end{block}
% \end{frame}


% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{definition}
%     Let $f:\mathbb{R}^{2}\to \mathbb{R}$. The \emph{tangent plane} to the graph
%     of $f$ at a point $P=(a, b, f(a, b))$ is the plane $\mathcal{P}$ with
%     point-normal equation
%     \begin{align*}
%       \vv{n}\cdot\vv{PX} &= 0 &
%       \vv{n} &= \langle -f_x(a,b), -f_y(a, b), 1\rangle
%     \end{align*}\pause
%     Note that expanding $\vv{n}\cdot\vv{PX}=0$ gives the equation
%     \[
%     -f_x(a,b)\cdot(x-a)-f_y(a,b)\cdot(y-b)+(z-f(a,b))=0
%     \]
%   \end{definition}

%   \pause
%   \begin{block}{Note}
%     When $f:\mathbb{R}^{2}\to \mathbb{R}$, approximation using local
%     linearization is often referred to as the \emph{tangent plane
%       approximation}.
%   \end{block}
% \end{frame}


% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \[
%   \begin{tikzpicture}[x={(170:1cm)},y={(55:.7cm)},z={(90:1cm)}, line join=round, line cap=round]
%     \draw[ultra thick, looseness=.6] (2.5,-2.5,-1) node[below] {$f(x,y)$}
%     to[bend left] (2.5,2.5,-1)
%     to[bend left] coordinate (mp) (-2.5,2.5,-1)
%     to[bend right] (-2.5,-2.5,-1)
%     to[bend right] coordinate (mm) (2.5,-2.5,-1)
%     -- cycle;

%     \onslide<3->{
%       \filldraw[thick, draw=teal, fill=teal!20]
%       (2.5,-2.5,0) -- (2.5,2.5,0) -- (-2.5,2.5,0) -- (-2.5,-2.5,0) -- cycle;
%     }

%     \draw[ultra thick, dashed,looseness=.6] (2.5,-2.5,-1) node[below] {$f(x,y)$}
%     to[bend left] (2.5,2.5,-1)
%     to[bend left] coordinate (mp) (-2.5,2.5,-1)
%     to[bend right] (-2.5,-2.5,-1)
%     to[bend right] coordinate (mm) (2.5,-2.5,-1)
%     -- cycle;

%     \onslide<2->{    
%       \node at (0, 0, 0) {\textbullet};
%       \node[below] at (0, 0, 0) {$P=(a, b, f(a, b))$};
%     }

%     \onslide<4->{
%       \draw[ultra thick, ->] (0,0,0) -- (0,0,3) node[right] {$\vv{n}=\langle-f_x(a,b), -f_y(a,b), 1\rangle$};
%     }
%   \end{tikzpicture}
%   \]
% \end{frame}


% \begin{sagesilent}
%   var('x y')
%   f = 3 * x * y ** 2
% \end{sagesilent}
% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{example}
%     Find an equation for the plane $\mathcal{P}$ tangent to the graph of $f(x,
%     y)=\sage{f}$ at the point $(2, 1, f(2, 1))$.
%   \end{example}
%   \pause
%   \begin{block}{Solution}
%     Note that\pause
%     \begin{align*}
%       f_x &= \sage{f.diff(x)} &
%       f_y &= \sage{f.diff(y)}
%     \end{align*}
%     It follows that\pause
%     \begin{align*}
%       f(2, 1)   &= \sage{f(x=2, y=1)}         &
%       f_x(2, 1) &= \sage{f.diff(x)(x=2, y=1)} &
%       f_y(2, 1) &= \sage{f.diff(y)(x=2, y=1)} 
%     \end{align*}\pause
%     The plane $\mathcal{P}$ is thus described by
%     \[
%     -\sage{f.diff(x)(x=2, y=1)}\cdot(x-2)-
%     \sage{f.diff(y)(x=2, y=1)}\cdot(y-1)+
%     (z-\sage{f(x=2, y=1)})=0
%     \]
%   \end{block}
% \end{frame}


\end{document}
