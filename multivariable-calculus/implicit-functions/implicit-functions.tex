\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning,
  shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\newcommand{\myUnitGoodPoint}[2]{
  \pgfmathsetmacro{\mytheta}{#1}
  \pgfmathsetmacro{\myx}{cos(\mytheta r)}
  \pgfmathsetmacro{\myy}{sin(\mytheta r)}
  \pgfmathsetmacro{\mywidth}{pi/4}
  \pgfmathsetmacro{\myDomA}{\mytheta+\mywidth}
  \pgfmathsetmacro{\myDomB}{\mytheta-\mywidth}

  \pgfmathtruncatemacro{\widthStart}{#2+1}
  \pgfmathtruncatemacro{\widthEnd}{#2+3}
  \onslide<\widthStart-\widthEnd>{
    \draw[ultra thick, violet, domain=\myDomA:\myDomB, samples=200, variable=\t, <->] 
    plot({cos(\t r)}, {sin(\t r)});}
  \onslide<#2-\widthEnd>{\filldraw (\myx, \myy) circle (1.5pt);}}

\newcommand{\myUnitBadPoint}[2]{
  \pgfmathsetmacro{\mytheta}{#1}
  \pgfmathsetmacro{\myx}{cos(\mytheta r)}
  \pgfmathsetmacro{\myy}{sin(\mytheta r)}
  \pgfmathsetmacro{\mywidth}{pi/4}
  \pgfmathsetmacro{\myDomA}{\mytheta+\mywidth}
  \pgfmathsetmacro{\myDomB}{\mytheta-\mywidth}

  \pgfmathtruncatemacro{\widthRightStart}{#2+1}
  \pgfmathtruncatemacro{\widthEnd}{#2+4}
  \onslide<\widthRightStart-\widthEnd>{
    \draw[ultra thick, red, domain=\mytheta:\myDomA, samples=200, variable=\t, ->] 
    plot({cos(\t r)}, {sin(\t r)});}

  \pgfmathtruncatemacro{\widthLeftStart}{\widthRightStart+1}
  \onslide<\widthLeftStart-\widthEnd>{
    \draw[ultra thick, red, domain=\mytheta:\myDomB, samples=200, variable=\t, ->] 
    plot({cos(\t r)}, {sin(\t r)});}
  \onslide<#2-\widthEnd>{\node at (\myx, \myy) {\textbullet};}}

\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%

\title{Implicit Functions}
\subtitle{Math 790-92.03}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={2-}]
  % \end{columns}
\end{frame}



\section{Motivation}
\subsection{The Unit Circle}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Let $P$ be a point on the unit circle. %
  \onslide<2->{Do the points ``near'' $P$ lie on the graph of a function?}
  \begin{columns}[onlytextwidth]
    \column{.7\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=2]
      \onslide<3->{
      \draw[ultra thick, <->] (-3/2, 0) -- (3/2, 0);%
      \draw[ultra thick, <->] (0, -3/2) -- (0, 3/2);%
    }

      \coordinate (O) at (0, 0);

      \coordinate (P2) at ($ ({-sqrt(3/4)}, 1/2)$ );

      \onslide<3->{
      \draw[very thick, blue] (O) circle (1);
    }

      \myUnitGoodPoint{pi/2}{4}
      \myUnitGoodPoint{pi/3}{8}
      \myUnitGoodPoint{4*pi/3}{12}

      \myUnitBadPoint{0}{16}
      \myUnitBadPoint{pi}{21}
    \end{tikzpicture}
    \]
    \column{.3\textwidth}
    \[
    \begin{array}{rl}
      \onslide<6->{P(0, 1)}                                       & \onslide<7->{\cmark}  \\
      \onslide<10->{P(\nicefrac{1}{2}, \nicefrac{\sqrt{3}}{2})}   & \onslide<11->{\cmark} \\
      \onslide<14->{P(-\nicefrac{1}{2}, -\nicefrac{\sqrt{3}}{2})} & \onslide<15->{\cmark} \\
      \onslide<19->{P(1, 0)}                                      & \onslide<20->{\xmark} \\
      \onslide<24->{P(-1, 0)}                                     & \onslide<25->{\xmark}
    \end{array}
    \]
  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Every point $(x,y)$ on the unit circle lies on the graph of a function
  $y=y(x)$, except for $(\pm1,0)$.

  \pause
  \begin{block}{Question}
    How do we find these ``bad'' points algebraically? 
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer}
    Use implicit differentiation! \pause The unit circle is defined by 
    \[
    x^2+y^2=1
    \]\pause
    differentiating both sides with respect to $x$ gives
    \[
    2\,x+2\,y\D{y}{x}=0
    \]\pause
    It follows that $\displaystyle\D{y}{x}=\pause-\frac{x}{y}$, provided that
    \pause$y\neq0$. 
  \end{block}

  \pause
  \begin{block}{Summary}
    Every point $(x, y)$ on the unit circle lies on the graph of a function
    $y=y(x)$, except for those points with $y=0$. \pause These are exactly the points
    $(\pm1, 0)$.
  \end{block}
\end{frame}



\subsection{The Folium of Descartes}


\begin{sagesilent}
  var('x y')
  F = x**2 + y**2
  G = x**3+y**3-3*x*y
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{folium of Descartes} is described by $x^3+y^3-3\,xy=0$. %
  \onslide<2->{At which points on the folium can $y$ be viewed at $y=y(x)$?}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=3/4]
      \draw[ultra thick, <->] (-4, 0) -- (2, 0);%
      \draw[ultra thick, <->] (0, -4) -- (0, 2);%

      \draw[very thick, blue, domain=-7/10:0, samples=200, variable=\t, <-]
      plot({3*\t/(1+\t*\t*\t)},{3*\t*\t / (1+\t*\t*\t)});

      \draw[very thick, blue, domain=-7/10:0, samples=200, variable=\t, <-]
      plot({3*\t*\t / (1+\t*\t*\t)}, {3*\t/(1+\t*\t*\t)});

      \draw[very thick, blue, domain=0:1, samples=200, variable=\t]
      plot({3*\t/(1+\t*\t*\t)},{3*\t*\t / (1+\t*\t*\t)});

      \draw[very thick, blue, domain=0:1, samples=200, variable=\t]
      plot({3*\t*\t / (1+\t*\t*\t)}, {3*\t/(1+\t*\t*\t)});

      \onslide<3->{
      \coordinate (O) at (0, 0);
      \filldraw (O) circle (3pt);
    }

      \onslide<4->{
      \coordinate (P) at ($ ({(2)^(2/3))}, {(2)^(1/3)})$ );
      \filldraw (P) circle (3pt);
    }
    \end{tikzpicture}
    \]
    \column{.5\textwidth}
    \onslide<5->{
    Implicit differentiation gives}
    \[
    \onslide<5->{\D{y}{x}=}\onslide<6->{-\frac{\sage{G.diff(x)}}{\sage{G.diff(y)}}}
    \]
  \end{columns}
  \onslide<7->{The points where $y\neq y(x)$ are where $3\,y^2-3\,x=0$. }%
  \onslide<8->{Thus the coordinates of the indicated points are $(0, 0)$ and
  $(2^{\nicefrac{2}{3}}, 2^{\nicefrac{1}{3}})$.}

\end{frame}


\subsection{Observations}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The unit circle is described by the level set $F(x, y)=1$ where 
    \[
    F(x, y)=\pause\sage{F}
    \]\pause
    Implicit differentiation gives
    \[
    \D{y}{x} 
    = -\frac{\sage{F.diff(x)}}{\sage{F.diff(y)}}
    = -\frac{F_x}{F_y}
    \]\pause
    The points $(x_0, y_0)$ where $y=y(x)$ are the points where $F_y(x_0,
    y_0)\neq 0$.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The folium is described by the level set $G(x, y)=0$ where 
    \[
    G(x, y)=\pause\sage{G}
    \]\pause
    Implicit differentiation gives
    \[
    \D{y}{x}
    = -\frac{\sage{G.diff(x)}}{\sage{G.diff(y)}}
    = -\frac{G_x}{G_y}
    \]\pause
    The points $(x_0, y_0)$ where $y=y(x)$ are the points where $G_y(x_0,
    y_0)\neq0$.
  \end{block}
\end{frame}


\section{Implicit Function Theorem for $G:\mathbb{R}^{2}\to \mathbb{R}$}
\subsection{Derivation}


\newcommand{\myDiag}{
  \begin{tikzcd}[column sep=small]
    \mathbb{R}\arrow{r}{g} 
    \pgfmatrixnextcell \mathbb{R}^{2}\arrow{r}{G} 
    \pgfmatrixnextcell \mathbb{R}
  \end{tikzcd}}
\newcommand{\myg}{
  \begin{bmatrix}
    x\\ y(x)
  \end{bmatrix}}
\newcommand{\myDG}{
  \begin{bmatrix}
    G_x(x_0, y_0) & G_y(x_0, y_0)
  \end{bmatrix}}
\newcommand{\myDg}{
  \begin{bmatrix}
    1 \\ y^\prime(x_0)
  \end{bmatrix}}

% \begingroup
% \small
% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   Let $(x_0, y_0)$ be a point on a level set $G(x,y)=c$. %
%   \onslide<2->{Suppose that the points $(x,y)$ near $(x_0, y_0)$ on the level
%     set $G=c$ can be viewed as $(x, y(x))$. }%
%   \onslide<3->{This gives a diagram}
%   \begin{align*}
%     \onslide<3->{\myDiag} && \onslide<4->{g(x) &= \myg} & \onslide<5->{G(g(x)) &= c}
%   \end{align*}
%   \onslide<6->{The multivariable chain rule implies that $DG(x_0, y_0)\cdot
%     Dg(x_0)=}\onslide<7->{0$. }%
%   \onslide<8->{In matrix form, this is }
%   \[
%   \onslide<8->{\myDG\myDg=0}
%   \]
%   \onslide<9->{It follows that $G_x(x_0, y_0)+G_y(x_0, y_0)y^\prime(x_0)=0$. }%
%   \onslide<10->{Assuming that $G_y(x_0, y_0)\neq 0$, this gives}
%   \[
%   \onslide<11->{y^\prime(x_0)=}\onslide<12->{-\frac{G_x(x_0, y_0)}{G_y(x_0, y_0)}}
%   \]
% \end{frame}
% \endgroup


\subsection{Statement}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $G:\mathbb{R}^{2}\to \mathbb{R}$ and suppose that $(x_0, y_0)$ is a
    point on the level set $G(x, y)=c$. \pause Suppose that $G_y(x_0, y_0)\neq
    0$. \pause Then the points on the level set $G(x,y)=c$ near $(x_0, y_0)$ are
    of the form $(x, y(x))$ where $y(x_0)=y_0$ and
    \[
    y^\prime(x_0)=-\frac{G_x(x_0, y_0)}{G_y(x_0, y_0)}
    \]
  \end{theorem}

  \pause
  \begin{block}{Note}
    We call $y$ an \emph{endogenous variable} and we call $x$ the corresponding
    \emph{exogenous variable}.
  \end{block}
\end{frame}


\subsection{Examples}

\begin{sagesilent}
  var('x y')
  G = x*y**2+2*x+exp(y)
\end{sagesilent}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\sage{G}=1$. \pause Compute $\displaystyle\D{y}{x}$ at $(0, 0)$.
  \end{example}
  \pause
  \begin{block}{Solution}
    We are interested in the level set $G=1$ where $G(x, y)=\sage{G}$. \pause
    The partials of $G$ are
    \begin{align*}
      G_x &= \sage{G.diff(x)} & G_y &= \sage{G.diff(y)}
    \end{align*}\pause
    It follows that $G_x(0, 0)=\pause\sage{G.diff(x)(x=0, y=0)}$ \pause and that $G_y(0,
    0)=\pause\sage{G.diff(y)(x=0, y=0)}$. \pause Since $G_y(0, 0)\neq 0$, the implicit
    function implies that
    \[
    \D{y}{x}
    = \pause-\frac{G_x(0, 0)}{G_y(0, 0)}
    = \pause-\frac{\sage{G.diff(x)(x=0, y=0)}}{\sage{G.diff(y)(x=0, y=0)}}
    = \pause\sage{-(G.diff(x)/G.diff(y))(x=0, y=0)}
    \]\pause
    at $(0, 0)$.
  \end{block}
\end{frame}


\subsection{Linear Approximations}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $(x_0, y_0)$ be a point on a level set $G(x, y)=c$. \pause Suppose that
    $G_y(x_0, y_0)\neq 0$. \pause The \emph{local linearization of the level set
      $G=c$ at $(x_0, y_0)$} is the function $L:\mathbb{R}\to \mathbb{R}$
    defined by
    \[
    L(x) = \pause y_0 - \frac{G_x(x_0, y_0)}{G_y(x_0, y_0)}\cdot(x - x_0)
    \]
  \end{definition}

  \pause
  \begin{block}{Note}
    The local linearization $L$ can be used to estimate the change in $y$ that
    results from a change in $x$.
  \end{block}

\end{frame}


\begin{sagesilent}
  G = x**2-3*x*y+y**3
\end{sagesilent}
\begingroup
\small
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the point $(4, 3)$ on the level set $\sage{G}=7$. \pause Suppose
    that the value of $x$ is increased to $x=7$. \pause Approximate the corresponding
    change in $y$.
  \end{example}
  \pause
  \begin{block}{Solution}
    We are interested in the level set $G=7$ where $G(x, y)=\sage{G}$. \pause The
    partials of $G$ are
    \begin{align*}
      G_x &= \sage{G.diff(x)} & G_y &= \sage{G.diff(y)}
    \end{align*}\pause
    It follows that $G_x(4, 3)=\pause\sage{G.diff(x)(x=4, y=3)}$ and $G_y(4,
    3)=\pause\sage{G.diff(y)(x=4, y=3)}$. \pause The local linearization is 
    \[
    L(x) = \pause 3 + \frac{1}{15}(x-4)
    \]\pause
    Our approximation is thus
    \[
    y(7)
    \approx \pause L(7)
    = \pause 3+\frac{1}{15}(7-4)
    = \pause 3+\frac{1}{5}
    = \pause \frac{16}{5}
    \]
  \end{block}
\end{frame}
\endgroup


\section{Implicit Function Theorem for $G:\mathbb{R}^{n}\to \mathbb{R}$}
\subsection{Statement}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Theorem 15.2 in Simon \& Blume]
    Let $G:\mathbb{R}^{n+1}\to \mathbb{R}$ and suppose that $P=(x_1^\ast,
    \dotsc, x_n^\ast, y^\ast)$ is a point on the level set $G=c$. \pause Suppose
    that $G_y(P)\neq0$. \pause Then the points on the level set $G=c$ near $P$
    are of the form $(x_1, \dotsc, x_n, y(x_1,\dotsc, x_n))$ where
    $y(x_1^\ast,\dotsc,x_n^\ast)=y^\ast$ \pause and
    \[
    \pderiv{y}{x_k}(x_1^\ast,\dotsc,x_n^\ast)
    =-\frac{G_{x_k}(P)}{G_{y}(P)}
    \]
  \end{theorem}

  \pause
  \begin{block}{Note}
    We call $y$ an \emph{endogenous variable} and we call $\Set{x_1,\dotsc,x_n}$
    the corresponding \emph{exogenous variables}.
  \end{block}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  var('x y z')
  G = x**3+x*y+y*z**2
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $\sage{G}=3$. \onslide<2->{Which of the partial derivatives
      \begin{align*}
        \pderiv{x}{y} && \pderiv{x}{z} &&
        \pderiv{y}{x} && \pderiv{y}{z} &&
        \pderiv{z}{x} && \pderiv{z}{y} 
      \end{align*}
      does the implicit function theorem guarantee to exist at $(1, 2, 0)$?}
  \end{example}
  \begin{block}{\onslide<3->{Solution}}
    \onslide<3->{We are interested in $G=3$ where $G(x, y, z)=\sage{G}$. The partials of $G$
      are}
    \begin{align*}
      \onslide<4->{G_x &= } \onslide<5->{\sage{G.diff(x)}} &
      \onslide<6->{G_y &= } \onslide<7->{\sage{G.diff(y)}} &
      \onslide<8->{G_z &= } \onslide<9->{\sage{G.diff(z)}} 
    \end{align*}
    \onslide<10->{At $(1, 2, 0)$, we have}
    \begin{align*}
      \onslide<11->{G_x(1, 2, 0) &=}\onslide<12->{ \sage{G.diff(x)(x=1, y=2, z=0)}} &
      \onslide<13->{G_y(1, 2, 0) &=}\onslide<14->{ \sage{G.diff(y)(x=1, y=2, z=0)}} &
      \onslide<15->{G_z(1, 2, 0) &=}\onslide<16->{ \sage{G.diff(z)(x=1, y=2, z=0)}} 
    \end{align*}
    \onslide<17->{The implicit function theorem then implies that}
    \begin{align*}
      \onslide<18->{\pderiv{x}{y}(1, 2, 0) &= } \onslide<19->{-\frac{1}{5}} & 
      \onslide<20->{\pderiv{x}{z}(1, 2, 0) &= } \onslide<21->{0           } &
      \onslide<22->{\pderiv{y}{x}(1, 2, 0) &= } \onslide<23->{-5          } & 
      \onslide<24->{\pderiv{y}{z}(1, 2, 0) &= } \onslide<25->{0           }
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\subsection{Linear Approximations}


\newcommand{\myDy}{
  \begin{bmatrix}
    -\frac{G_{x_1}(P)}{G_y(P)} & \dotsb & -\frac{G_{x_n}(P)}{G_y(P)}
  \end{bmatrix}}
\newcommand{\myvvXast}{
  \begin{bmatrix}
    x_1^\ast\\ \vdots\\ x_n^\ast
  \end{bmatrix}
}
\begingroup
\small
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $P=(x_1^\ast,\dotsc,x_n^\ast,y^\ast)$ be a point on a level set
    $G(x_1,\dotsc,x_n,y)=c$. Suppose that $G_y(P)\neq 0$. \pause The \emph{local
      linearization of the level set $G=c$ at the point $P$} is the function
    $L:\mathbb{R}^{n}\to \mathbb{R}$ defined by
    \[
    L(X)=\pause y^\ast+Dy\cdot(\vv{X}-\vv{X}^\ast)
    \]\pause
    where
    \begin{align*}
      Dy &= \myDy &
      \vv{X}^\ast &= \myvvXast
    \end{align*}
  \end{definition}

  \pause
  \begin{block}{Note}
    The local linearization $L$ can be used to estimate the change in $y$ that
    results from changes to $\Set{x_1,\dotsc,x_n}$.
  \end{block}
\end{frame}
\endgroup


\begin{sagesilent}
  G = x**2-x*y*z**3+y**2*z
  Gx = G.diff(x)(x=3, y=2, z=1)
  Gy = G.diff(y)(x=3, y=2, z=1)
  Gz = G.diff(z)(x=3, y=2, z=1)
  Dz = matrix([-Gx/Gz, -Gy/Gz])
  vv = matrix.column([2, -1])
\end{sagesilent}

\begingroup
\footnotesize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the point $(3, 2, 1)$ on the level set $\sage{G}=7$. \pause Suppose that
    $x$ is increased by $2$ and that $y$ is decreased by $1$. \pause Approximate the
    corresponding change in $z$.
  \end{example}
  \pause
  \begin{block}{Solution}
    We are interested in the level set $G=7$ where $G(x, y, z)=\sage{G}$. \pause
    The partials of $G$ are
    \begin{align*}
      G_x &= \sage{G.diff(x)} &
      G_y &= \sage{G.diff(y)} &
      G_z &= \sage{G.diff(z)} 
    \end{align*}\pause
    It follows that $G_x(3, 2, 1)=\pause\sage{Gx}$, $G_y(3, 2, 1)=\pause\sage{Gy}$, and
    $G_z(3, 2, 1)=\pause\sage{Gz}$. \pause Our approximation is 
    \[
    z(3+2, 2-1)
    \approx \pause L(3+2, 2-1) 
    = \pause 1 + \sage{Dz}\sage{vv}
    = \pause\frac{3}{2}
    \]
  \end{block}
\end{frame}
\endgroup





\end{document}
