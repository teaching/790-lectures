\documentclass{beamer}

\usepackage{amsmath}%
\usepackage{amssymb}%
\usepackage{amsthm}%
\usepackage{caption}%
\usepackage{cite}%
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{mathtools}%
\usepackage{multicol}%
\usepackage{nicefrac}%
\usepackage{sagetex}%
\usepackage{siunitx}%
\usepackage{stmaryrd}%
\usepackage{tikz}%
\usetikzlibrary{arrows, calc, decorations.pathreplacing, decorations.markings,
  matrix, positioning, shapes.geometric}%
\usepackage{tikz-3dplot}%
\usepackage{tikz-cd}%
\usepackage{cool}%
\usepackage{pifont}% http://ctan.org/pkg/pifont
% \usepackage[dvipsnames]{xcolor}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\Span}{Span}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Image}{Image}
\DeclareMathOperator{\Range}{Range}
\DeclareMathOperator{\Graph}{Graph}
\DeclareMathOperator{\Domain}{Domain}
\DeclareMathOperator{\Target}{Target}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\newcommand*\Eval[3]{\left.#1\right\rvert_{#2}^{#3}}



\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }


\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}

\newcommand{\mysage}[2]{{#1\sage{#2}}}

\title{Double Integrals}
\subtitle{Math 790-92.03}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../dukemath.pdf}}


\sisetup{
  quotient-mode = fraction,
  fraction-function=\nicefrac
}

\begin{document}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Single-Variable Integrals}
\subsection{Definition and Motivation}


\newcommand{\myIntegralDescription}{
  \begin{array}{c}
    \text{weighted area between graph of $f(x)$} \\
    \text{and $x$-axis from $x=a$ and $x=b$}
  \end{array}
}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Let $f$ be a continuous function on an interval $[a, b]$.
  \[
  \pgfmathsetmacro{\a}{-pi/2}
  \pgfmathsetmacro{\b}{2*pi}
  \pgfmathsetmacro{\mydelta}{1/3}
  \pgfmathsetmacro{\am}{\a-\mydelta}
  \pgfmathsetmacro{\bm}{\b+\mydelta}
  \begin{tikzpicture}[
    line join=round,
    line cap=round,
    declare function = {myf(\x) = sin(\x r)+3;}
    ]

    \onslide<6->{
      \fill[teal!40]
      (\a, 0)
      -- ($ (\a, {myf(2)}) $)
      -- plot[domain=\a:\b, samples=200, variable=\x] ({\x},{myf(\x)})
      -- (\b, 0)
      -- cycle;
    }

    \onslide<2->{
      \draw[very thick, blue, <->, domain=\am:\bm, samples=200, variable=\x]
      plot ({\x}, {myf(\x)}) node[right] {$f(x)$};

      \draw[ultra thick, <->] (\am, 0) -- (\bm, 0) node[right] {$x$};

      \draw[ultra thick] (\a, 3pt) -- (\a, -3pt) node[below] {$a$};
      \draw[ultra thick] (\b, 3pt) -- (\b, -3pt) node[below] {$b$};
    }
  \end{tikzpicture}
  \]
  \onslide<3->{The definite integral of $f$ on $[a, b]$ is often interpreted as}
  \[
  \onslide<4->{\Int{f(x)}{x, a, b}=}\onslide<5->{\myIntegralDescription}
  \]
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The area interpretation of $\Int{f(x)}{x, a, b}$ is \emph{not} the
    definition of the definite integral. \pause The definite integral is defined
    as \emph{a limit of Riemann sums}.
  \end{block}

  \pause
  \begin{definition}
    The \emph{definite integral} of $f$ on $[a, b]$ is
    \[
    \Int{f(x)}{x, a, b}=\lim_{n\to\infty}\sum_{k=1}^n f(x_k)\cdot\Delta x
    \]
    where $\displaystyle \Delta x=\pause\frac{b-a}{n}$ \pause and
    $x_k=\pause a+k\cdot\Delta x$.
  \end{definition}
\end{frame}


\subsection{The Fundamental Theorem of Calculus}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    In general, using the limit definition to compute $\Int{f(x)}{x, a, b}$ is
    difficult. \pause The Fundamental Theorem of Calculus allows us to easily
    compute $\Int{f(x)}{x, a, b}$ if we can find an \emph{antiderivative} of
    $f$.
  \end{block}

\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We say that a function $F$ is an \emph{antiderivative} of $f$ if
    $F^\prime=f$.
  \end{definition}

  \pause
  \begin{theorem}[The Fundamental Theorem of Calculus]
    Suppose that $F$ is an antiderivative of $f$. Then
    \[
    \Int{f(x)}{x, a, b}=F(b)-F(a)
    \]
  \end{theorem}
\end{frame}


\begin{sagesilent}
  var('x t theta')
  f = x**2
  F = integral(f, x)
  g = exp(-2*t)
  G = integral(g, t)
  h = cos(pi * theta)
  H = integral(h, theta)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    $
    \displaystyle
    \Int{\sage{f}}{x, 0, 1}
    = \Eval{\sage{F}}{0}{1}
    = \sage{F(x=1)}-\sage{F(x=0)}
    = \sage{integral(f, x, 0, 1)}
    $
  \end{example}

  \pause
  \begin{example}
    $
    \displaystyle
    \Int{\sage{g}}{t, -3, 3}
    = \Eval{\sage{G}}{-3}{3}
    = \sage{G(t=3)}+\sage{-G(t=-3)}
    $
  \end{example}

  \pause
  \begin{example}
    $
    \displaystyle
    \Int{\sage{h}}{\theta, 1, \nicefrac{5}{2}}
    = \Eval{\sage{H}}{1}{\nicefrac{5}{2}}
    = \sage{H(theta=5/2)}-\sage{H(theta=1)}
    = \sage{integral(h, theta, 1, 5/2)}
    $
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Although FTC can make integration easier, sometimes approximation by Riemann
    Sums is required.
  \end{block}

  \pause
  \begin{example}
    In 1968 Robert Henry Risch proved that the integral
    \[
    \Int{e^{-x^2}}{x, a, b}
    \]
    cannot be computed using the Fundamental Theorem of Calculus!
  \end{example}
\end{frame}


\subsection{Physical Interpretation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  View the interval $[a, b]$ as a physical object of length $\ell=b-a$.\pause
  \[
  \pgfmathsetmacro{\a}{-3}
  \pgfmathsetmacro{\b}{-\a}
  \pgfmathsetmacro{\mydelta}{1/2}
  \pgfmathsetmacro{\am}{\a-\mydelta}
  \pgfmathsetmacro{\bm}{\b+\mydelta}
  \pgfmathsetmacro{\p}{\a+(2/5)*(\b-\a)}
  \begin{tikzpicture}[line join=round, line cap=round]
    \draw[ultra thick, <->] (\am, 0) -- (\bm, 0);
    \draw[ultra thick] (\a, 3pt) -- (\a, -3pt) node[below] {$a$};
    \draw[ultra thick] (\b, 3pt) -- (\b, -3pt) node[below] {$b$};

    \draw[ultra thick] (\p, 3pt) -- (\p, -3pt) node[below] {$x$};
  \end{tikzpicture}
  \]
  Suppose that at every point $x$, the function $f(x)$ measures the density of
  the object. \pause Then $f(x)$ has units
  \[
  \frac{\text{"mass" units}}{\text{length units}}
  \]\pause
  Integrating $f$ from $x=a$ to $x=b$ then gives
  \[
  \Int{f(x)}{x, a, b}
  = \pause\text{mass of the object}
  \]
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Suppose that the length is measured in meters.
  \[
  \begin{array}{lcc}
    \text{interpretation} & f(x)                                         & \Int{f(x)}{x, a, b} \\ \hline \pause
    \text{mass}              & \SI[per-mode=fraction]{}{\kg\per\meter}      & \SI{}{\kg} \\ \pause
    \text{temperature}       & \SI[per-mode=fraction]{}{\celsius\per\meter} & \SI{}{\celsius} \\ \pause
    \text{electrical charge} & \SI[per-mode=fraction]{}{\coulomb\per\meter} & \SI{}{\coulomb} \\ \pause
    \text{magnetic field strength} & \SI[per-mode=fraction]{}{\ampere\per\meter} & \SI{}{\ampere} \\ \pause
    \text{price}             & \SI[per-mode=fraction]{}{\$\per\meter}       & \SI{}{\$}
  \end{array}
  \]
\end{frame}



\begin{sagesilent}
  fc = 3*x**2
  fr = 6*sqrt(x)
  f = fr-fc
  F = integral(f, x)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    You have decided to purchase the interval $[4, 16]$ at a price of
    $\sage{fc}$ dollars per meter, where $x$ is the location on the
    interval. \pause The government offers a tax incentive that rebates your
    purchase by $\sage{fr}$ dollars per meter, where $x$ is the location on the
    interval. \pause How much does it cost you to purchase the interval $[4,
    16]$?
  \end{example}

  \begin{block}{Solution}
    The ``price-density'' function is
    \[
    f(x)
    = \pause\sage{f}
    \]\pause
    Note that
    \[
    \Int{f(x)}{x, 4, 16}
    = \Int{\sage{f}}{x, 4, 16}
    = \Eval{\sage{F}}{4}{16}
    = -\SI{3808}[\$]{}
    \]\pause
    It thus costs you $\SI{3808}[\$]{}$ to purchase the interval $[4, 16]$.
  \end{block}
\end{frame}
\endgroup


\subsection{Summary}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{itemize}
  \item Definite integrals are defined as limits of Riemann sums and this
    definition provides the ``area'' interpretation.
  \item<2-> The Fundamental Theorem of Calculus makes certain integrals easier to
    compute, but not every integral can be computed using FTC.
  \item<3-> Definite integrals are useful tools for solving ``mass-density''
    problems.
  \end{itemize}
\end{frame}


\section{Double Integrals}
\subsection{Mass-Density Interpretation}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}

    Let $D$ be a region in $\mathbb{R}^{2}$. %
    \onslide<3->{Suppose that at every point $(x,y)$ in $D$, the function
      $f(x,y)$ measures the density of $D$. }%
    \onslide<5->{Then $f(x,y)$ has units
      \[
      \frac{\text{"mass" units}}{\text{area units}}
      \]}%

    \column{.5\textwidth}
    \[
    \begin{tikzpicture}[xscale = 5/8, yscale=1/2]
      \path[]
      (-4 , -4)   coordinate (p1)
      (0  , -4)   coordinate (p2)
      (3  , -3)   coordinate (p3)
      (4  ,  1)   coordinate (p4)
      (3  ,  2)   coordinate (p5)
      (0 ,   0)   coordinate (p6)
      (-2 ,  3)   coordinate (p7)
      (-4 ,  1)   coordinate (p8)
      % ...
      ;
      \onslide<2->{
        \draw[
        fill = blue!25!white
        , draw = black
        , very thick]
        plot [smooth cycle, tension=0.7]
        coordinates {(p1) (p2) (p3) (p4) (p5) (p6) (p7) (p8)};

        \node[below=1em] at (p7) {$D$};
      }

      \onslide<4->{
        \node[below=1cm] at (p6) {\textbullet};
        \node[below=.5cm] at (p6) {$(x,y)$};
      }
    \end{tikzpicture}
    \]
  \end{columns}
  
  \onslide<6->{From this perspective, the \emph{integral of $f$ over the region $D$} should
    give}
  \[
  \onslide<7->{\iint_{D}f(x,y)\,dA=}\onslide<8->{\text{mass of the region $D$}}
  \]
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Terminology}
    We call $\displaystyle\iint_{D}f(x,y)\,dA$ the \emph{double integral of $f$
      over the region $D$}.
  \end{block}

  \pause
  \begin{block}{Note}
    Double integrals are formally defined as limits of ``double Riemann sums''
    \pause but we are only interested in the physical interpretation.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Suppose that area is measured by square-meters.
  \[
  \begin{array}{lcc}
    \text{interpretation}          & f(x, x)                                              & \iint_{D}f(x, y)\,dA \\ \hline \pause
    \text{mass}                    & \SI[per-mode=fraction]{}{\kg\per\meter\squared}      & \SI{}{\kg}          \\ \pause
    \text{temperature}             & \SI[per-mode=fraction]{}{\celsius\per\meter\squared} & \SI{}{\celsius}     \\ \pause
    \text{electrical charge}       & \SI[per-mode=fraction]{}{\coulomb\per\meter\squared} & \SI{}{\coulomb}     \\ \pause
    \text{magnetic field strength} & \SI[per-mode=fraction]{}{\ampere\per\meter\squared}  & \SI{}{\ampere}      \\ \pause
    \text{price}                   & \SI[per-mode=fraction]{}{\$\per\meter\squared}       & \SI{}{\$}
  \end{array}
  \]
\end{frame}


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{columns}[onlytextwidth, t]
      \column{.6\textwidth}%
      You decide to buy a the circular plot of land $D$ described by
      $x^2+y^2\leq 1$. %
      \onslide<3->{The price of land at a point $(x, y)$ in $D$ is
        \[
        f(x, y)=\frac{100}{(x^2+y^2)+1}
        \]
        dollars per square-meter. }%
      \onslide<4->{How much does the plot of land cost?}

      \column{.4\textwidth}
      \[
      \begin{tikzpicture}[line join=round, line cap=round, scale=3/2]
        \onslide<2->{
          \filldraw[ultra thick, fill=teal!40] (0, 0) circle (1);

          \node at (1/2, 1/2) {$D$};
        }
      \end{tikzpicture}
      \]
    \end{columns}
  \end{example}

  \begin{block}{\onslide<5->{Solution}}
    \onslide<5->{The cost of the land is}
    \[
    \onslide<6->{\iint_{D}\frac{100}{(x^2+y^2)+1}\,dA}
    \]
    \onslide<7->{How do we compute this double integral? }%
    \onslide<8->{We need a method for computation! }%
  \end{block}

\end{frame}
\endgroup


\section{Iterated Integrals Over Rectangles}
\subsection{The Slicing Methods}


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}%
    \begin{block}{$x$-Slicing}
      Let $D$ be the rectangle $[a, b]\times [c, d]$. %
      \onslide<3->{Suppose that $f(x, y)$ measures the density at every point in
        $D$. }%
      \onslide<4->{Consider an ``$x$-strip'' for some $a\leq x\leq b$. }%
      \onslide<6->{ The density of this $x$-strip is given by }
      \[
      \onslide<7->{\delta(x) = }\onslide<8->{\Int{f(x, y)}{y, c, d}}
      \]
    \end{block}


    \column{.5\textwidth}%
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1]
      \onslide<2->{
        \draw[ultra thick, <->] (-.5, 0) -- (4.5, 0) node [right] {$x$};
        \draw[ultra thick, <->] (0, -.5) -- (0, 3.5) node [above] {$y$};

        \filldraw[very thick, fill=teal!40]
        (1/2, 1/2) -- (4, 1/2) -- (4, 3) -- (1/2, 3) -- cycle;


        \draw[ultra thick] (1/2, 3pt) -- (1/2, -3pt) node[below] {$a$};
        \draw[ultra thick] (4, 3pt) -- (4, -3pt) node[below] {$b$};

        \draw[ultra thick] (3pt, 1/2) -- (-3pt, 1/2) node[left] {$c$};
        \draw[ultra thick] (3pt, 3) -- (-3pt, 3) node[left] {$d$};
      }

      \onslide<5->{
        \draw[ultra thick] (3, 3pt) -- (3, -3pt) node[below] {$x$};
        \draw[thick, dashed] (3, 1/2) -- (3, 3);
      }
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<9->{The total mass of the rectangle $D$ is given by}
  \[
  \onslide<10->{\iint_{D}f(x, y)\,dA
    =} \onslide<11->{\Int{\delta(x)}{x, a, b}
    =} \onslide<12->{\Int{\Set*{\Int{f(x, y)}{y, c, d}}}{x, a, b}
    =} \onslide<13->{\Int{\Int{f(x, y)}{y, c, d}}{x, a, b}}
  \]
\end{frame}
\endgroup



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The expression $\Int{\Int{f(x, y)}{y, c, d}}{x, a, b}$ is
    called an \emph{iterated integral}.
  \end{definition}

  \pause
  \begin{block}{Idea}
    Computing $\Int{\Int{f(x, y)}{y, c, d}}{x, a, b}$ is
    easy. \pause First, compute the ``inner integral''
    \[
    \Int{f(x, y)}{y, c, d}
    \]
    treating $x$ as a constant. \pause This gives an expression in $x$ that we
    can integrate with respect to $x$ to obtain the value of the iterated
    integral $\Int{\Int{f(x, y)}{y, c, d}}{x, a, b}$.
  \end{block}
\end{frame}



\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}%
    \begin{block}{$y$-Slicing}
      The method of ``$y$-slicing'' is similar. %
      \onslide<3->{Consider a ``$y$-strip'' for
        some $c\leq y\leq d$. }%
      \onslide<5->{The density of this $y$-strip is given by
        \[
        \delta(y) = \Int{f(x, y)}{x, a, b}
        \]}
    \end{block}


    \column{.5\textwidth}%
    \[
    \begin{tikzpicture}[line join=round, line cap=round, scale=1]
      \onslide<2->{
        \draw[ultra thick, <->] (-.5, 0) -- (4.5, 0) node [right] {$x$};
        \draw[ultra thick, <->] (0, -.5) -- (0, 3.5) node [above] {$y$};

        \filldraw[very thick, fill=teal!40]
        (1/2, 1/2) -- (4, 1/2) -- (4, 3) -- (1/2, 3) -- cycle;

        \draw[ultra thick] (1/2, 3pt) -- (1/2, -3pt) node[below] {$a$};
        \draw[ultra thick] (4, 3pt) -- (4, -3pt) node[below] {$b$};

        \draw[ultra thick] (3pt, 1/2) -- (-3pt, 1/2) node[left] {$c$};
        \draw[ultra thick] (3pt, 3) -- (-3pt, 3) node[left] {$d$};
      }

      \onslide<4->{
        \draw[ultra thick] (3pt, 2) -- (-3pt, 2) node[left] {$y$};
        \draw[thick, dashed] (1/2, 2) -- (4, 2);
      }
    \end{tikzpicture}
    \]
  \end{columns}

  \onslide<6->{The total mass of the rectangle $D$ is given by}
  \[
  \onslide<7->{\iint_{D}f(x, y)\,dA
    =} \onslide<8->{\Int{\delta(y)}{y, c, d}
    =} \onslide<9->{\Int{\Set*{\Int{f(x, y)}{x, a, b}}}{y, c, d}
    =} \onslide<10->{\Int{\Int{f(x, y)}{x, a, b}}{y, c, d}}
  \]
\end{frame}
\endgroup





\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The expression $\Int{\Int{f(x, y)}{x, a, b}}{y, c, d}$ is
    called an \emph{iterated integral}.
  \end{definition}

  \pause
  \begin{block}{Idea}
    Computing $\Int{\Int{f(x, y)}{x, a, b}}{y, c, d}$ is
    easy. \pause First, compute the ``inner integral''
    \[
    \Int{f(x, y)}{x, a, b}
    \]
    treating $y$ as a constant. \pause This gives an expression in $y$ that we
    can integrate with respect to $y$ to obtain the value of the iterated
    integral $\Int{\Int{f(x, y)}{x, a, b}}{y, c, d}$.
  \end{block}
\end{frame}


\subsection{Example}


\begin{sagesilent}
  var('x y')
  f = x**2+y**2
  a, b, c, d = 1, 3, 2, 4
  Ify = integral(f, y)
  Ifyd = Ify(y=d)
  Ifyc = Ify(y=c)
  Ifycd = Ifyd - Ifyc
  Ifx = integral(Ifycd, x)
  Ifxa = Ifx(x=a)
  Ifxb = Ifx(x=b)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $D$ be the rectangle $[1,3]\times[2, 4]$. %
    \onslide<2->{Suppose that the density throughout $D$ is given by $f(x,
      y)=\sage{f}\,\SI[per-mode=fraction]{}{\kg\per\meter\squared}$. }%
    \onslide<3->{Compute the mass of $D$.}
  \end{example}

  \begin{block}{\onslide<4->{Solution}}
    \onslide<5->{To compute the mass of $D$, we use an iterated integral}
    \begin{align*}
      \onslide<6->{\iint_D f(x, y)\,dA
        &=} \onslide<7->{\Int{\Int{\sage{f}}{y, 2, 4}}{x, 1, 3} \\
        &=} \onslide<8->{\Int{\Set*{\sage{Ify}}_{y=2}^{y=4}}{x, 1, 3} \\
        &=} \onslide<9->{\Int{\Set*{\left(\sage{Ifyd}\right)-\left(\sage{Ifyc}\right)}}{x, 1, 3} \\
        &=} \onslide<10->{\Int{\sage{Ifycd}}{x, 1, 3} \\
        &=} \onslide<11->{\Set*{\sage{Ifx}}_{x=1}^{x=3} \\
        &=} \onslide<12->{\sage{Ifxb-Ifxa}\,\SI{}{\kg}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup




\section{Iterated Integrals Over Non-Rectangular Regions}
\subsection{Motivation}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How do we compute $\iint_D f(x,y)\,dA$ if $D$ is not rectangular?
  \end{block}

  \pause
  \begin{block}{Answer}
    We can use iterated integrals, but our ``slicing'' choice will depend on the
    shape of $D$.
  \end{block}
\end{frame}


\subsection{$x$-Slicing}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Suppose that our region $D$ has no top or bottom ``corners''.
  \[
  \pgfmathsetmacro{\p}{0.271534}
  \pgfmathsetmacro{\q}{3.06155}
  \begin{tikzpicture}[
    line join=round,
    line cap=round,
    xscale=2,
    declare function = {myf(\x) = sin((\x-2) r)+2; myg(\x) = -(\x - 2)*(\x-2) + 4;},
    scale=2/3
    ]

    \pgfmathsetmacro{\py}{myf(\p)}
    \pgfmathsetmacro{\qy}{myf(\q)}

    \onslide<2->{
      \draw[very thick, blue, domain=\p:\q, samples=200, variable=\x]
      plot ({\x}, {myf(\x)});% node[] {$f(x)$};

      \draw[very thick, blue, -, domain=\p:\q, samples=200, variable=\x]
      plot ({\x}, {myg(\x)});% node[right] {$g(x)$};
    }

    \onslide<3->{
    \node[] at (\p, \py) {\textbullet};
    \node[] at (\q, \qy) {\textbullet};
    }

    \onslide<2->{
      \draw[ultra thick, <->] (-1/2, 0) -- (3.5, 0) node[right] {$x$};
      \draw[ultra thick, <->] (0, -1) -- (0, 5) node[above] {$y$};
    }

    \onslide<3->{
      \draw[ultra thick] (\p, 3pt) -- (\p, -3pt) node[below] {$a$};
      \draw[ultra thick] (\q, 3pt) -- (\q, -3pt) node[below] {$b$};


      \draw[ultra thick] (1.5, 3pt) -- (1.5, -3pt) node[below] {$x$};

      \coordinate (ymin) at ($ (1.5, {myf(1.5)}) $);
      \coordinate (ymax) at ($ (1.5, {myg(1.5)}) $);

      \draw[thick, dashed] (ymin) -- (ymax);
    }

    \onslide<4->{
      \node at (ymin) {\textbullet};
      \node at (ymax) {\textbullet};


      \draw[ultra thick] ($ (1.5pt, {myf(1.5)}) $) -- ($ (-1.5pt, {myf(1.5)}) $) node[left] {$y_1(x)$};
      \draw[ultra thick] ($ (1.5pt, {myg(1.5)}) $) -- ($ (-1.5pt, {myg(1.5)}) $) node[left] {$y_2(x)$};
    }
  \end{tikzpicture}
  \]
  \onslide<5->{Then we may use ``$x$-slicing'' to set up an iterated integral}
  \[
  \onslide<6->{\iint_D f(x, y)\,dA
    =} \onslide<7->{\Int{\Int{f(x, y)}{y, y_1(x), y_2(x)}}{x, a, b}}
  \]
\end{frame}


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{columns}[onlytextwidth, t]
    \column{.45\textwidth}
    \begin{example}
      Let $D$ be the region in $\mathbb{R}^{2}$ bounded by $y=x^2$ and
      $y=x+2$. %
      \onslide<3->{Suppose that $f(x, y)=x^2+y$ measures density throughout
        $D$. }%
      \onslide<4->{Compute the mass of the region $D$.}
    \end{example}

    \begin{block}{\onslide<5->{Solution}}
      \onslide<5->{The region has no top or bottom corners, so we may use
        $x$-slicing. }%
      \onslide<6->{The minimum and maximum values of $x$ within the region can
        be obtained by solving the equation $x^2=x+2$. }%
      \onslide<7->{These values are $a=-1$ and $b=2$.}
    \end{block}

    \column{.55\textwidth}
    \[
    \begin{tikzpicture}[line join=round, line cap=round, yscale=1/2,
      declare function = {myf(\x) = \x*\x; myg(\x) = \x+2;},]

      % \fill[fill=teal!40]
      % (-1, 1)
      % -- plot[domain=-1:2, samples=200, variable=\x] ({\x},{\myf(\x)})
      % -- plot[domain=2:-1, samples=200, varaiable=\x] ({\x}, {\myg(\x)})
      % -- cycle;

      \onslide<2->{
        \filldraw[fill=teal!40, draw=white]
        (-1, 0) -- (-1, 1) -- (2, 4) -- (2, 0) -- cycle;

        \filldraw[fill=white, draw=white]
        (-1, 0)
        -- (-1, 1)
        -- plot[domain=-1:2, samples=200, variable=\x] ({\x},{\x*\x})
        -- (2, 0)
        -- cycle;

        \node at (1, 2) {$D$};

        \draw[ultra thick, <->] (-3/2, 0) -- (5/2, 0) node[right] {$x$};
        \draw[ultra thick, <->] (0, -1) -- (0, 7) node[above] {$y$};

        \draw[very thick, blue, <->, domain=-3/2:5/2, samples=200, variable=\x]
        plot ({\x}, {myf(\x)}) node[above] {$y=x^2$};

        \draw[very thick, blue, <->, domain=-3/2:5/2, samples=200, variable=\x]
        plot ({\x}, {myg(\x)}) node[right] {$y=x+2$};
      }

      \onslide<8->{
        \node at (-1, 1) {\textbullet};
        \draw[ultra thick] (-1, 3pt) -- (-1, -3pt) node[below] {$-1$};

        \node at (2, 4) {\textbullet};
        \draw[ultra thick] (2, 3pt) -- (2, -3pt) node[below] {$2$};
      }
    \end{tikzpicture}
    \]
  \end{columns}
  \vspace{.25in}
  \onslide<9->{The method of $x$-slicing then gives}
  \[
  \onslide<10->{\iint_{D}f(x, y)\,dA
    =} \onslide<11->{\Int{\Int{f(x, y)}{y, y_1(x), y_2(x)}}{x, a, b}
    =} \onslide<12->{\Int{\Int{x^2+y}{y, x^2, x+2}}{x, -1, 2}}
  \]
\end{frame}
\endgroup


\begin{sagesilent}
  var('x y')
  f = x**2+y
  y1 = x**2
  y2 = x+2
  a = -1
  b = 2
  Ify = integral(f, y)
  Ify1 = Ify(y=y1)
  Ify2 = Ify(y=y2)
  Ify1y2 = Ify2-Ify1
  Ifx = integral(Ify1y2, x)
  m = integral(integral(f, y, y1, y2), x, a, b)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    Now, we compute the iterated integral
    \begin{align*}
      \onslide<2->{\Int{\Int{x^2+y}{y, x^2, x+2}}{x, -1, 2}
        &=} \onslide<3->{\Int{\Set*{\sage{Ify}}_{y=x^2}^{y=x+2}}{x, -1, 2} \\
        &=} \onslide<4->{\Int{\Set*{\left(\sage{Ify2}\right)-\left(\sage{Ify1}\right)}}{x, -1, 2} \\
        &=} \onslide<5->{\Int{\sage{Ify1y2}}{x, -1, 2} \\
        &=} \onslide<6->{\Set*{\sage{Ifx}}_{-1}^2 \\
        &=} \onslide<7->{\sage{m}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup










\subsection{$y$-Slicing}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{columns}[onlytextwidth]
    \column{.65\textwidth}%
    Suppose that our region $D$ has no left or right ``corners''. %
    \onslide<3->{Then we may use ``$y$-slicing'' to set up an iterated integral.}
    \[
    \onslide<5->{\iint_{D}f(x, y)\,dA
      =} \onslide<6->{\Int{\Int{f(x, y)}{x, x_1(y), x_2(y)}}{y, c, d}}
    \]

    \column{.35\textwidth}
    \[
    \pgfmathsetmacro{\p}{0.271534} \pgfmathsetmacro{\q}{3.06155}
    \begin{tikzpicture}[ line join=round, line cap=round, yscale=2, declare
      function = {myf(\x) = sin((\x-2) r)+2; myg(\x) = -(\x - 2)*(\x-2) + 4;},
      scale=2/3 ]

      \pgfmathsetmacro{\py}{myf(\p)} \pgfmathsetmacro{\qy}{myf(\q)}

      \onslide<2->{
      \draw[very thick, blue, domain=\p:\q, samples=200, variable=\x] plot
      ({myf(\x)},{\x});% node[] {$f(x)$};

      \draw[very thick, blue, -, domain=\p:\q, samples=200, variable=\x] plot
      ({myg(\x)}, {\x});% node[right] {$g(x)$};
      }

      \onslide<4->{
      \node[] at (\py, \p) {\textbullet}; 
      \node[] at (\qy, \q) {\textbullet};
      }
      
      \onslide<2->{
      \draw[ultra thick, <->] (0, -1/2) -- (0, 3.5) node[above] {$y$};
      \draw[ultra thick, <->] (-1, 0) -- (5, 0) node[right] {$x$};
      }

      \onslide<4->{
      \draw[ultra thick] (3pt, \p) -- (-3pt, \p) node[left] {$c$}; 
      \draw[ultra thick] (3pt, \q) -- (-3pt, \q) node[left] {$d$};
      }

      \onslide<4->{
      \draw[ultra thick] (3pt, 1.5) -- (-3pt, 1.5) node[left] {$y$};

      \coordinate (ymin) at ($ ({myf(1.5)}, 1.5) $); \coordinate (ymax) at ($
      ({myg(1.5)}, 1.5) $);

      \draw[thick, dashed] (ymin) -- (ymax);

      \node at (ymin) {\textbullet}; \node at (ymax) {\textbullet};

      \draw[ultra thick] ($ ({myf(1.5)}, 1.5pt) $) -- ($ ({myf(1.5)}, -1.5pt) $)
      node[below] {$x_1(y)$}; \draw[ultra thick] ($ ({myg(1.5)}, 1.5pt) $) -- ($
      ({myg(1.5)}, -1.5pt) $) node[below] {$x_2(y)$};}
    \end{tikzpicture}
    \]
  \end{columns}
\end{frame}


\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{example}
      Use $y$-slicing to compute $\iint_D y\,dA$ where $D$ is the region in
      $\mathbb{R}^{2}$ bounded by $y=x^2$ and $y=4$.
    \end{example}

    \begin{block}{\onslide<3->{Solution}}
      \onslide<3->{The minimum value of $y$ in the region $D$ is $c=0$. }%
      \onslide<4->{The maximum value of $y$ in $D$ is $d=4$. }

      \onslide<5->{The ``left side'' of $D$ is described by the function
        $x_1(y)=-\sqrt{y}$. }%
      \onslide<6->{The ``right side'' of $D$ is described by the function
        $x_2(y)=\sqrt{y}$.}
    \end{block}

    \column{.5\textwidth}
    \begin{tikzpicture}[line join=round, line cap=round, scale=3/4]
      \onslide<2->{
      \draw[ultra thick, <->] (-2.5, 0) -- (2.5, 0) node[right] {$x$};
      \draw[ultra thick, <->] (0, -.5) -- (0, 4.5) node[above] {$y$};

      \draw[very thick, blue, <->] (-2.5, 4) -- (2.5, 4) node[right] {$y=4$};
      \draw[very thick, blue, <->, domain=-2.1:2.1, samples=200, variable=\x] 
      plot ({\x}, {\x*\x}) node[above] {$y=x^2$};
      }
    \end{tikzpicture}
  \end{columns}

  \vspace{.1cm}
  \onslide<7->{The method of $y$-slicing then gives}
  \[
  \onslide<8->{\iint_{D}f(x, y)\,dA
    =} \onslide<9->{\Int{\Int{f(x, y)}{x, x_1(y), x_2(y)}}{y, c, d}
    =} \onslide<10->{\Int{\Int{y}{x, -\sqrt{y}, \sqrt{y}}}{y, 0, 4}}
  \]

\end{frame}
\endgroup




\begin{sagesilent}
  var('x y')
  f = y
  x1 = -sqrt(y)
  x2 = sqrt(y)
  c, d = 0, 4
  Ifx = integral(f, x)
  Ifx1 = Ifx(x=x1)
  Ifx2 = Ifx(x=x2)
  Ifx1x2 = Ifx2-Ifx1
  Ify = integral(Ifx1x2, y)
  Ify1 = Ify(y=c)
  Ify2 = Ify(y=d)
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solution (continued)}
    Now, we compute the iterated integral
    \begin{align*}
      \onslide<2->{\Int{\Int{y}{x, -\sqrt{y}, \sqrt{y}}}{y, 0, 4}
        &=} \onslide<3->{\Int{\Set*{\sage{Ifx}}_{x=-\sqrt{y}}^{x=\sqrt{y}}}{y, 0, 4} \\
        &=} \onslide<4->{\Int{\Set*{\sage{Ifx2}+\sage{Ifx1}}}{y, 0, 4} \\
        &=} \onslide<5->{\Int{\sage{Ifx1x2}}{y, 0, 4} \\
        &=} \onslide<6->{\Eval{\sage{Ify}}{0}{4} \\
        &=} \onslide<7->{\sage{Ify2-Ify1}}
    \end{align*}
  \end{block}
\end{frame}
\endgroup


\end{document}
