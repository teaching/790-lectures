\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{The $PA=LU$ Factorization}
\subtitle{Math 790-92}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}


\section{Background}
\subsection{``Big Picture'' Overview of $PA=LU$}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{$PA=LU$ algorithm} produces a factorization used in \emph{numerical
    linear algebra}.
  \begin{itemize}[<+->]
  \item $P$ is a \emph{permutation matrix}
  \item $L$ is \emph{lower triangular}
  \item $U$ is \emph{upper triangular}
  \item $PA=LU$ algorithm is faster than Gau\ss-Jordan
  \item $PA=LU$ algorithm uses only row swaps and row addition
  \item row swaps are determined by the \emph{method of partial pivoting}
  \item $A\vv{x}=\vv{b}$ is equivalent to $U\vv{x}=\vv{y}$ where $\vv{y}$ solves
    $L\vv{y}=P\vv{b}$
  \end{itemize}

\end{frame}



\subsection{Rounding Errors}
\begin{sagesilent}
  import sigfig
  M = sigfig.ex1()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Using \emph{rational numbers} to reduce a system produces an \emph{exact
      answer}.
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
  \end{block}


\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  In practice, computers use \emph{floating point numbers} to store data. After
  each computation, the computer rounds each entry to a given number of
  significant digits.

\end{frame}



\begin{sagesilent}
  import sigfig
  L = sigfig.gj1()
\end{sagesilent}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Applying Gau\ss-Jordan algorithm to the previous example with rounding to
  three significant digits gives
  \begingroup
  \tiny
  \begin{align*}
    \sage{L[0]}
    \to\sage{L[1]} \\
    \to\sage{L[2]} \\
    \to\sage{L[3]} \\
    \to\sage{L[4]} \\
    \to\sage{L[5]} \\
    \to\sage{L[6]}
  \end{align*}
  \endgroup \pause
  The exact solution is $\vv{x}=\sage{vector([1, 1, 1])}$. \pause Rounding
  ``destabilized'' the algorithm.

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The method of \emph{partial pivoting} uses the largest possible number (in
  absolute value) to create pivots. This minimizes rounding errors.

\end{frame}



\begin{sagesilent}
  import sigfig
  L = sigfig.pp1()
\end{sagesilent}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Using partial pivoting, we have
  \begingroup
  \scriptsize
  \begin{align*}
    \sage{L[0]}
    \to\sage{L[1]} \\
    \to\sage{L[2]}
    \to\sage{L[3]} \\
    \to\sage{L[4]}
    \to\sage{L[5]} \\
    \to\sage{L[6]}
    \to\sage{L[7]} \\
    \to\sage{L[8]}
  \end{align*}
  \endgroup \pause
  By using partial pivoting, we have reduced the rounding error.

\end{frame}

\subsection{Permutation Matrices}


\begin{sagesilent}
  set_random_seed(759752592)
  P2 = Permutations(2).random_element().to_matrix()
  P3 = Permutations(3).random_element().to_matrix()
  P4 = Permutations(4).random_element().to_matrix()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{permutation matrix} is a matrix obtained by performing row swaps on
    an identity matrix.
  \end{definition}

  \pause
  \begin{example}
    \begin{align*}
      \sage{P2} && \sage{P3} && \sage{P4}
    \end{align*}
  \end{example}

\end{frame}


\begin{sagesilent}
  set_random_seed(4397)
  n = 2
  A = random_matrix(ZZ, n)
  B = A - A.T
  I = identity_matrix(n)
  Q = ((B-I).inverse())*(B+I)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{orthogonal matrix} is a matrix $Q$ satisfying $Q^{-1}=Q^\intercal$.
  \end{definition}

  \pause
  \begin{example}
    Consider the matrix $Q$ given by
    \[
      Q = \sage{Q}
    \]\pause
    Then $Q$ is orthogonal since
    \[
      QQ^\intercal
      = \sage{Q}\sage{Q.T}
      = \sage{Q*Q.T}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every permutation matrix is an orthogonal matrix.
  \end{theorem}

\end{frame}


\subsection{Forward Elimination}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The $PA=LU$ algorithm uses \emph{forward elimination}.

\end{frame}


\begin{sagesilent}
  A = matrix([(1, -2, 1), (1, 0, 2), (-1, 1, 0)])
  from functools import partial
  elem = partial(elementary_matrix, A.nrows())
  E11 = elem(row1=1, row2=0, scale=-A[1, 0])
  E12 = elem(row1=2, row2=0, scale=-A[2, 0])
  A1 = E12*E11*A
  E2 = elem(row1=2, row2=1, scale=-A1[2, 1]/A1[1, 1])
  A2 = E2*A1
  E = E2*E12*E11
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the row reductions
  \newcommand{\myStepA}{\arraycolsep=1pt\tiny
    \begin{array}{rcrcr}
      R_2 &-& R_1 &\to& R_2 \\
      R_3 &+& R_1 &\to& R_3
    \end{array}
  }
  \begingroup
  \scriptsize
  \[
    \underset{A}{\sage{A}}
    \xrightarrow{\myStepA}\sage{A1}
    \xrightarrow{R_3+(\frac{1}{2})\cdot R_2\to R_3}\underset{U}{\sage{A2}}
  \]
  \endgroup

\end{frame}





\section{The $PA=LU$ Algorithm}
\subsection{Description}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm ($PA=LU$ Factorization with Partial Pivoting)}
    Suppose $A$ is $m\times n$. Start with $i=j=1$ and $L=P=I_m$.
    \begin{description}[<+->]
    \item[Step 1] Find $k\geq i$ where $\abs{a_{kj}}>0$ is as large as
      possible. Perform $R_i\leftrightarrow R_k$ on $A$, $P$, and the
      multipliers in $L$. If not possible, increase $j$ by one and repeat this
      step.
    \item[Step 2] Eliminate all entries \emph{below} $a_{ij}$ by subtracting
      suitible multiples $m_{kj}$ of $\Row_i$. Put $\ell_{kj}=-m_{kj}$.
    \item[Step 3] Increase $i$ and $j$ by one and return to Step 1.
    \end{description}
    \onslide<+->{The algorithm terminates after the last row or column is
      processed.}
  \end{block}

\end{frame}


\subsection{Example}

\begin{sagesilent}
  A = matrix([(1, 2, 0), (4, -3, -5), (3, 2, 13)])
  P = identity_matrix(A.nrows())
  L = identity_matrix(A.nrows())
  from functools import partial
  elem = partial(elementary_matrix, A.nrows())
  E1 = elem(row1=0, row2=1)
  A1 = E1*A
  L1 = L
  P1 = E1*P
  m21 = -A1[1, 0]/A1[0, 0]
  m31 = -A1[2, 0]/A1[0, 0]
  E21 = elem(row1=1, row2=0, scale=m21)
  E22 = elem(row1=2, row2=0, scale=m31)
  A2 = E22*E21*A1
  L2 = matrix([(1, 0, 0), (-m21, 1, 0), (-m31, 0, 1)])
  P2 = P1
  E3 = elem(row1=1, row2=2)
  A3 = E3*A2
  L3 = matrix([(1, 0, 0), (-m31, 1, 0), (-m21, 0, 1)])
  P3 = E3*P1
  m32 = -A3[2, 1] / A3[1, 1]
  E4 = elem(row1=2, row2=1, scale=m32)
  A4 = E4*A3
  P4 = P3
  L4 = matrix([(1, 0, 0), (-m31, 1, 0), (-m21, -m32, 1)])
\end{sagesilent}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStepA}{\arraycolsep=1pt\tiny
    \begin{array}{rcrcr}
      R_2 &-& (\frac{1}{4})\cdot R_1 &\to& R_2 \\
      R_3 &-& (\frac{3}{4})\cdot R_1 &\to& R_3
    \end{array}
  }
  The $PA=LU$ agorithm with partial pivoting gives
  \begingroup
  \tiny
  \begin{align*}
    \overset{A}{\sage{A}}
    \onslide<2->{\xrightarrow{\onslide<3->{R_1\leftrightarrow R_2}}}\onslide<4->{\overset{U}{\sage{A1}}} && \onslide<5->{\overset{L}{\sage{L1}}} && \onslide<6->{\overset{P}{\sage{P1}}} \\
    \onslide<7->{\xrightarrow{\onslide<8->{\myStepA}}}\onslide<9->{\sage{A2}}                            && \onslide<10->{\sage{L2}}              && \onslide<11->{\sage{P2}} \\
    \onslide<12->{\xrightarrow{\onslide<13->{R_2\leftrightarrow R_3}}}\onslide<14->{\sage{A3}}              && \onslide<15->{\sage{L3}}              && \onslide<16->{\sage{P3}} \\
    \onslide<17->{\xrightarrow{\onslide<18->{R_3-(\frac{11}{17})\cdot R_2\to R_3}}}\onslide<19->{\sage{A4}} && \onslide<20->{\sage{L4}}              && \onslide<21->{\sage{P4}} \\
  \end{align*}
  \endgroup
  \onslide<22->{This gives the $PA=LU$ factorization}
  \begingroup
  \tiny
  \[
    \onslide<23->{\underset{P}{\sage{P4}}}  \onslide<24->{\underset{A}{\sage{A}}} \onslide<25->{=\underset{L}{\sage{L4}}}  \onslide<26->{\underset{U}{\sage{A4}}}
  \]
  \endgroup

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    How does $PA=LU$ help us solve $A\vv{x}=\vv{b}$?
  \end{block}

  \pause
  \begin{block}{Answer}
    $A\vv{x}=\vv{b}$ is equivalent to $PA\vv{x}=P\vv{b}$. This gives
    $LU\vv{x}=P\vv{b}$.
    \begin{description}[<+->]
    \item[Step 1] Use ``forward substitution'' to solve $L\vv{y}=P\vv{b}$ for
      $\vv{y}$.
    \item[Step 2] Use ``back substitution'' to solve $U\vv{x}=\vv{y}$ for
      $\vv{x}$.
    \end{description}
  \end{block}

\end{frame}

\begin{sagesilent}
  P, L, U = A.LU()
  P = P.T
  b = vector([326, -326, -163])
  pb1, pb2, pb3 = P*b
  y = L.inverse()*P*b
  y1, y2, y3 = y
  x = A.inverse()*b
  x1, x2, x3 = x
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  For $\vv{b}=\sage{b}$, the system $L\vv{y}=P\vv{b}$ is
  \[
    \begin{array}{rcrcrcrcl}
      \onslide<2->{y_1                    & &                          & &     &=& \sage{pb1}} &\onslide<3->{\to& y_1=\sage{y1}} \\
      \onslide<2->{(\frac{3}{4})\cdot y_1 &+& y_2                      & &     &=& \sage{pb2}} &\onslide<4->{\to& y_2=\sage{y2}} \\
      \onslide<2->{(\frac{1}{4})\cdot y_1 &+& (\frac{11}{17})\cdot y_2 &+& y_3 &=& \sage{pb3}} &\onslide<5->{\to& y_3=\sage{y3}}
    \end{array}
  \]
  \onslide<6->{For $\vv{y}=\sage{y}$, the system $U\vv{x}=\vv{y}$ is}
  \[
    \begin{array}{rcrcrcrcl}
      \onslide<7->{4\,x_1 &-& 3\,x_2                  &-& 5\,x_3                     &=& \sage{y1}} &\onslide<10->{\to& x_1=\sage{x1}} \\
                          & & \onslide<7->{(\frac{17}{4})\cdot x_2 &+& (\frac{67}{4})\cdot x_3    &=& \sage{y2}} &\onslide<9->{\to& x_2=\sage{x2}} \\
                          & &                         & & \onslide<7->{(-\frac{163}{17})\cdot x_3 &=& \sage{y3} &\onslide<8->{\to& x_3=\sage{x3}}}
    \end{array}
  \]
  \onslide<11->{The solution to $A\vv{x}=\vv{b}$ is $\vv{x}=\sage{x}$.}

\end{frame}


\end{document}
