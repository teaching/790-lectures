\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{The Gau\ss-Jordan Algorithm}
\subtitle{Math 790-92}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  % \tableofcontents
  \begin{columns}[onlytextwidth, t]
    \column{.5\textwidth}
    \tableofcontents[sections={1-3}]
    \column{.5\textwidth}
    \tableofcontents[sections={4-}]
  \end{columns}
\end{frame}


\section{Motivation}
\subsection{What We Know}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Matrices can be used to study systems of linear equations.

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The \emph{augmented matrix} of the system
  \[
    \begin{array}{ccccccccc}
      a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
      a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
      \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
      a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
    \end{array}
  \]
  is given by
  \[
    \left[
      \begin{array}{cccc|c}
        a_{11} & a_{12}  & \dotsb & a_{1n} & b_{1} \\
        a_{21} & a_{22}  & \dotsb & a_{2n} & b_{2} \\
        \vdots & \vdots & \ddots & \vdots  & \vdots  \\
        a_{m1} & a_{m2} & \dotsb & a_{mn}  & b_{m}
      \end{array}
    \right]
  \]

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Solving systems of linear equations is \emph{hard}.

\end{frame}



\begin{sagesilent}
  set_random_seed(4808201371480)
  A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=2)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
  coeff = matrix.column(coeffs)
  xp1, xp2, xp3 = map(matrix.column, coeff.change_ring(QQ).right_kernel(basis='pivot').basis())
  xp = matrix.column([-2, -2, 0, 0, 0])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The solutions to
  \[
    \sage{M}
  \]
  are given by
  \[
    \vv{x}
    = \sage{xp}+c_1\sage{xp1}+c_2\sage{xp2}+c_3\sage{xp3}
  \]\pause
  Where did these solutions come from?!

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Solving systems in \emph{reduced row echelon form} is easy: write the
  dependent variables in terms of the free variables.

\end{frame}


\begin{sagesilent}
  var('x1 x2 x3 x4 x5')
  vx = matrix.column([x1, x2, x3, x4, x5])
  xs = matrix.column([-2+2*x4+2*x5, -2+3*x3+3*x5, x3, x4, x5])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  The solutions to
  \[
    \sage{M.rref()}
  \]
  are given by
  \[\scriptsize
    \sage{vx}
    = \pause\sage{xs}
    = \pause\sage{xp}+x_3\sage{xp1}+x_4\sage{xp2}+x_5\sage{xp3}
  \]

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Subtitution is a terrible method.

\end{frame}




\subsection{What We Want}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Good Idea}
    Replace ``hard'' systems with equivalent ``easy'' systems.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Gau\ss-Jordan Algorithm}
    A procedure that transforms ``hard'' systems into ``easy'' systems.
    \begin{description}[<+(1)->]
    \item[Input] A system of linear equations.
    \item[Output] An \emph{equivalent} system in reduced row echelon form.
    \end{description}
    \pause Each step in the algorithm calls for a \emph{row manipulation}.
  \end{block}

\end{frame}


\section{Elementary Row Operations}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    There are three types of \emph{elementary row operations}.
    \begin{description}[<+(1)->][Row Switching]
    \item[Row Switching] A row can be switched with another row.
    \item[Row Scaling] A row can be multiplied by a \emph{nonzero} scalar.
    \item[Row Addition] A row can be replaced by the sum of that row and a
      scalar multiple of another row.
    \end{description}
  \end{definition}

  \pause
  \begin{block}{Notation}
    The notation $R_i$ refers to the $i$th row of a matrix.
    \gcenter{
      $\underset{\textnormal{row switching}}{\myotherbold{R_i\leftrightarrow R_j}}$ &&
      $\underset{\textnormal{row scaling}}{\myotherbold{c\cdot R_i\to R_i}}$ &&
      $\underset{\textnormal{row addition}}{\myotherbold{R_i+c\cdot R_j\to R_i}}$
      }
  \end{block}
\end{frame}

\subsection{Examples}


\begin{sagesilent}
  set_random_seed(32450845082)
  A = random_matrix(ZZ, 4, 4)
  E = elementary_matrix(4, row1=1, row2=3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Switching]
    \[
      \sage{A}\xrightarrow{R_2\leftrightarrow R_4}
      \pause
      \sage{E*A}
    \]
  \end{example}
\end{frame}


\begin{sagesilent}
  set_random_seed(504321865014)
  A = random_matrix(ZZ, 4, 3)
  E = elementary_matrix(4, row1=2, scale=-11)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Scaling]
    \[
      \sage{A}\xrightarrow{-11\cdot R_3\to R_3}
      \pause
      \sage{E*A}
    \]
  \end{example}
\end{frame}



\begin{sagesilent}
  set_random_seed(42151235801125)
  A = random_matrix(ZZ, 6, 4)
  E = elementary_matrix(6, row1=4, row2=1, scale=-3)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Row Addition]
    \[
      \sage{A}\xrightarrow{R_5-3\cdot R_2\to R_5}
      \pause
      \sage{E*A}
    \]
  \end{example}
\end{frame}


\section{The Gau\ss-Jordan Algorithm}
\subsection{Description}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every matrix $A$ can be row-reduced to a reduced row echelon form matrix
    $\rref(A)$. \pause Furthermore, $\rref(A)$ is unique.
  \end{theorem}

\end{frame}


\begin{sagesilent}
  A = matrix([(-9, 18, 20), (-5, 10, 11)])
  E1 = elementary_matrix(2, row1=0, scale=-1/9)
  E2 = elementary_matrix(2, row1=1, row2=0, scale=5)
  E3 = elementary_matrix(2, row1=1, scale=-9)
  E4 = elementary_matrix(2, row1=0, row2=1, scale=20/9)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The row-reductions
    \begin{align*}
      \onslide<1->{\sage{A}}
      &\onslide<2->{\xrightarrow{(-\frac{1}{9})\cdot R_1\to R_1}\sage{E1*A} \\ }
      &\onslide<3->{\xrightarrow{R_2+5\cdot R_1\to R_2}\sage{E2*E1*A} \\ }
      &\onslide<4->{\xrightarrow{-9\cdot R_2\to R_2}\sage{E3*E2*E1*A} \\ }
      &\onslide<5->{\xrightarrow{R_1+(\frac{20}{9})\cdot R_2\to R_1}\sage{E4*E3*E2*E1*A}}
    \end{align*}
    \onslide<6->{show that}
    \[
      \onslide<7->{\rref\sage{A}=}\onslide<8->{\sage{A.rref()}}
    \]
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{algorithm}[The Gau\ss-Jordan Algorithm]
    To determine $\rref(A)$, start with $(i, j)=(1, 1)$.\pause
    \begin{description}
    \item[Step 1] If $a_{ij}=0$, switch $\Row_i$ with the first row below it whose
      $j$th entry is nonzero. If no such row exists, increase $j$ by one and
      repeat this step.\pause
    \item[Step 2] Scale $\Row_i$ by $\frac{1}{a_{ij}}$ to produce a pivot.\pause
    \item[Step 3] Eliminate \emph{all} nonpivot entries in the $j$th column by
      subtracting suitable multiples of $\Row_i$.\pause
    \item[Step 4] Increase $i$ and $j$ by one and return to Step 1.\pause
    \end{description}
    The algorithm terminates after the last row or column is processed.
  \end{algorithm}

\end{frame}


\subsection{A $3\times 3$ Example}
\begin{sagesilent}
  A = matrix([(-4, 20, -12), (-1, 5, -3), (5, -25, 16)])
  E1 = elementary_matrix(3, row1=0, scale=-1/4)
  E2 = elementary_matrix(3, row1=1, row2=0, scale=1)
  E3 = elementary_matrix(3, row1=2, row2=0, scale=-5)
  E4 = elementary_matrix(3, row1=1, row2=2)
  E5 = elementary_matrix(3, row1=0, row2=1, scale=-3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStepA}{\scriptsize
    \begin{array}{rcrcr}
      R_2 &+& R_1 &\to& R_2 \\
      R_3 &-& 5\cdot R_1 &\to& R_3
    \end{array}
  }
  \begin{align*}
    \onslide<1->{\underset{A}{\sage{A}}}
    &\onslide<2->{\xrightarrow{\onslide<3->{(-\frac{1}{4})\cdot R_1\to R_1}}} \onslide<4->{\sage{E1*A}} \\
    &\onslide<5->{\xrightarrow{\onslide<6->{\myStepA}}}\onslide<7->{\sage{E3*E2*E1*A}} \\
    &\onslide<8->{\xrightarrow{\onslide<9->{R_2\leftrightarrow R_3}}}\onslide<10->{\sage{E4*E3*E2*E1*A}} \\
    &\onslide<11->{\xrightarrow{\onslide<12->{R_1-3\cdot R_2\to R_1}}}\onslide<13->{\underset{\rref(A)}{\sage{E5*E4*E3*E2*E1*A}}}
  \end{align*}

\end{frame}


\subsection{A $4\times 3$ Example}
\begin{sagesilent}
  set_random_seed(502756)
  A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  while A.pivots() != (0, 2): A = random_matrix(ZZ, 4, 3, algorithm='echelonizable', rank=2)
  E1 = elementary_matrix(4, row1=0, scale=-1/2)
  E2 = elementary_matrix(4, row1=1, row2=0, scale=1)
  E3 = elementary_matrix(4, row1=2, row2=0, scale=-1)
  E4 = elementary_matrix(4, row1=1, scale=-2)
  E5 = elementary_matrix(4, row1=0, row2=1, scale=5/2)
  E6 = elementary_matrix(4, row1=2, row2=1, scale=1/2)
  E7 = elementary_matrix(4, row1=3, row2=1, scale=5)
\end{sagesilent}
\begingroup
\footnotesize
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \newcommand{\myStepA}{\tiny
    \begin{array}{rcrcr}
      R_2 &+& R_1 &\to& R_2 \\
      R_3 &-& R_1 &\to& R_3
    \end{array}
  }
  \newcommand{\myStepB}{\tiny
    \begin{array}{rcrcr}
      R_1 &+& (\frac{5}{2})\cdot R_2 &\to& R_1 \\
      R_3 &+& (\frac{1}{2})\cdot R_2 &\to& R_3 \\
      R_4 &+& 5\cdot R_2 &\to& R_4
    \end{array}
  }
  \begin{align*}
    \onslide<1->{\underset{A}{\sage{A}}}
    &\onslide<2->{
      \xrightarrow{\onslide<3->{(-\frac{1}{2})\cdot R_1\to R_1}}
      }
      \onslide<4->{\sage{E1*A}} \\
    &\onslide<5->{
      \xrightarrow{\onslide<6->{\myStepA}}
      }
      \onslide<7->{\sage{E3*E2*E1*A}} \\
    &\onslide<8->{
      \xrightarrow{\onslide<9->{-2\cdot R_2\to R_2}}
      }\onslide<10->{\sage{E4*E3*E2*E1*A}} \\
    &\onslide<11->{
      \xrightarrow{\onslide<12->{\myStepB}}
      }\onslide<13->{\underset{\rref(A)}{\sage{E7*E6*E5*E4*E3*E2*E1*A}}}
  \end{align*}

\end{frame}
\endgroup


\subsection{A $3\times 4$ Example}
\begin{sagesilent}
  A = matrix([(0, 0, -1, 1), (1, -3, -3, 4), (-4, 12, 17, -21)])
  E1 = elementary_matrix(3, row1=0, row2=1)
  E2 = elementary_matrix(3, row1=2, row2=0, scale=4)
  E3 = elementary_matrix(3, row1=1, scale=-1)
  E4 = elementary_matrix(3, row1=0, row2=1, scale=3)
  E5 = elementary_matrix(3, row1=2, row2=1, scale=-5)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStepA}{\tiny
    \begin{array}{rcrcr}
      R_1 &+& 3\cdot R_2 &\to& R_1 \\
      R_3 &-& 5\cdot R_2 &\to& R_3
    \end{array}
  }
  \begin{align*}
    \onslide<1->{\underset{A}{\sage{A}}}
    &\onslide<2->{\xrightarrow{
      \onslide<3->{R_1\leftrightarrow R_2}
      }}\onslide<4->{\sage{E1*A}} \\
    &\onslide<5->{\xrightarrow{
      \onslide<6->{R_3+4\cdot R_1\to R_3}
      }}
      \onslide<7->{\sage{E2*E1*A}} \\
    &\onslide<8->{\xrightarrow{
      \onslide<9->{-R_2\to R_2}
      }}
      \onslide<10->{\sage{E3*E2*E1*A}} \\
    &\onslide<11->{\xrightarrow{
      \onslide<12->{\myStepA}
      }}
      \onslide<13->{\underset{\rref(A)}{\sage{E5*E4*E3*E2*E1*A}}}
  \end{align*}

\end{frame}



\section{Solving Linear Systems}
\subsection{Idea}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    What happens when we perform an elementary row operation on an augmented
    matrix?
  \end{block}

  \onslide<2->
  \begin{block}{Answer}
    Each row in an augmented matrix represents an equation, so
    \begin{description}[row switching]
    \item<3->[row switching] switches equations
    \item<4->[row scaling] scales an equation (by a nonzero number!)
    \item<5->[row addition] adds a multiple of one equation to another equation
    \end{description}
    \onslide<6->Each of these operations yields an \emph{equivalent} system!
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The rref of a System}
    The Gau\ss-Jordan algorithm allows us to transform any linear system into an
    equivalent reduced row echelon form system\pause
    \[
      [A\mid\vv{b}]\rightsquigarrow\rref[A\mid\vv{b}]
    \]\pause
    The solutions to the ``hard system'' $[A\mid\vv{b}]$ are the same as the
    solutions to the ``easy system'' $\rref[A\mid\vv{b}]$!
  \end{block}

\end{frame}


\subsection{A $2\times 2$ Example}
\begin{sagesilent}
  A = matrix([(1, 2), (3, 4)])
  b = vector([3, 0])
  M = A.augment(b, subdivide=True)
  E1 = elementary_matrix(2, row1=1, row2=0, scale=-3)
  E2 = elementary_matrix(2, row1=1, scale=-1/2)
  E3 = elementary_matrix(2, row1=0, row2=1, scale=-2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  We previously used substitution to solve
  \[
    \begin{array}{rcrcr}
      x &+& 2\,y &=& 3 \\
      3\,x &+& 4\,y &=& 0
    \end{array}
  \]
  \onslide<2->{The Gau\ss-Jordan algorithm gives}
  \begin{align*}
    \onslide<3->{\sage{M}}
    &\onslide<4->{\xrightarrow{
      \onslide<5->{R_2-3\cdot R_1\to R_2}
      }}
      \onslide<6->{\sage{E1*M}} \\
    &\onslide<7->{\xrightarrow{
      \onslide<8->{(-\frac{1}{2})\cdot R_2\to R_2}
      }}
      \onslide<9->{\sage{E2*E1*M}} \\
    &\onslide<10->{\xrightarrow{
      \onslide<11->{R_1-2\cdot R_2\to R_1}
      }}
      \onslide<12->{\sage{E3*E2*E1*M}}
  \end{align*}
  \onslide<13->{This shows that $\rref\sage{M}=\sage{M.rref()}$.} \onslide<14->{The only solution to the
    system is $\vv{x}=\sage{vector([-6, 9/2])}$.}

\end{frame}



\subsection{A $3\times 3$ Example}
\begin{sagesilent}
  A = matrix([(5, 20, 17), (3, 12, 10), (4, 16, 14)])
  b = vector([56, 33, 46])
  M = A.augment(b, subdivide=True)
  E1 = elementary_matrix(3, row1=0, scale=1/5)
  E2 = elementary_matrix(3, row1=1, row2=0, scale=-3)
  E3 = elementary_matrix(3, row1=2, row2=0, scale=-4)
  E4 = elementary_matrix(3, row1=1, scale=-5)
  E5 = elementary_matrix(3, row1=0, row2=1, scale=-17/5)
  E6 = elementary_matrix(3, row1=2, row2=1, scale=-2/5)
  var('x1 x2 x3')
  vx = matrix.column([x1, x2, x3])
  vxs = matrix.column([1-4*x2, x2, 3])
  vxp = matrix.column([1, 0, 3])
  vx1 = matrix.column([-4, 1, 0])
\end{sagesilent}
\begingroup
\scriptsize
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myStepA}{\tiny
    \begin{array}{rcrcr}
      R_2 &-& 3\cdot R_1 &\to& R_2 \\
      R_3 &-& 4\cdot R_1 &\to& R_3
    \end{array}
  }
  \newcommand{\myStepB}{\tiny
    \begin{array}{rcrcr}
      R_1 &-& (\frac{17}{5})\cdot R_2 &\to& R_1 \\
      R_3 &-& (\frac{2}{5})\cdot R_2 &\to& R_3
    \end{array}
  }
  \begin{align*}
    \onslide<1->{\sage{M}}
    &\onslide<2->{\xrightarrow{
      \onslide<3->{(\frac{1}{5})\cdot R_1\to R_1}
      }}
      \onslide<4->{\sage{E1*M}} \\
    &\onslide<5->{\xrightarrow{
      \onslide<6->{\myStepA}
      }}
      \onslide<7->{\sage{E3*E2*E1*M}} \\
    &\onslide<8->{\xrightarrow{
      \onslide<9->{-5\cdot R_2\to R_2}
      }}
      \onslide<10->{\sage{E4*E3*E2*E1*M}} \\
    &\onslide<11->{\xrightarrow{
      \onslide<12->{\myStepB}
      }}
      \onslide<13->{\sage{E6*E5*E4*E3*E2*E1*M}}
  \end{align*}
  \onslide<14->{By using the Gau\ss-Jordan algorithm, we find that the solutions to the system
    are given by}
  \[
    \onslide<15->{\vv{x}
      =} \onslide<16->{\sage{vx}
      =} \onslide<17->{\sage{vxs}
      =} \onslide<18->{\sage{vxp}+x_2\sage{vx1}}
  \]

\end{frame}
\endgroup



\subsection{A $3\times 4$ Example}
\begin{sagesilent}
  var('b1 b2 b3')
  A = matrix([(2, 0, 1, -1), (0, 1, 2, 1), (2, -1, -1, -2)])
  b = vector([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
  E1 = elementary_matrix(3, row1=0, scale=1/2)
  E2 = elementary_matrix(3, row1=2, row2=0, scale=-2)
  E3 = elementary_matrix(3, row1=2, row2=1, scale=1)
\end{sagesilent}
\begingroup
\small
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Which vectors $\vv{b}=\sage{b}$ make the system consistent?
  \begin{align*}
    \sage{M}
    &\onslide<2->{\xrightarrow{
      \onslide<3->{(\frac{1}{2})\cdot R_1\to R_1}
      }}
      \onslide<4->{\sage{E1*M}} \\
    &\onslide<5->{\xrightarrow{
      \onslide<6->{R_3-2\cdot R_1\to R_3}
      }}
      \onslide<7->{\sage{E2*E1*M}} \\
    &\onslide<8->{\xrightarrow{
      \onslide<9->{R_3+R_2\to R_3}
      }}
      \onslide<10->{\sage{E3*E2*E1*M}}
  \end{align*}
  \onslide<11->{The vectors $\vv{b}$ that make the system consistent are the
    vectors whose coordinates satisfy} \onslide<12->{$-b_1+b_2+b_3=0$.}
  \onslide<13->{These are the vectors \emph{orthogonal} to}
  \onslide<14->{$\sage{vector([-1, 1, 1])}$.}

\end{frame}
\endgroup



\subsection{Weighted Net Flow}

\begin{sagesilent}
  A = matrix.column([(-1, 1, 0), (0, -1, 1), (-1, 0, 1)])
  b = vector([-3, -8, 4])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      Can we set weights $x_1$, $x_2$, and $x_3$ so that the weighted net flow
      through the nodes is given by the vector $\vv{b}=\sage{b}$?
    \end{block}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , shorten <= 4pt
        , shorten >= 4pt
        , line join=round
        , line cap=round
        , node distance=2cm
        , on grid
        , ultra thick
        , state/.append style={fill=cyan!66}
        , mystyle/.style={->}
        , every node/.style={fill=white}
        , initial/.style={}
        ]

        \node[state] (v1)                {$v_1$};
        \node[state] (v2) [right =3cm of v1] {$v_2$};
        \node[state] (v3) [above right=2cm and 1.5cm of v1] {$v_3$};

        \path
        (v1) edge [mystyle] node {$a_1$} (v2)
        (v2) edge [mystyle] node {$a_2$} (v3)
        (v1) edge [mystyle] node {$a_3$} (v3)
        ;
      \end{tikzpicture}
    \]
  \end{columns}\pause
  \begin{block}{Answer}
    Use the Gau\ss-Jordan algorithm
    \[
      \rref\sage{M}=\pause\sage{M.rref()}
    \]\pause
    The system is inconsistent! \pause No such weights exist.
  \end{block}

\end{frame}



\section{Rank and Nullity}
\subsection{Definitions}
\begin{sagesilent}
  set_random_seed(4205832)
  A = random_matrix(ZZ, 4, 7, algorithm='echelonizable', rank=3)
  while not A.pivots() == (0, 2, 4): A = random_matrix(ZZ, 4, 7, algorithm='echelonizable', rank=3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{rank} of a rref matrix is the number of pivots.
  \end{block}

  \pause
  \begin{example}
    The rref matrix
    \[
      R=\sage{A.rref()}
    \]
    has $\rank(R)=\pause\sage{A.rank()}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{nullity} of a rref matrix is the number of nonpivot columns.
  \end{block}

  \pause
  \begin{example}
    The rref matrix
    \[
      R=\sage{A.rref()}
    \]
    has $\nullity(R)=\pause\sage{A.right_nullity()}$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    For a $m\times n$ matrix $A$, we define
    \begin{description}[<+->]
    \item[$\rank(A)$] as the number of pivots in $\rref(A)$
    \item[$\nullity(A)$] as the number of nonpivot columns in $\rref(A)$
    \end{description}
  \end{definition}

\end{frame}



\begin{sagesilent}
  set_random_seed(4205832)
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3)
  while not A.pivots() == (0, 1, 3): A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Using the Gau\ss-Jordan algorithm, we find that
    \[
      \rref\underset{A}{\sage{A}}=\sage{A.rref()}
    \]\pause
    This means that $\rank(A)=\pause\sage{A.rank()}$ \pause and
    $\nullity(A)=\pause\sage{A.right_nullity()}$.
  \end{example}

\end{frame}


\subsection{Transposition}
\begin{sagesilent}
  set_random_seed(48720958)
  A = random_matrix(ZZ, 2, 3, algorithm='echelonizable', rank=2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Important Observation}
    $\rref(A^\intercal)\neq\rref(A)^\intercal$
  \end{block}

  \pause
  \begin{example}
    \begin{align*}
      \rref\sage{A} &= \sage{A.rref()} \\
      \rref\sage{A.T} &= \sage{A.T.rref()}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\rank(A)=\rank(A^\intercal)$
  \end{theorem}

  \pause
  \begin{example}
    \begin{align*}
      \rref\sage{A} &= \sage{A.rref()}\leftarrow\rank(A)=\sage{A.rank()} \\
      \rref\sage{A.T} &= \sage{A.T.rref()}\leftarrow\rank(A^\intercal)=\sage{A.T.rank()}
    \end{align*}
  \end{example}

\end{frame}


\subsection{The Rank-Nullity Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Rank-Nullity Theorem]
    Every $m\times n$ matrix satisfies
    \[
      \rank(A)+\nullity(A)=n
    \]
  \end{theorem}

\end{frame}


\subsection{Bounds}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every $m\times n$ matrix $A$ satisfies
    \gcenter{
      $\rank(A)\leq m$ && and && $\rank(A)\leq n$
    }
  \end{theorem}

  \pause
  \begin{definition}
    A $m\times n$ matrix $A$ has
    \begin{description}[<+->][full column rank]
    \item[full column rank] if $\rank(A)=n$
    \item[full row rank] if $\rank(A)=m$
    \item[full rank] if $\rank(A)=m$ or $\rank(A)=n$
    \end{description}
  \end{definition}


  \onslide<+->
  \begin{definition}
    A \emph{nonsingular} matrix is a square matrix with full rank. \onslide<+->
    A \emph{singular matrix} is a square matrix with nonfull rank.
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(45702)
  A = random_matrix(ZZ, 2, 3, algorithm='echelonizable', rank=2)
  B = random_matrix(ZZ, 3, 2, algorithm='echelonizable', rank=2)
  C = random_matrix(ZZ, 2, 2, algorithm='echelonizable', rank=2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
  Suppose that $A$, $B$, and $C$ satisfy
  \begin{align*}
    \rref(A) &= \sage{A.rref()} & \rref(B) &= \sage{B.rref()} & \rref(C) &= \sage{C.rref()}
  \end{align*}\pause
  This means that \pause
  \begin{description}
  \item[$A$ has] \emph{full row rank} but not full column rank \pause
  \item[$B$ has] \emph{full column rank} but not full row rank \pause
  \item[$C$ has] \emph{full row and column rank} \pause
  \end{description}
  In fact, $C$ is a \pause\emph{nonsingular matrix}.
\end{example}


\end{frame}





\end{document}
