\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\title{Linear Systems and Reduced Row Echelon Form}
\subtitle{Math 790-92}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-4}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={5-}]
  % \end{columns}
\end{frame}


\section{Motivation and Goals}
\subsection{Algebraic Motivation}

\begin{sagesilent}
  v1 = vector([1, 2, -1, 3])
  v2 = vector([-1, -6, 3, 19])
  v3 = vector([-3, 15, 2, 0])
  b = vector([-4, 2, -3, -1])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Let
    $\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}\in\mathbb{R}^4$
    be given by
    \begin{align*}
      \vv*{v}{1} &= \sage{matrix.column(v1)} &
                                               \vv*{v}{2} &= \sage{matrix.column(v2)} &
                                                                                        \vv*{v}{3} &= \sage{matrix.column(v3)}
    \end{align*}%
    Is $\vv{b}=\sage{matrix.column(b)}$ a linear combination of
    $\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}$?
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer}
    We wish to determine if the equation
    \[
      c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+c_3\cdot\vv*{v}{3}=\vv{b}
    \]
    is solvable. \pause%
    In coordinates, this gives the system of equations
    \[
      \begin{array}{rcrcrcr}
        c_1    &-&     c_2 &-&  3\,c_3 &=& -4   \\
        2\,c_1 &-&  6\,c_2 &+& 15\,c_3 &=& 2  \\
        -c_1   &+&  3\,c_2 &+&  2\,c_3 &=& -3   \\
        3\,c_1 &+& 19\,c_2 & &         &=& -1
      \end{array}
    \]%
    \pause This is a system with \emph{four equations} and \emph{three variables}.
    \pause%
    Solving this system is\textellipsis\pause annoying!
  \end{block}
\end{frame}



\begin{sagesilent}
  A = matrix([(1, -1, -3), (2, -6, 15), (-1, 3, 2), (3, 19, 0)])
  var('c1 c2 c3')
  vx = matrix.column([c1, c2, c3])
  b = matrix.column([-4, 2, -3, -1])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The system
    \[
      \begin{array}{rcrcrcr}
        c_1    &-&     c_2 &-&  3\,c_3 &=& -4   \\
        2\,c_1 &-&  6\,c_2 &+& 15\,c_3 &=& 2  \\
        -c_1   &+&  3\,c_2 &+&  2\,c_3 &=& -3   \\
        3\,c_1 &+& 19\,c_2 & &         &=& -1
      \end{array}
    \]%
    may be succinctly written as $A\vv{x}=\vv{b}$ where
    \begin{align*}
      \onslide<2->{A &=} \onslide<3->{\sage{A}} & \onslide<4->{\vv{x} &=} \onslide<5->{\sage{vx}} & \onslide<6->{\vv{b} &=} \onslide<7->{\sage{b}}
    \end{align*}
  \end{block}

\end{frame}


\subsection{Weighted Net Flow}

\begin{sagesilent}
  b = vector([-3, -8, 4])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{columns}[onlytextwidth]
    \column{.5\textwidth}
    \begin{block}{Question}
      Can we set weights $x_1$, $x_2$, and $x_3$ so that the weighted net flow
      through the nodes is given by the vector $\vv{b}=\sage{b}$?
    \end{block}
    \column{.5\textwidth}
    \[
      \begin{tikzpicture}[
        , shorten <= 4pt
        , shorten >= 4pt
        , line join=round
        , line cap=round
        , node distance=2cm
        , on grid
        , ultra thick
        , state/.append style={fill=cyan!66}
        , mystyle/.style={->}
        , every node/.style={fill=white}
        , initial/.style={}
        ]

        \node[state] (v1)                {$v_1$};
        \node[state] (v2) [right =3cm of v1] {$v_2$};
        \node[state] (v3) [above right=2cm and 1.5cm of v1] {$v_3$};

        \path
        (v1) edge [mystyle] node {$a_1$} (v2)
        (v2) edge [mystyle] node {$a_2$} (v3)
        (v1) edge [mystyle] node {$a_3$} (v3)
        ;
      \end{tikzpicture}
    \]
  \end{columns}
  \pause
  \begin{block}{Answer}
    To answer this question, we must solve
    \[
      x_1\cdot\vv*{a}{1}+x_2\cdot\vv*{a}{2}+x_3\cdot\vv*{a}{3}=\vv{b}
    \]
    for the ``unknown'' scalars $x_1$, $x_2$, and $x_3$. \pause The answer to
    the question is ``yes'' if $\vv{b}$ is a \emph{linear combination} of the
    incidence vectors.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Answer (continued)}
    In coordinates, the equation
    \[
      x_1\cdot\vv*{a}{1}+x_2\cdot\vv*{a}{2}+x_3\cdot\vv*{a}{3}=\vv{b}
    \]
    gives the system of equations
    \[
      \begin{array}{rcrcrcr}
        -x_1 & &     &-& x_3 &=& -3 \\
        x_1 &-& x_2 & &     &=& -8 \\
             & & x_2 &+& x_3 &=&  4
      \end{array}
    \]\pause
    This is a system with \pause\emph{three equations} \pause and \emph{three
      variables}. \pause Solving this system is\textellipsis\pause annoying!
  \end{block}

\end{frame}



\begin{sagesilent}
  A = matrix([(-1, 0, -1), (1, -1, 0), (0, 1, 1)])
  var('x1 x2 x3')
  b = matrix.column([-3, -8, 4])
  vx = matrix.column([x1, x2, x3])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The system
    \[
      \begin{array}{rcrcrcr}
        -x_1 & &     &-& x_3 &=& -3 \\
        x_1 &-& x_2 & &     &=& -8 \\
             & & x_2 &+& x_3 &=&  4
      \end{array}
    \]
    may be succinctly written as $A\vv{x}=\vv{b}$ where
    \begin{align*}
      \onslide<2->{A &=} \onslide<3->{\sage{A}} & \onslide<4->{\vv{x} &=} \onslide<5->{\sage{vx}} & \onslide<6->{\vv{b} &=} \onslide<7->{\sage{b}}
    \end{align*}
  \end{block}

\end{frame}





\subsection{Goal}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Set-Up}
    Consider the system of $m$ equations with $n$ variables.
    \begin{equation}\label{eq:goal}
      % \setlength\belowdisplayskip{0pt}
      \begin{array}{ccccccccc}\pause
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\ \pause
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\ \pause
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\ \pause
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}\tag{$\ast$}
    \end{equation}
    \pause
    A \emph{solution} to \eqref{eq:goal} is a vector $\vv{x}=\langle x_1,x_2,\dotsc,x_n\rangle$ in
    $\mathbb{R}^n$ satisfying each equation in \eqref{eq:goal}.
  \end{block}
  \begin{columns}[onlytextwidth, T]
    \column{.5\textwidth}
    \pause
    \begin{block}{Question 1}
      Does a solution to \eqref{eq:goal} exist?
    \end{block}
    \pause
    \begin{block}{Question 2}
      How \emph{many} solutions exist?
    \end{block}
    \column{.5\textwidth}
    \pause
    \begin{block}{Question 3}
      Can we describe \emph{all} solutions?
    \end{block}
  \end{columns}

\end{frame}





\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The system
    \[
      \begin{array}{ccccccccc}
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}
    \]
    can be written as the \emph{matrix equation} $A\vv{x}=\vv{b}$ where
    \newcommand{\myA}{
      \left[
        \begin{array}{cccc}
          a_{11} &  a_{12} &  \dotsb &  a_{1n}  \\
          a_{21} &  a_{22} &  \dotsb &  a_{2n}  \\
          \vdots &  \vdots &  \ddots &  \vdots  \\
          a_{m1} &  a_{m2} &  \dotsb &  a_{mn}
        \end{array}
      \right]
    }
    \newcommand{\myx}{\begin{bmatrix}x_1\\ x_2\\ \vdots\\ x_n\end{bmatrix}}
    \newcommand{\myb}{\begin{bmatrix}b_1\\ b_2\\ \vdots \\ b_m\end{bmatrix}}
    \begin{align*}
      \onslide<2->{A &=} \onslide<3->{\myA} & \onslide<4->{\vv{x} &=} \onslide<5->{\myx} & \onslide<6->{\vv{b} &=} \onslide<7->{\myb}
    \end{align*}
  \end{block}

\end{frame}



% matrix form note here



\begin{sagesilent}
  A = matrix([(2, 1, 1), (1, 0, -2)])
  n1, = A.right_kernel().basis()
  xp = vector([3, -4, 0])
  v1 = xp+n1
  v2 = xp-2*n1
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The system of two equations and three variables
    \[
      \begin{array}{rcrcrcr}
        2\,x & + & y & + &    z & = 2 \\
        x &   &   & - & 2\,z & = 3
      \end{array}
    \]
    is solved by $\vv{x}=\sage{v1}$ and by $\vv{x}=\sage{v2}$. \pause The system
    has \emph{more than one solution}.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The system of three equations and two variables
    \begin{alignat}{6}
      x &\ & + &\ & 5\,y &\ &=& &\  13 \tag{eq 1}\label{eq:noa}\\
      -2\,x &\ & - &\ & 9\,y &\ &=& &\ -24 \tag{eq 2}\label{eq:nob}\\
      -x &\ & - &\ & 8\,y &\ &=& &\   7 \tag{eq 3}\label{eq:noc}
    \end{alignat}
    has \emph{no solutions} because
    \[
      7\cdot\eqref{eq:noa}+3\cdot\eqref{eq:nob}+\eqref{eq:noc}
      =
      \begin{cases}
        \hspace{\fill} \onslide<2->{0} & \textnormal{LHS} \\
        \onslide<3->{26} & \textnormal{RHS}
      \end{cases}
    \]
  \end{example}
\end{frame}


\subsection{Substitution is a Bad Idea}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \setcounter{equation}{0}
  \begin{example}
    Find all solutions to the system
    \begin{alignat}{6}
      x &\ & + &\ & 2\,y &\ &=& &\  3 \label{eq:suba}\\
      3\,x &\ & + &\ & 4\,y &\ &=& &\  0 \label{eq:subb}
    \end{alignat}
  \end{example}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Bad Idea}
    Start deriving new equations. \pause Equation \eqref{eq:subb} is
    \begin{equation}
      \label{eq:subc}
      y=-\frac{3}{4}\,x
    \end{equation}\pause
    Plug \eqref{eq:subc} into \eqref{eq:suba} to obtain
    \begin{equation}
      \label{eq:subd}
      x+2\left(-\frac{3}{4}\,x\right)=3\implies x=-6
    \end{equation}\pause
    Plug \eqref{eq:subd} into \eqref{eq:subc} to obtain
    \begin{equation}
      \label{eq:sube}
      y=\left(-\frac{3}{4}\right)(-6)=\frac{9}{2}
    \end{equation}\pause
    Equations \eqref{eq:subd} and \eqref{eq:sube} then imply that
    $\vv{x}=\langle-6,\frac{9}{2}\rangle$ solves the system.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question}
    Why is this a bad idea?
  \end{block}\pause
  \begin{block}{Answer 1}
    Equations get messy in high dimensions.
  \end{block}\pause
  \begin{block}{Answer 2}
    The procedure is not algorithmic.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Two systems are \emph{equivalent} if they have the same solutions.
  \end{definition}\pause
  \begin{block}{Good Idea}
    Replace ``hard'' systems with equivalent ``easy'' systems.
  \end{block}
\end{frame}


\section{Augmented Matrices and rref}
\subsection{Definitions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider the system of equations
    \begin{equation}
      \begin{array}{ccccccccc}
        a_{11}x_1 & + & a_{12}x_2 & + & \dotsb & + & a_{1n}x_n & = & b_1    \\
        a_{21}x_1 & + & a_{22}x_2 & + & \dotsb & + & a_{2n}x_n & = & b_2    \\
        \vdots    &   & \vdots    &   & \ddots &   & \vdots    &   & \vdots \\
        a_{m1}x_1 & + & a_{m2}x_2 & + & \dotsb & + & a_{mn}x_n & = & b_m
      \end{array}\tag{$\ast$}\label{eq:augdef}
    \end{equation}
    \pause
    The \emph{augmented matrix} of \eqref{eq:augdef} is
    \[
      \left[
        \begin{array}{cccc|c}
          a_{11} & a_{12}  & \dotsb & a_{1n}                  & b_{1} \\ %&\tikzmark{upper1} a_{} & a_{} & a_{}\tikzmark{upper2} \\
          a_{21} & a_{22}  & \dotsb & a_{2n}                  & b_{2} \\ %& a_{}                  & a_{} & a_{}                  \\
          \vdots & \vdots & \ddots & \vdots                 & \vdots \\
          \tikzmark{lower1}a_{m1} & a_{m2}  & \dotsb & a_{mn}\tikzmark{lower2} & b_{m}\tikzmark{bcol}    %& a_{}                  & a_{} & a_{}                  \\
        \end{array}
      \right]
    \]
    % code from https://tex.stackexchange.com/questions/102460/underbraces-in-matrix-divided-in-blocks
    \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
      \pause
      \draw[decorate, thick] (lower2.south) -- (lower1.south)
      node [midway,below=5pt] {{\tiny``coefficient matrix''}};%
      \pause
      \node at (bcol.south) [below=-4pt] {{\footnotesize$\underset{\textnormal{``augmented column''}}{\uparrow}$}};
    \end{tikzpicture}
  \end{definition}
\end{frame}


\begin{sagesilent}
  A = matrix([[3,4,-8],[0,1,10],[9,7,-2]])
  b = vector([0,6,-5])
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    There is a natural correspondence between systems of linear equations and
    augmented matrices.
    \begin{align*}
      \begin{array}{rcrcrcr}
        3\,x & + & 4\,y & - & 8\,z  & = & 0 \\
             &   &    y & + & 10\,z & = & 6 \\
        9\,x & + & 7\,y & - & 2\,z  & = & -5
      \end{array} && \longleftrightarrow &&
                                            \sage{M}
    \end{align*}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A matrix is in \emph{reduced row-echelon form} (rref) if
    \begin{enumerate}
    \item<2-> any zero-rows occur at the bottom
    \item<3-> the first nonzero entry of a nonzero row is $1$ (called a
      \emph{pivot})
    \item<4-> every pivot occurs to the right of any previous pivots
    \item<5-> all nonpivot entries in a column containing a pivot are zero
    \end{enumerate}
  \end{definition}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
      \begin{tikzpicture}[line cap=round, line join=round]
        \matrix [
        inner sep=0pt, %<- set inner sep for all nodes
        column sep=.33333em,
        nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
        matrix of math nodes,
        left delimiter=\lbrack,
        right delimiter=\rbrack
        ] (m) {
          1 \pgfmatrixnextcell 2 \pgfmatrixnextcell 0 \pgfmatrixnextcell -5 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell -1 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 3 \pgfmatrixnextcell 0 \pgfmatrixnextcell -1 \pgfmatrixnextcell 1 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell -1 \pgfmatrixnextcell -2 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
        };%
        \onslide<4->{\boxpivot{1-1}}
        \onslide<6->{\boxpivot{2-3}}
        \onslide<8->{\boxpivot{3-5}}
      \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Yes. }%
    \onslide<3->{The pivots are in positions $(1,1)$, }%
    \onslide<5->{$(2,3)$, }%
    \onslide<7->{and $(3, 5)$.}
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
      \begin{tikzpicture}[line cap=round, line join=round]
        \matrix [
        inner sep=0pt, %<- set inner sep for all nodes
        column sep=.33333em,
        nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
        matrix of math nodes,
        left delimiter=\lbrack,
        right delimiter=\rbrack
        ] (m) {
          1 \pgfmatrixnextcell 2 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 3 \\
          0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 3 \pgfmatrixnextcell 1 \pgfmatrixnextcell 1 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 2 \\
        };%
        \onslide<4->{\boxpivot{1-1}}
        \onslide<6->{\boxpivot{2-2}}
        \onslide<8->{\boxpivot{3-4}}
        \onslide<10->{\draw[ultra thick, color=red] (m-1-2.north west) -- (m-3-2.south west) -- (m-3-2.south east) -- (m-1-2.north east) -- cycle;}
        \onslide<11->{\draw[ultra thick, color=red] (m-1-4.north west) -- (m-3-4.south west) -- (m-3-4.south east) -- (m-1-4.north east) -- cycle;}
      \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{No. }%
    \onslide<3->{There are pivots in positions $(1, 1)$, }%
    \onslide<5->{$(2, 2)$, }%
    \onslide<7->{and $(3, 4)$. }%
    \onslide<9->{However, the pivots in columns two and four are not the only
      nonzero entries in these columns.}%
  \end{block}
\end{frame}




\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is the matrix
    \[
      \begin{tikzpicture}[line cap=round, line join=round]
        \matrix [
        inner sep=0pt, %<- set inner sep for all nodes
        column sep=.33333em,
        nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
        matrix of math nodes,
        left delimiter=\lbrack,
        right delimiter=\rbrack
        ] (m) {
          0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell -2 \\
          1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 1 \pgfmatrixnextcell  1 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell  0 \\
        };%
        \onslide<4->{\boxpivot{1-2}}
        \onslide<6->{\boxpivot{2-1}}
        \onslide<8->{\draw[ultra thick, color=red]
          (m-2-1.north west)
          -- (m-1-2.north west)
          -- (m-1-2.north east)
          -- (m-1-2.south east)
          -- (m-2-1.south east)
          -- (m-2-1.south west)
          -- cycle;}
      \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{No. }%
    \onslide<3->{There are pivots in positions $(1, 2)$ }%
    \onslide<5->{and $(2, 1)$. }%
    \onslide<7->{However, the pivot in column one occurs to the left of the
      pivot in column two.}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \begin{example}
    Is the matrix
    \[
      \begin{tikzpicture}[line cap=round, line join=round]
        \matrix [
        inner sep=0pt, %<- set inner sep for all nodes
        column sep=.33333em,
        nodes={inner sep=.3333em, anchor=base east}, %<- set another inner sep for inner nodes,
        matrix of math nodes,
        left delimiter=\lbrack,
        right delimiter=\rbrack
        ] (m) {
          0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 6 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell -4 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \pgfmatrixnextcell 2 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 1 \pgfmatrixnextcell 0 \\
          0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \pgfmatrixnextcell 0 \\
        };%
        \onslide<4->{\boxpivot{1-2}}
        \onslide<6->{\boxpivot{2-4}}
        \onslide<8->{\boxpivot{3-5}}
      \end{tikzpicture}
    \]
    in reduced row-echelon form? If so, where are the pivots?
  \end{example}
  \begin{block}{\onslide<2->{Solution}}
    \onslide<2->{Yes. }%
    \onslide<3->{The pivots are in positions $(1,2)$, }%
    \onslide<5->{$(2,4)$, }%
    \onslide<7->{and $(3,5)$.}
  \end{block}
\end{frame}


\subsection{Consistency and Dependency}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A rref system is
    \begin{description}
    \item<+->[consistent] if the augmented column contains no pivot
    \item<+->[inconsistent] if the augmented column contains a pivot
    \end{description}
  \end{definition}

  \onslide<+->
  \begin{definition}
    A column in a consistent rref matrix corresponds to a
    \begin{description}[dependent variable]
    \item<+->[free variable] if the column contains no pivot
    \item<+->[dependent variable] if the column contains a pivot
    \end{description}
  \end{definition}

\end{frame}


\subsection{Solving rref Systems}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Idea}
    Solving consistent rref systems is \emph{easy}: write the dependent
    variables in terms of the free variables.
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
      \left[
        \begin{array}{rrrr|r}
          \tikzmark{x1}1 & \tikzmark{x2}0 & \tikzmark{x3}0 & \tikzmark{x4}0 & 4 \\
          0 & 0 & 1 & 2 & -1 \\
          0 & 0 & 0 & 0 & 0
        \end{array}
      \right]
      \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
        \node at (x1.north east) {$x_1$};
        \node at (x2.north east) {$x_2$};
        \node at (x3.north east) {$x_3$};
        \node at (x4.north east) {$x_4$};
      \end{tikzpicture}
    \]
    is consistent.%
    \onslide<2->{The free variables are }%
    \onslide<3->{$\Set{x_2,x_4}$ }%
    \onslide<4->{and the dependent variables are }%
    \onslide<5->{$\Set{x_1,x_3}$. }%
    \onslide<6->{The solutions are}
    \[
      \onslide<7->{\left[\begin{array}{c} x_1\\ x_2\\ x_3\\ x_4\end{array}\right]=}%
      \onslide<8->{\left[}\begin{array}{c} \onslide<9->{4}\\ \onslide<10->{x_2}\\ \onslide<11->{-1-2\,x_4}\\ \onslide<12->{x_4}\end{array}\onslide<8->{\right]}%
      \onslide<13->{= \left[}\begin{array}{r} \onslide<14->{4\\ 0\\ -1\\ 0}\end{array}\onslide<13->{\right]}
      \onslide<13->{+x_2\left[}\begin{array}{r} \onslide<15->{0\\ 1\\ 0\\ 0}\end{array}\onslide<13->{\right]}
      \onslide<13->{+x_4\left[}\begin{array}{r} \onslide<16->{0\\0\\-2\\1}\end{array}\onslide<13->{\right]}
    \]%
    \onslide<17->{Note that there are \emph{infinitely many solutions}.}%
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
      \left[
        \begin{array}{rrr|r}
          \tikzmark{x1}1 & \tikzmark{x2}0 & \tikzmark{x3}0 & 3 \\
          0 & 1 & 0 & -4 \\
          0 & 0 & 1 &  8
        \end{array}
      \right]
      \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
        \node at (x1.north east) {$x_1$};%
        \node at (x2.north east) {$x_2$};%
        \node at (x3.north east) {$x_3$};%
      \end{tikzpicture}
    \]
    is consistent with no free variables. \pause The only solution is
    \[
      \left[\begin{array}{c} x_1\\ x_2\\ x_3\end{array}\right]
      = \left[\begin{array}{r} 3\\ -4\\ 8\end{array}\right]
    \]
    \pause
    Note that there is \emph{exactly one solution}.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The rref system
    \[
      \left[
        \begin{array}{rrrr|r}
          \tikzmark{x1}0 & \tikzmark{x2}1 & \tikzmark{x3}0 & \tikzmark{x4}0 & 0 \\
          0 & 0 & 0 & 1 & 0 \\
          0 & 0 & 0 & 0 & 1
        \end{array}
      \right]
      \begin{tikzpicture}[overlay, remember picture,decoration={brace,amplitude=5pt}]
        \node at (x1.north east) {$x_1$};%
        \node at (x2.north east) {$x_2$};%
        \node at (x3.north east) {$x_3$};%
        \node at (x4.north east) {$x_4$};%
      \end{tikzpicture}
    \]
    is inconsistent. \pause The last equation is $0=1$, which is impossible! \pause This
    system has \emph{no solutions}.
  \end{example}
\end{frame}


\subsection{Counting Solutions}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A consistent rref system has
    \begin{description}[infinitely many solutions]
    \item<+->[\emph{a unique solution}] if it has no free variables
    \item<+->[\emph{infinitely many solutions}] if it has at least one free variable
    \end{description}\onslide<+->
    An inconsistent rref system has no solutions.
  \end{theorem}
\end{frame}




\section{Rank and Nullity}
\subsection{Rank}

\begin{sagesilent}
  set_random_seed(29152938)
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=2)
  A = A.rref()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{rank} of a matrix in reduced row echelon form is the number of
    pivots.
  \end{definition}

  \pause
  \begin{example}
    The matrix
    \[
      A=\sage{A}
    \]
    is in reduced row echelon form.
    \pause
    The rank of $A$ is $\rank(A)=\sage{A.rank()}$.
  \end{example}
\end{frame}


\begin{sagesilent}
  set_random_seed(1329485)
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{rank} of a consistent system is the number of pivots in its
    rref. Equivalently, the rank is the number of dependent variables.
  \end{definition}

  \pause
  \begin{example}
    The system
    \[
      \sage{M.rref()}
    \]
    is consistent and in reduced row echelon form.  \pause Thus the rank of the
    system is \pause three.
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Every consistent system satisfies
    \begin{align*}
      \rank\leq\#\textnormal{variables}
      &&
         \text{and}
      &&
         \rank\leq\#\textnormal{equations}
    \end{align*}
  \end{theorem}

  \onslide<+->
  \begin{theorem}
    Every consistent system has
    \begin{description}[infinitely many solutions]
    \item<+->[\emph{a unique solution}] if $\rank=\#\textnormal{variables}$
    \item<+->[\emph{infinitely many solutions}] if
      $\rank<\#\textnormal{variables}$
    \end{description}
  \end{theorem}
\end{frame}


\subsection{Nullity}

\begin{sagesilent}
  set_random_seed(490895)
  A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=3)
  while A.pivots() != (0, 2, 3): A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=3)
  A = A.rref()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{nullity} of a matrix in reduced row echelon form is the number of
    nonpivot columns.
  \end{definition}

  \pause
  \begin{example}
    The matrix
    \[
      A=\sage{A}
    \]
    is in reduced row echelon form. The nullity of $A$ is
    $\nullity(A)=\sage{A.right_nullity()}$.
  \end{example}

\end{frame}







\begin{sagesilent}
  A = random_matrix(ZZ, 4, 5, algorithm='echelonizable', rank=3)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{nullity} of a consistent system in reduced row echelon form is the
    number of nonpivot columns of in the coefficient matrix of its
    rref. Equivalently, the nullity is the number of free variables.
  \end{definition}

  \pause
  \begin{example}
    The system
    \[
      \sage{M.rref()}
    \]
    is consistent and in reduced row echelon form.
    \pause
    The nullity of the system is \pause one.
  \end{example}
\end{frame}



\begin{sagesilent}
  A = random_matrix(ZZ, 4, 6, algorithm='echelonizable', rank=2)
  coeffs = A.columns()[:-1]
  b = A.columns()[-1]
  M = matrix.column(coeffs).augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A consistent system has
    \begin{description}[infinitely many solutions]
    \item<+->[\emph{a unique solution}] if $\text{nullity}=0$
    \item<+->[\emph{infinitely many solutions}] if $\text{nullity}>0$
    \end{description}
  \end{theorem}

  \onslide<+->
  \begin{example}
    The system
    \[
      \sage{M.rref()}
    \]%
    is consistent and in reduced row echelon form. \onslide<+-> Here, we have
    $\text{rank}=\onslide<+->2$ \onslide<+-> and
    $\text{nullity}=\onslide<+->3>0$. \onslide<+-> Thus the system has
    infinitely many solutions.
  \end{example}
\end{frame}


\subsection{The Rank-Nullity Theorem}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Rank-Nullity Theorem]
    Every consistent system in reduced row echelon form satisfies
    \[
      \rank+\nullity=\#\textnormal{variables}
    \]
  \end{theorem}

  \onslide<2->
  \begin{block}{Summary}
    Every consistent system in reduced row echelon form has
    \begin{description}[infinitely many solutions]
    \item<3->[a unique solution] if $\rank=\#\text{variables}$, or $\nullity=0$
    \item<4->[infinitely many solutions] if $\rank<\#\text{variables}$, or
      $\text{nullity}>0$
    \end{description}
  \end{block}
\end{frame}

\end{document}
