\documentclass[usenames,dvipsnames]{beamer}

\usepackage{mathtools} % loads amsmath
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{blkarray, bigstrut}
\usepackage[d]{esvect}%
\usepackage{graphicx}%
\usepackage{multicol}
\usepackage{hyperref}
\hypersetup{colorlinks, linkcolor=., urlcolor=blue}
\usepackage{sagetex}
\usepackage{siunitx}
\newcommand{\mydollars}[1]{\SI[round-precision=2,round-mode=places,round-integer-to-decimal]{#1}[\$]{}}
\DeclareSIUnit{\mph}{mph}
\usepackage{xparse}
\usepackage{xfrac}
% \usepackage{showframe}          % for testing
\usepackage{tikz}
\usetikzlibrary{
  , arrows
  , automata
  , calc
  , cd
  , decorations
  , decorations.pathmorphing
  , decorations.pathreplacing
  , fit
  , matrix
  , positioning
  , shapes
  , shapes.geometric
}
\usepackage{tikz-3dplot}
\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%



\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\boxpivot}[1]{
  \filldraw[draw=blue, thick, fill=blue!20, fill opacity=.25] (m-#1.north east) -- (m-#1.north west) -- (m-#1.south west) -- (m-#1.south east) -- cycle;
}
\newcommand{\boxcell}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-#3.north east) -- (#1-#2-#3.north west) -- (#1-#2-#3.south west) -- (#1-#2-#3.south east) -- cycle;
}
\newcommand{\boxrow}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-#2-1.north west) -- (#1-#2-1.south west) -- (#1-#2-#3.south east) -- (#1-#2-#3.north east) -- cycle;
}
\newcommand{\boxcol}[4]{
  \filldraw[draw=#4, thick, fill=#4!20, fill opacity=.25, rounded corners] (#1-1-#2.north west) -- (#1-#3-#2.south west) -- (#1-#3-#2.south east) -- (#1-1-#2.north east) -- cycle;
}



\ExplSyntaxOn
\NewDocumentCommand{\gcenter}{m}
{
  \begin{center}
    \seq_set_split:Nnn \l_tmpa_seq { \\ } { #1 }
    \seq_map_inline:Nn \l_tmpa_seq
    {
      \seq_set_split:Nnn \l_tmpb_seq { & } { ##1 }
      \seq_use:Nn \l_tmpb_seq { \hfil }
      \\
    }
  \end{center}
}
\ExplSyntaxOff

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\nullity}{nullity}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}

\setbeamertemplate{caption}{\raggedright\insertcaption\par}


\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\RR}{\mathbb{R}}

\let\oldfrac\frac% Store \frac
\renewcommand{\frac}[2]{%
  \mathchoice
  {\oldfrac{#1}{#2}}% display style
  {\sfrac{#1}{#2}}% text style
  {\sfrac{#1}{#2}}% script style
  {\sfrac{#1}{#2}}% script-scr#ipt style
}

\theoremstyle{definition}
\newtheorem{algorithm}{Algorithm}


\title{Nonsingular Matrices}
\subtitle{Math 790-92}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}


\titlegraphic{\includegraphics[scale=.175]{../../dukemath.pdf}}

\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
  % \begin{columns}[onlytextwidth, t]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={1-3}]
  %   \column{.5\textwidth}
  %   \tableofcontents[sections={4-}]
  % \end{columns}
\end{frame}


\section{Motivation}
\subsection{What We Know}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    A $m\times n$ matrix $A$ has
    \begin{description}[<+->][full column rank]
    \item[full row rank] if $\rank(A)=m$
    \item[full column rank] if $\rank(A)=n$
    \item[full rank] if $\rank(A)=m$ or $\rank(A)=n$
    \end{description}
  \end{block}

  \onslide<+->
  \begin{block}{Recall}
    A \emph{nonsingular matrix} is a square matrix with full rank. \onslide<+->
    A \emph{singular matrix} is a square matrix with nonfull rank.
  \end{block}
\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    For a $n\times n$ matrix $A$, the following are equivalent.
    \begin{itemize}[<+->]
    \item $A$ is nonsingular
    \item $\rank(A)=n$
    \item $\nullity(A)=0$
    \end{itemize}
  \end{theorem}

  \onslide<+->{This is a consequence of the \emph{rank-nullity theorem}.}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Every system can be written as $A\vv{x}=\vv{b}$.
  \end{block}

  \pause
  \begin{block}{Idea}
    Divide $A\vv{x}=\vv{b}$ by $A$. This gives the solution $\vv{x}=\frac{\vv{b}}{A}$.
  \end{block}

  \pause
  \begin{block}{Problem}
    We do not know how to perform matrix division!
  \end{block}

\end{frame}


\subsection{What We Want}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    Every nonzero scalar $c$ has a ``reciprocal'' $c^{-1}$ satisfying
    $c^{-1}c=cc^{-1}=1$.
  \end{block}

  \pause
  \begin{block}{Question}
    Does every matrix $A$ have a ``reciprocal'' $A^{-1}$? This ``reciprocal''
    would satisfy $A^{-1}A=AA^{-1}=I$. \pause Then $A\vv{x}=\vv{b}$ could be
    left-multiplied by $A^{-1}$ to obtain the solution $\vv{x}=\pause A^{-1}\vv{b}$.
  \end{block}

  \pause
  \begin{block}{Answer}
    Only some matrices have ``reciprocals.'' These are called \emph{invertible
      matrices} and the ``reciprocal'' is called the \emph{inverse matrix}.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Question 1}
    Which matrices are invertible?
  \end{block}

  \pause
  \begin{block}{Question 2}
    How do we find the inverse of a matrix?
  \end{block}

  \pause
  \begin{block}{Question 3}
    How are matrix inverses useful?
  \end{block}

\end{frame}



\section{Invertible Matrices}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. A $n\times m$ matrix $B$ is a
    \begin{description}[<+->][right inverse of $A$]
    \item[left inverse of $A$] if $BA=I_n$ ($A$ is ``left invertible'')
    \item[right inverse of $A$] if $AB=I_m$ ($A$ is ``right invertible'')
    \item[inverse of $A$] if $BA=I_n$ and $AB=I_m$ ($A$ is ``invertible'')
    \end{description}
  \end{definition}

\end{frame}



\begin{sagesilent}
  A = matrix([(9, -16), (4, -7), (-2, 8)])
  xp = vector([-7, 16, 0])
  yp = vector([-4, 9, 0])
  n = vector([18, -40, 1])
  B1 = matrix([xp+n, yp-n])
  B2 = matrix([xp-n, yp+n])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \underset{B_1}{\sage{B1}}\underset{A}{\sage{A}} &= \sage{B1*A}=I_2 \\
      \onslide<2->{\underset{B_2}{\sage{B2}}\underset{A}{\sage{A}} &= \sage{B2*A}=I_2}
    \end{align*}
    \onslide<3->{``$A$ is \emph{left invertible} and $B_1$ and $B_2$ are
      \emph{left inverses} of $A$''}
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix([(3, 7, 4), (2, 5, 3)])
  xp = -vector([-5, 2, 0])
  yp = -vector([7, -3, 0])
  n = vector([1, -1, 1])
  B1 = matrix.column([xp+2*n, yp-n])
  B2 = matrix.column([xp-n, yp+3*n])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \underset{A}{\sage{A}}\underset{B_1}{\sage{B1}} &= \sage{A*B1}=I_2 \\
      \onslide<2->{\underset{A}{\sage{A}}\underset{B_2}{\sage{B2}} &= \sage{A*B2}=I_2}
    \end{align*}
    \onslide<3->{``$A$ is \emph{right invertible} and $B_1$ and $B_2$ are
      \emph{right inverses} of $A$''}
  \end{example}

\end{frame}



\begin{sagesilent}
  set_random_seed(4579725)
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  B = A.inverse()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \underset{B}{\sage{B}}\underset{A}{\sage{A}} &= \sage{B*A}=I_3 \\
      \onslide<2->{\underset{A}{\sage{A}}\underset{B}{\sage{B}} &= \sage{A*B}=I_3}
    \end{align*}
    \onslide<3->{``$A$ is \emph{invertible} and $B$ is an \emph{inverse} of
      $A$''}
  \end{example}

\end{frame}



\begin{sagesilent}
  A = matrix([(1, 0, 0), (0, 0, 0)])
  var('a1 a2 a3 b1 b2 b3')
  B = matrix.column([(a1, a2, a3), (b1, b2, b3)])
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \begin{align*}
      \underset{L}{\sage{B}}\underset{A}{\sage{A}} &= \sage{B*A} \neq I_3 \\
      \onslide<2->{\underset{A}{\sage{A}}\underset{R}{\sage{B}} &= \sage{A*B} \neq I_2}
    \end{align*}
    \onslide<3->{``$A$ is \emph{not left invertible} and \emph{not right
        invertible}''}
  \end{example}

\end{frame}



\subsection{Facts}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Left and Right Invertibility of Nonsquare Matrices]
    A nonsquare $m\times n$ matrix $A$ is
    \begin{description}[<+->][not left or right invertible]
    \item[left invertible] with $\infty$ left inverses if $\rank(A)=n$
    \item[right invertible] with $\infty$ right inverses if $\rank(A)=m$
    \item[not left or right invertible] if $A$ is not full rank
    \end{description}
  \end{theorem}

  \onslide<+->
  \begin{block}{Note}
    According to this theorem, nonsquare matrices cannot have both left inverses
    and right inverses. \onslide<+->Rectangular matrices cannot be invertible!
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(479274)
  A = random_matrix(ZZ, 2, 3, algorithm='echelonizable', rank=2)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\underset{A}{\sage{A}}=\sage{A.rref()}
    \]\pause
    Since $\rank(A)=\sage{A.rank()}$, $A$ is right invertible but not left
    invertible.
  \end{example}

\end{frame}




\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a square $n\times n$ matrix. If one of the equations
    \begin{align*}
      AB &= I_n & BA &= I_n
    \end{align*}
    holds, then so does the other equation.
  \end{theorem}

  \pause
  \begin{block}{Note}
    According to this theorem, to check if $B$ is an inverse of a square matrix
    $A$, we only need to check one of $BA=I$ and $AB=I$.
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    If $B$ and $C$ are inverses of a square matrix $A$, then $B=C$.
  \end{theorem}\pause
  \begin{proof}
    $B=\pause BI=\pause B(AC)= \pause (BA)C= \pause IC=\pause C$\qedhere
  \end{proof}


  \pause
  \begin{block}{Note}
    This theorem says that ``inverses are unique.''
  \end{block}

  \pause
  \begin{definition}
    Suppose that $A$ is invertible. The inverse of $A$ is denoted by $A^{-1}$.
  \end{definition}

\end{frame}



\begin{sagesilent}
  set_random_seed(4797592)
  A = random_matrix(ZZ, 3, algorithm='unimodular')
  B = A.inverse()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Note that
    \[
      \underset{A}{\sage{A}}\underset{B}{\sage{B}} = \sage{A*B} = I_3
    \]\pause
    Our previous theorem tells us that $BA=I_3$ too. \pause Thus $B$ is the
    inverse of $A$ \pause and we write
    \[
      A^{-1}=\sage{B}
    \]
  \end{example}

\end{frame}



\subsection{Properties of Inverses}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of Inverses}
    The inversion operation satifies the following properties.
    \begin{description}[<+->][transposition rule]
    \item[involution rule] $(A^{-1})^{-1}=A$
    \item[order reversing] $(AB)^{-1}=B^{-1}A^{-1}$
    \item[scaling rule] $(c\cdot A)^{-1}=(\frac{1}{c})\cdot A^{-1}$
    \item[transposition rule] $(A^\intercal)^{-1}=(A^{-1})^\intercal$
    \end{description}
  \end{block}

  \onslide<+->
  \begin{block}{Reminder}
    $A\vv{x}=\vv{b}$ is quickly solved by $\vv{x}=A^{-1}\vv{b}$
  \end{block}


\end{frame}




\section{Algorithms for Computing Inverses}
\subsection{The Augmentation Method}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    Let $A$ be a $n\times n$ matrix. The following steps will compute $A^{-1}$
    or determine that $A^{-1}$ does not exist.
    \begin{description}[<+->]
    \item[Step 1] Form the augmented matrix $[A\mid I_n]$.
    \item[Step 2] Row-reduce $[A\mid I_n]\rightsquigarrow[\rref(A)\mid B]$.
    \item[Step 3] If $\rref(A)\neq I_n$, then $A$ is not invertible.
    \item[Step 4] If $\rref(A)=I_n$, then $B=A^{-1}$.
    \end{description}
  \end{block}

\end{frame}



\begin{sagesilent}
  I2 = identity_matrix(2)
  A = matrix([(-1, -1), (-1, 2)])
  M = A.augment(I2, subdivide=True)
  E1 = elementary_matrix(2, row1=0, scale=-1)
  E2 = elementary_matrix(2, row1=1, row2=0, scale=1)
  E3 = elementary_matrix(2, row1=1, scale=1/3)
  E4 = elementary_matrix(2, row1=0, row2=1, scale=-1)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the row-reductions
    \begin{align*}
      \sage{M}
      &\onslide<2->{\xrightarrow{-R_1\to R_1}                       \sage{E1*M}} \\
      &\onslide<3->{\xrightarrow{R_2+R_1\to R_2}                    \sage{E2*E1*M}} \\
      &\onslide<4->{\xrightarrow{(\frac{1}{3})\cdot R_2\to R_2} \sage{E3*E2*E1*M}} \\
      &\onslide<5->{\xrightarrow{R_1-R_2\to R_1}                    \sage{E4*E3*E2*E1*M}}
    \end{align*}%
    \onslide<6->{Thus $A=\sage{A}$ is invertible and $A^{-1}=\sage{A.inverse()}$.}%
  \end{example}
\end{frame}



\begin{sagesilent}
  set_random_seed(7987627927777)
  A = random_matrix(ZZ, 3)
  M = A.augment(identity_matrix(A.nrows()), subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]\pause
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]\pause
    The matrix $A$ is invertible and
    \[
      A^{-1}=\sage{A.inverse()}
    \]
  \end{example}

\end{frame}



\begin{sagesilent}
  set_random_seed(7979502)
  A = random_matrix(ZZ, 3, algorithm='echelonizable', rank=2)
  M = A.augment(identity_matrix(A.nrows()), subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ given by
    \[
      A=\sage{A}
    \]\pause
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]\pause
    The matrix $A$ is \emph{not invertible}.
  \end{example}

\end{frame}



\section{Linear Algebra in a Nutshell}
\subsection{Invertible Means Nonsingular}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    Each of our algorithms states that a $n\times n$ matrix $A$ is invertible if
    and only if $\rank(A)=\pause n$. \pause This means that ``invertible matrices'' and
    ``nonsingular matrices'' are the same!
  \end{block}

\end{frame}



\subsection{Partial Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Linear Algebra in a Nutshell}
    For a $n\times n$ matrix, each of the following statements is equivalent.
    \begin{itemize}[<+->]
    \item $A$ is nonsingular
    \item $\rank(A)=n$
    \item $\nullity(A)=0$
    \item $A$ is invertible
    \item $A^{-1}$ exists
    \item $A\vv{x}=\vv{O}$ has only the ``trivial solution'' $\vv{x}=\vv{O}$
    \item every system $A\vv{x}=\vv{b}$ is consistent
    \item every system $A\vv{x}=\vv{b}$ has a unique solution
    \item $\rref(A)=I_n$
    \end{itemize}
  \end{block}

\end{frame}



\end{document}
