\documentclass{beamer}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{cite}
\usepackage[d]{esvect}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multicol}
\usepackage{nicefrac}
\usepackage{sagetex}
\usepackage{siunitx}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows, calc, decorations.pathreplacing, matrix, positioning}
\usepackage{tikz-3dplot}
\usepackage{tikz-cd}

\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\text{\ding{51}}}%
\newcommand{\xmark}{\text{\ding{55}}}%


\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\comp}{comp}
\DeclareMathOperator{\area}{area}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\rref}{rref}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\myrows}{rows}
\DeclareMathOperator{\mycolumns}{columns}
\DeclareMathOperator{\Row}{Row}
\DeclareMathOperator{\Col}{Col}
\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Null}{Null}
\DeclareMathOperator{\LNull}{LNull}
\DeclareMathOperator{\Span}{Span}

\newcommand{\mybold}[1]{{\usebeamercolor[fg]{example text}{#1}}}
\newcommand{\myotherbold}[1]{{\usebeamercolor[fg]{title}{#1}}}

\DeclarePairedDelimiter\abs{\lvert}{\rvert}%
\DeclarePairedDelimiter\norm{\lVert}{\rVert}%

\providecommand\given{} % so it exists
\newcommand\SetSymbol[1][]{
  \nonscript\,#1\vert \allowbreak \nonscript\,\mathopen{}}
\DeclarePairedDelimiterX\Set[1]{\lbrace}{\rbrace}%
{ \renewcommand\given{\SetSymbol[\delimsize]} #1 }

\newcommand{\semitransp}[2][35]{\color{fg!#1}#2}
\newcommand{\myhide}[2]{\makebox[0pt][l]{#1}\phantom{#2}}
\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture,baseline=(#1.base)]
  \node (#1) {\strut};}
\newcommand{\mybmat}[1]{
  \begin{bmatrix}
    #1
  \end{bmatrix}}




\title{Vector Spaces}
\subtitle{Math 790-92}
\author{Brian D.\ Fitzpatrick}
\institute{Duke University}
\date{\today}
\titlegraphic{\includegraphics[scale=.175]{../../dukemath.pdf}}


\usepackage{currfile}
\usepackage{xstring}


% https://tex.stackexchange.com/questions/91691/beamer-set-mode-mid-presentation
\makeatletter
\newcommand\changemode[1]{%
  \gdef\beamer@currentmode{#1}}
\makeatother


\begin{document}

\IfSubStr*{\currfilename}{handout}{
  \changemode{handout}% options are handout, beamer, trans
}{}

\begin{sagesilent}
  latex.matrix_delimiters(left='[', right=']')
  latex.vector_delimiters(left='<', right='>')
\end{sagesilent}


\begin{frame}
  \titlepage
\end{frame}


\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Definitions}
\subsection{Subcollections of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    If $V$ is a subcollection of $\mathbb{R}^n$, then we write
    $V\subset\mathbb{R}^n$ or $V\subseteq\mathbb{R}^n$.
  \end{definition}


  \pause
  \begin{example}
    Let $V=\Set*{\langle1,0\rangle, \langle3, -34\rangle,
      \langle22,-22\rangle}$. Then $V\subset\mathbb{R}^2$.
  \end{example}

  \pause
  \begin{example}
    Let $V$ be the subcollection of $\mathbb{R}^3$ consisting of all vectors
    $\vv{v}=\langle v_1,v_2,v_3\rangle$ where $v_3=11$. Then
    $V\subset\mathbb{R}^3$. \pause %
    Note that $\langle2,-22,11\rangle\in V$ but $\langle-2,1,0\rangle\notin V$.
  \end{example}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Set-Builder Notation]
    The notation $V=\Set*{\vv{v}\in\mathbb{R}^n\given P}$ means ``$V$ consists
    of all $\vv{v}\in\mathbb{R}^n$ that satisfy the property $P$.''
  \end{definition}

  \pause

  \begin{example}
    Let $V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2\geq 0}$. Then
    $\langle 1, 3\rangle\in V$ but $\langle1,-3\rangle\notin V$.
  \end{example}

  \pause

  \begin{example}
    Let $S=\Set*{\vv{v}\in\mathbb{R}^4\given\norm{\vv{v}}=1}$. Then $\langle1,
    0, 0, 0\rangle\in S$ but $\langle1, 1, 1, 1\rangle\notin S$.
  \end{example}
\end{frame}


\subsection{Vector Subspaces of $\mathbb{R}^n$}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A subcollection $V$ of $\mathbb{R}^n$ is a \emph{vector subspace of
      $\mathbb{R}^n$} if
    \begin{description}[<+->][$V$ respects addition]
    \item[$V$ respects zero] $\vv{O}\in V$
    \item[$V$ respects addition] $\vv{v},\vv{w}\in V$ implies $\vv{v}+\vv{w}\in V$
    \item[$V$ respects scaling] $c\in\mathbb{R}$ and $\vv{v}\in V$ implies $c\cdot\vv{v}\in V$
    \end{description}
  \end{definition}

  \onslide<+->
  \begin{block}{Idea}
    Vector subspaces of $\mathbb{R}^n$ are exactly the subcollections of
    $\mathbb{R}^n$ that are ``closed'' under linear combinations.
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $V=\Set*{\langle v_1,v_2\rangle\in\mathbb{R}^2\given v_2=1}$ a vector
    subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check each axiom individually.
    \begin{enumerate}[<+->]
    \item $\vv{O}\notin V$ since $\vv{O}=\langle0,0\rangle$ but $0\neq
      1$ \onslide<+->\xmark
    \item $\langle0,1\rangle\in V$ and $\langle1,1\rangle\in V$
      but $\langle0,1\rangle+\langle1,1\rangle\notin V$ \onslide<+->\xmark
    \item $\langle0,1\rangle\in V$ but
      $2\cdot\langle0,1\rangle=\langle0,2\rangle\notin V$ \onslide<+->\xmark
    \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $V=\Set*{\vv{v}\in\mathbb{R}^3\given \norm{\vv{v}}=1}$ a vector subspace
    of $\mathbb{R}^3$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check each axiom individually.
    \begin{enumerate}[<+->]
    \item $\vv{O}\notin V$ since $\norm{\vv{O}}=0\neq 1$ \onslide<+->\xmark
    \item $\langle1, 0, 0\rangle, \langle0,1,0\rangle\in V$ but
      $\langle1,0,0\rangle+\langle0,1,0\rangle=\langle1,1,0\rangle\notin V$
      \onslide<+->\xmark
    \item $\langle1,0,0\rangle\in V$ but
      $-10\cdot\langle1,0,0\rangle=\langle-10,0,0\rangle\notin V$
      \onslide<+->\xmark
    \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is
    $V=\Set*{\langle x,y\rangle\in\mathbb{R}^2\given x\geq0\textnormal{ and
      }y\geq0}$ a vector subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item $\vv{O}\in V$ since $\vv{O}=\langle0,0\rangle$ \onslide<+->\cmark
    \item Suppose that $\langle v_1,v_2\rangle\in V$ and
      $\langle w_1,w_2\rangle\in V$. \onslide<+->This means that
      $v_1,v_2,w_1,w_2\geq\onslide<+-> 0$. \onslide<+->Then
      \[
        \langle v_1,v_2\rangle+\langle w_1,w_2\rangle=\langle v_1+w_1,v_2+w_2\rangle\in V
      \]
      since $v_1+w_1\geq 0$ and $v_2+w_2\geq 0$. \onslide<+->\cmark
    \item $\langle1,0\rangle\in V$ but
      $-22\cdot\langle1,0\rangle=\langle-22,0\rangle\notin V$ \onslide<+->\xmark
    \end{enumerate}
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $V=\Set*{\vv{v}\in\mathbb{R}^2\given\vv{v}=\langle x, 2\,x\rangle}$ a
    vector subspace of $\mathbb{R}^2$?
  \end{example}

  \pause
  \begin{block}{Solution}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that
      $\vv{O}=\langle0,0\rangle=\langle0, 2\cdot0\rangle\in V$ \onslide<+->\cmark
    \item Suppose that $\langle x, 2\,x\rangle\in V$ and
      $\langle y,2\,y\rangle\in V$. \onslide<+->Then
      \[
        \left\langle x,2\,x\rangle+\langle y,2\,y\rangle
          =\langle x+y, 2\,x+2\,y\right\rangle\in V\quad \onslide<+->\cmark
      \]
    \item Suppose that $\langle x, 2\,x\rangle\in V$. \onslide<+->Then 
      \[
        c\cdot\langle x, 2\,x\rangle
        =\langle cx, 2\,cx\rangle
        \in V\quad \onslide<+->\cmark
      \]
    \end{enumerate}
  \end{block}
\end{frame}


\section{Important Vector Subspaces}
\subsection{Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $A$ be a $m\times n$ matrix and let 
    $
    V=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    $.
    Then $V$ is a vector subspace of $\mathbb{R}^n$.
  \end{theorem}

  \pause
  \begin{proof}
    We can check the axioms individually.
    \begin{enumerate}[<+->]
    \item Note that $\vv{O}\in V$ since $A\vv{O}=\vv{O}$.
    \item Suppose that $\vv{x}\in V$ and that $\vv{y}\in V$. \onslide<+->Then
      \[
        A(\vv{x}+\vv{y})
        = A\vv{x}+A\vv{y}
        = \vv{O}+\vv{O}
        = \vv{O}
      \]
      \onslide<+->Thus $\vv{x}+\vv{y}\in V$.
    \item Suppose that $\vv{x}\in V$. \onslide<+->Then
      $A(c\cdot\vv{x})=c\cdot A\vv{x}=c\cdot\vv{O}=\vv{O}$. \onslide<+->Hence
      $c\cdot\vv{x}\in V$.
    \end{enumerate}
    \onslide<+->All axioms pass!
  \end{proof}
\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The vector subspace
    \[
      \Null(A)=\Set{\vv{x}\in\mathbb{R}^n\given A\vv{x}=\vv{O}}
    \]
    of $\mathbb{R}^n$ is called the \emph{null space of $A$}. 
  \end{definition}

\end{frame}


\begin{sagesilent}
  set_random_seed(14971)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  n1, n2 = A.right_kernel().basis()
  r1, r2 = A.row_space().basis()
  u = n1+n2
  v = n1-n2+r1
  w = 2*n1+n2-r2
  Au, Av, Aw = A*u, A*v, A*w
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A=\sage{A}$. Which of the vectors
    \begin{align*}
      \vv{u} &= \sage{u} & \vv{v} &= \sage{v} & \vv{w} &= \sage{w}
    \end{align*}
    is in $\Null(A)$?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that
    \begin{align*}
      A\vv{u} &= \sage{Au} & A\vv{v} &= \sage{A*v} & A\vv{w} &= \sage{Aw}
    \end{align*}
    \pause Ths means that $\vv{u}\in\Null(A)$ while $\vv{v}\notin\Null(A)$ and
    $\vv{w}\notin\Null(A)$.
  \end{block}
\end{frame}



\begin{sagesilent}
  A = matrix(2, 3, [-3,-4,3,0,6,-1])
  var('x y z')
  vvx = matrix.column([x, y, z])
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the subcollection of $\mathbb{R}^3$ given by
    \[
      V=
      \Set*{\langle x,y,z\rangle\in\mathbb{R}^3\given
        \begin{array}{rcrcrcr}
          -3\,x & - & 4\,y & + & 3\,z & = & 0 \\
                &   & 6\,y & - &    z & = & 0
        \end{array}}
    \]
    Is $V$ a vector subspace of $\mathbb{R}^3$?
  \end{example}

  \pause
  \begin{block}{Solution}
    Note that the equations defining $V$ are exactly the equations given by
    $A\vv{x}=\vv{O}$ where
    \begin{align*}
      A &= \sage{A} & \vv{x} &= \sage{vvx}
    \end{align*}
    \pause This means that $V=\Null(A)$, so $V$ is a vector subspace of
    $\mathbb{R}^3$.
  \end{block}
\end{frame}


\subsection{Span}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We define $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ as the
    collection of all linear combinations of
    $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$.
  \end{definition}

\end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Note}
    The vectors in $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ are
    exactly the vectors $\vv{b}$ of the form
    \[
      \vv{b}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}
    \]
    for some scalars $c_1,c_2,\dotsc,c_k$.
  \end{block}

  \pause
  \begin{block}{Note}
    Every vector $\vv{b}$ of the form
    \[
      \vv{b}=c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}+\dotsb+c_k\cdot\vv*{v}{k}
    \]
    is a vector in $\mathbb{R}^n$. This means that
    $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ is a subcollection of
    $\mathbb{R}^n$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(2, 12, -14, -14), (-5, -19, 24, 24), (-1, -8, 9, 9)])
  b = vector([3,10,-1])
  M = A.augment(b, subdivide=True)
  v1, v2, v3, v4 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{matrix.column(b)}}$ in
    ${\footnotesize\Span\Set*{\sage{v1},\sage{v2},\sage{v3},\sage{v4}}}$?
  \end{example}
  
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause This means that the equation
    \[
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      +c_4\cdot\vv*{v}{4}
      =\vv{b}
    \]
    is unsolvable. \pause Thus
    $\vv{b}\notin\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$.
  \end{block}
\end{frame}


\begin{sagesilent}
  A = matrix([(16, -64, 32), (5, -19, 10), (5, -20, 10)])
  b = matrix.column([-160, -48, -50])
  M = A.augment(b, subdivide=True)
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is $\vv{b}={\footnotesize\sage{b}}$ in $\Span{\footnotesize\Set*{\sage{v1},
        \sage{v2}, \sage{v3}}}$?
  \end{example}
  
  \pause
  \begin{block}{Solution}
    Note that
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]
    \pause This means that the equation
    \[
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      =\vv{b}
    \]
    has infinitely many solutions. \pause Thus
    $\vv{b}\in\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3}}$.
  \end{block}
\end{frame}



\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myAcols}{
    \begin{bmatrix}
      \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k}
    \end{bmatrix}}
  \newcommand{\myAcolsb}{
    \begin{bmatrix}
      \vv*{v}{1} & \vv*{v}{2} & \dotsb & \vv*{v}{k} & \mid & \vv{b}
    \end{bmatrix}}
  \begin{theorem}
    A vector $\vv{b}$ is in $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$
    if and only if the system $\myAcolsb$ is consistent.
  \end{theorem}

  \pause
  {\footnotesize
    \begin{corollary}
      $\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}=\mathbb{R}^n$ if and
      only if $\rank\myAcols=\#\myrows$
    \end{corollary}}
\end{frame}



\begin{sagesilent}
  set_random_seed(0)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  var('b1 b2 b3')
  b = vector([b1, b2, b3])
  M = A.change_ring(SR).augment(b, subdivide=True)
  L = A.solve_left(A.rref())
  B = A.left_kernel().basis().__iter__()
  L = matrix(map(lambda r: r if not r.is_zero() else next(B), L.rows()))
  v1, v2, v3, v4 = map(matrix.column, A.columns())
  l, = A.left_kernel().basis()
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  Consider the vectors
  \begin{align*}
    \vv*{v}{1} &= \sage{v1} & \vv*{v}{2} &= \sage{v2} & \vv*{v}{3} &= \sage{v3} & \vv*{v}{4} &= \sage{v4}
  \end{align*}
  Describe all $\vv{b}=\sage{b}$ in
  $\Span\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}$.

  \pause
  \begin{block}{Solution}
    Row-reducing gives
    \[
      \sage{M}\rightsquigarrow\sage{L*M}
    \]
    \pause The vectors in
    $\Span{\scriptsize\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}}$ are
    the vectors orthogonal to \pause $\sage{l}$. \pause Note that
    $\mathbb{R}^3\neq\Span{\scriptsize\Set{\vv*{v}{1},\vv*{v}{2},\vv*{v}{3},\vv*{v}{4}}}$.
  \end{block}
\end{frame}


% \begin{sagesilent}
%   A = matrix([(1,2),(1,1)])
%   v1, v2 = [matrix.column(col) for col in A.columns()]
%   L = A.inverse()
%   b = matrix.column([b1, b2])
%   M = A.change_ring(SR).augment(b, subdivide=True)
% \end{sagesilent}
% \begin{frame}
%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{example}
%     Does ${\scriptsize\Set*{\sage{v1},\sage{v2}}}$ span $\mathbb{R}^2$?
%   \end{example}

%   \onslide<+->
%   \begin{block}{Solution}
%     Since $\rref{\scriptsize\sage{A}}={\scriptsize\sage{A.rref()}}$,
%     $\Span{\scriptsize\Set*{\sage{v1},\sage{v2}}}=\mathbb{R}^2$. \pause In fact,
%     the row-reduction
%     \[
%       \sage{M}\rightsquigarrow\sage{L*M}
%     \]
%     \onslide<+->
%     shows that every $\vv{b}=\langle b_1,b_2\rangle$ satisfies
%     \[
%       c_1\cdot\vv*{v}{1}+c_2\cdot\vv*{v}{2}=\vv{b}
%     \]
%     \onslide<+-> where $c_1=\pause-b_1+2\,b_2$ and $c_2=\pause b_1-b_2$.
%   \end{block}
% \end{frame}


\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ be a collection of
    vectors in $\mathbb{R}^n$. Then
    $V=\Span\Set{\vv*{v}{1},\vv*{v}{2},\dotsc,\vv*{v}{k}}$ is a vector subspace
    of $\mathbb{R}^n$.
  \end{theorem}

\end{frame}



\subsection{Column Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{column space} of $A$ is defined
    as 
    \[
      \Col(A)=\Span\Set*{\textnormal{columns of }A}
    \]
    Note that $\Col(A)$ is a vector subspace of $\mathbb{R}^m$.
  \end{definition}

\end{frame}


\begin{sagesilent}
  A = random_matrix(ZZ, 3, 5)
  a1, a2, a3, a4, a5 = [matrix.column(col) for col in A.columns()]
  B = random_matrix(ZZ, 5, 3)
  b1, b2, b3 = [matrix.column(col) for col in B.columns()]
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A={\tiny\sage{A}}$. \pause Then
    \[
      \Col(A)=\Span{\tiny\Set*{\sage{a1},\sage{a2},\sage{a3},\sage{a4},\sage{a5}}}
    \]
  \end{example}

  \pause

  \begin{example}
    Let $B={\tiny\sage{B}}$. \pause Then
    \[
      \Col(B)=\Span{\tiny\Set*{\sage{b1}, \sage{b2}, \sage{b3}}}
    \]
  \end{example}

\end{frame}


\begin{sagesilent}
  A = matrix([(1, 0, 5), (-3, 1, -11), (-5, -1, -29), (1, -3, -7)])
  v1, v2, v3 = [matrix.column(col) for col in A.columns()]
  b = matrix.column((1, -1, -6, -4))
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Let $A={\tiny\sage{A}}$ and let $\vv{b}={\tiny\sage{b}}$. Is $\vv{b}$ in
    $\Col(A)$?
  \end{example}

  \pause

  \begin{block}{Solution}
    Note that
    \[
      \rref{\tiny\sage{M}}={\tiny\sage{M.rref()}}
    \]\pause
    Thus the equation
    \[
      c_1\cdot\vv*{v}{1}
      +c_2\cdot\vv*{v}{2}
      +c_3\cdot\vv*{v}{3}
      =\vv{b}
    \] 
    has no solution. \pause Hence $\vv{b}\notin\Col(A)$.
  \end{block}
\end{frame}


\subsection{Row Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{row space} of $A$ is defined
    as 
    \[
      \Row(A)=\Span\Set*{\textnormal{rows of }A}
    \]
    \pause
    Note that $\Row(A)$ is a vector subspace of \pause $\mathbb{R}^n$. 
  \end{definition}


  \pause
  \begin{theorem}
    $\Row(A)=\Col(A^\intercal)$
  \end{theorem}
\end{frame}


\subsection{Left Null Spaces}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Let $A$ be a $m\times n$ matrix. The \emph{left null space} of $A$ is
    defined as
    \[
      \LNull(A)=\Set*{\vv{x}\in \mathbb{R}^{m}\given \vv{x}^\intercal A=\vv{O}}
    \]
    \pause
    Note that $\LNull(A)$ is a vector subspace of \pause $\mathbb{R}^m$. 
  \end{definition}


  \pause
  \begin{theorem}
    $\LNull(A)=\Null(A^\intercal)$
  \end{theorem}
\end{frame}


\section{The Four Fundamental Subspaces}
\subsection{Summary}

\begin{frame}
  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{block}{The Four Fundamental Subspaces}
    A $m\times n$ matrix $A$ has \emph{four fundamental subspaces}.
    \begin{center}
      \begin{tabular}{rll}
        \multicolumn{1}{c}{\myotherbold{Name}} & \multicolumn{1}{c}{\myotherbold{$V\subset\mathbb{R}^{?}$}} & \multicolumn{1}{c}{\myotherbold{$\vv{b}\in V$}} \\ 
        \hline\pause
        \mybold{Column Space} & $\Col(A)\subset\mathbb{R}^m$ & $A\vv{x}=\vv{b}$ consistent \\ \pause
        \mybold{Null Space} & $\Null(A)\subset\mathbb{R}^n$ & $A\vv{b}=\vv{O}$ \\ \pause
        \mybold{Row Space} & $\Row(A)\subset\mathbb{R}^n$ & $A^\intercal\vv{x}=\vv{b}$ consistent \\ \pause
        \mybold{Left Null Space} & $\LNull(A)\subset\mathbb{R}^m$ & $A^\intercal\vv{b}=\vv{O}$
      \end{tabular}
    \end{center}
  \end{block}

\end{frame}



\subsection{Working with Column Spaces}


\begin{sagesilent}
  set_random_seed(4)
  A = random_matrix(ZZ, 3, 2)
  x1 = random_matrix(ZZ, 2, 1)
  x2 = random_matrix(ZZ, 2, 1)
  x3 = random_matrix(ZZ, 2, 1)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Produce examples of $\vv{b}\in\Col(A)$}
    This problem is \emph{easy}. \pause Simply multiply $A$ by any vector $\vv{x}$.
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vectors $\vv*{x}{1}$, $\vv*{x}{2}$, and
    $\vv*{x}{3}$ given by
    \begin{align*}
      A &= \sage{A} & \vv*{x}{1} &= \sage{x1} & \vv*{x}{2} &= \sage{x2} & \vv*{x}{3} &= \sage{x3}
    \end{align*}\pause
    Defining $\vv*{b}{1}=A\vv*{x}{1}$, $\vv*{b}{2}=A\vv*{x}{2}$, and
    $\vv*{b}{3}=A\vv*{x}{3}$ gives
    \begin{align*}
      \vv*{b}{1} &= \sage{A*x1} & \vv*{b}{2} &=  \sage{A*x2} & \vv*{b}{3} &= \sage{A*x3}
    \end{align*}\pause
    Each of $\vv*{b}{1}$, $\vv*{b}{2}$, and $\vv*{b}{3}$ is in $\Col(A)$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Determine if $\vv{b}\in\Col(A)$}
    This problem is \emph{hard}. \pause Given $\vv{b}$, we must check if the system
    $A\vv{x}=\vv{b}$ is \emph{consistent}.
  \end{block}

\end{frame}



\begin{sagesilent}
  set_random_seed(4792)
  A = random_matrix(ZZ, 3, 2)
  b = random_matrix(ZZ, 3, 1)
  M = A.augment(b, subdivide=True)
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vector $\vv{b}$ given by
    \begin{align*}
      A &= \sage{A} & \vv{b} &= \sage{b}
    \end{align*}\pause
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]\pause
    The system $A\vv{x}=\vv{b}$ is inconsistent, so $\vv{b}\notin\Col(A)$.
  \end{example}

\end{frame}


\subsection{Working with Null Spaces}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Determine if $\vv{b}\in\Null(A)$}
    This problem is \emph{easy}. \pause Simply check if $A\vv{b}=\vv{O}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  set_random_seed(4294)
  A = random_matrix(ZZ, 3, 4, algorithm='echelonizable', rank=2)
  b1 = random_matrix(ZZ, 4, 1)
  b2, b3 = map(matrix.column, A.right_kernel().basis())
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the matrix $A$ and the vectors $\vv*{b}{1}$ and $\vv*{b}{2}$ given
    by
    \begin{align*}
      A &= \sage{A} & \vv*{b}{1} &= \sage{b1} & \vv*{b}{2} &= \sage{b2}
    \end{align*}\pause
    Then
    \begin{align*}
      A\vv*{b}{1} &= \sage{A*b1} & A\vv*{b}{2} &= \sage{A*b2}
    \end{align*}\pause
    This means that $\vv*{b}{1}\notin\Null(A)$ and $\vv*{b}{2}\in\Null(A)$.
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Produce examples of $\vv{b}\in\Null(A)$}
    This problem is \emph{hard}. \pause We must find all the solutions $\vv{b}$
    to $A\vv{b}=\vv{O}$.
  \end{block}

\end{frame}


\begin{sagesilent}
  var('b1 b2 b3 b4')
  M = A.augment(zero_vector(A.nrows()), subdivide=True)
  b = matrix.column([b1, b2, b3, b4])
  bs = matrix.column([-2*b3, b3+b4, b3, b4])
  bn1 = matrix.column([-2, 1, 1, 0])
  bn2 = matrix.column([0, 1, 0, 1])
  bv1, = bn1.columns()
  bv2, = bn2.columns()
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The Gau\ss-Jordan algorithm gives
    \[
      \rref\sage{M}=\sage{M.rref()}
    \]\pause
    The vectors $\vv{b}\in\Null(A)$ are then of the form
    \[
      \vv{b}
      = \sage{b}
      = \sage{bs}
      = b_3\sage{bn1}+b_4\sage{bn2}
    \]\pause
    Plugging in any scalars for $b_3$ and $b_4$ produces examples of
    $\vv{b}\in\Null(A)$.
  \end{example}


\end{frame}



\subsection{Vector Subspaces are Column Spaces and Null Spaces}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $V\subset\mathbb{R}^n$. Then there is a $n\times k$ matrix $A$ and a
    $m\times n$ matrix $B$ satisfying $V=\Col(A)=\Null(B)$.
  \end{theorem}

  \pause

  \begin{block}{Note}
    In the homework, you will solve two problems of the form $\Col(A)=\Null(B)$.
  \end{block}

\end{frame}


\end{document}
