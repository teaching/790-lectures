all: tex

tex:
	find . -type f -name '*.tex' -printf '%h\n' | sort -u | parallel "cd {} && latexmk"

clean:
	find . -type f -name '*.tex' -printf '%h\n' | sort -u | parallel "cd {} && latexmk -C"
